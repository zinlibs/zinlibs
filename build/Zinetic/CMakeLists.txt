cmake_minimum_required(VERSION 2.4.0 FATAL_ERROR)

PROJECT (Zinetic)

set(LIBRARY_OUTPUT_PATH ../../lib/${CMAKE_BUILD_TYPE})

include_directories(
    ../../include
    ${SFML_include_dir}
)


file(
    GLOB
    zinetic_inc

# HEADERS

    Zin/config.hpp
    Zin/Zinetic.hpp
    Zin/Zinetic/Particle.hpp
    Zin/Zinetic/Object.hpp
    Zin/Zinetic/Simulation.hpp
    Zin/Zinetic/Sandbox.hpp

)

file(
    GLOB
    zinetic_src

# SRC

    ../../src/Zin/Zinetic/Simulation.cpp
    ../../src/Zin/Zinetic/Object.cpp
    ../../src/Zin/Zinetic/Particle.cpp
    ../../src/Zin/Zinetic/Sandbox.cpp


    ${zinetic_inc}

)


add_library(
    zinetic
    SHARED
    ${zinetic_src}
)

target_link_libraries(
    zinetic
)

install(
    DIRECTORY ../../include/Zin/Zinetic/
    DESTINATION include/Zin
)

install(
    TARGETS zinetic
    DESTINATION lib
)