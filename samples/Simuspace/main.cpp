////////////////////////////////////////////////////////////
/// Options
////////////////////////////////////////////////////////////
//#define ZINCONSOLE_ENABLED
#define MENU_ENABLED
#define BACKGROUND_ENABLED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zindows.hpp>
#include <Zin/Zintools.hpp>
#include <Zin/Zinetic.hpp>

////////////////////////////////////////////////////////////
/// Point d'entrée de l'application
////////////////////////////////////////////////////////////
int main()
{

////////////////////////////////////////////////////////////
/// Création et configuration de la fenêtre de rendu SFML
////////////////////////////////////////////////////////////
sf::RenderWindow App(sf::VideoMode(800, 600, 32), "Simuspace", sf::Style::Default, sf::ContextSettings(24, 8)); // Création de la fenêtre de rendu // Création de la fenêtre de rendu
    App.SetFramerateLimit(30); // Limitation du framerate à 60

////////////////////////////////////////////////////////////
/// Création et configuration du fond d'écran
////////////////////////////////////////////////////////////
#ifdef BACKGROUND_ENABLED
    sf::Image BackgroundImage;
    if( !BackgroundImage.LoadFromFile("datas/space.jpg") )
        exit(0);
    sf::Sprite BackgroundSprite;
    BackgroundSprite.SetImage(BackgroundImage);
    BackgroundSprite.SetOrigin(400, 300);
    BackgroundSprite.SetPosition(400, 320);
    BackgroundSprite.SetScale(1.6, 1.6);
    float angus = 0;
#endif

////////////////////////////////////////////////////////////
/// Création et configuration de la simulation
////////////////////////////////////////////////////////////
    zin::Simulation Simulation(779, 600); // Création de la simulation
    Simulation.Move(0, 21);
    sf::FloatRect Sandbox(50, 60, 700, 500);
    Simulation.SetSandbox(true, Sandbox);
    sf::Image ObjectsImage;
    Simulation.Randomize(100);
    if( !ObjectsImage.LoadFromFile("datas/star.tga") )
        exit(0);
    Simulation.SetObjectsImage(ObjectsImage);
    bool AttractiveCursor = false;
    unsigned int randomObjects = 100;


////////////////////////////////////////////////////////////
/// Obtention de l'index d'action
////////////////////////////////////////////////////////////
zin::ActionIndex& ActionIndex = *(zin::ActionIndex::GetSingleton());

////////////////////////////////////////////////////////////
/// Obtention du registre
////////////////////////////////////////////////////////////
zin::Register& Register = *(zin::Register::GetSingleton());
Register.Add("SimulationIsRunning", true);

////////////////////////////////////////////////////////////
/// Création et configuration des listes de l'interface
////////////////////////////////////////////////////////////
#ifdef MENU_ENABLED
    zin::List List01; // "Game" list
        zin::ActionQueue Actions01;
        Actions01+=ActionIndex["New"];
        Actions01+=ActionIndex["Pause"];
        zin::ActionSlotForList Slot01("New", Actions01);
        zin::ActionSlotForList Slot02("Load", ActionIndex["Load"]);
        Slot02.SetActivation(false);
        zin::ActionSlotForList Slot03("Save", ActionIndex["Save"]);
        Slot03.SetActivation(false);
        zin::ActionSlotForList Slot04("Save as...", ActionIndex["SaveAs"]);
        Slot04.SetActivation(false);
        zin::ActionSlotForList Slot05("Exit", ActionIndex["Exit"]);
        List01+=Slot01;
        List01+=Slot02;
        List01+=Slot03;
        List01+=Slot04;
        List01+=Slot05;

    zin::List List02; // 'View' list
        zin::ActionSlotForList Slot06("Zoom in", ActionIndex["ZoomIn"]);
        zin::ActionSlotForList Slot07("Zoom out", ActionIndex["ZoomOut"]);
        zin::ActionSlotForList Slot08("Reset zoom", ActionIndex["ResetZoom"]);
        List02+=Slot06;
        List02+=Slot07;
        List02+=Slot08;

    zin::List List03; // 'Control' list
        zin::SwitchSlotForList Slot09("Run", ActionIndex["Run"], "Pause", ActionIndex["Pause"]); // Entrée basculable
        zin::ActionSlotForList Slot10("Previous step", ActionIndex["PreviousStep"]);
        zin::ActionSlotForList Slot11("Next step", ActionIndex["NextStep"]);
        zin::ActionSlotForList Slot12("Restart", ActionIndex["Restart"]);
        zin::ActionSlotForList Slot13("Clear", ActionIndex["Clear"]);
        Slot10.SetActivation(false);
        Slot11.SetActivation(false);
        List03+=Slot09;
        List03+=Slot10;
        List03+=Slot11;
        List03+=Slot12;
        List03+=Slot13;

    zin::List List04; // 'Cells color' sublist
        zin::ActionSlotForList Slot14("White", ActionIndex["SetCellsColorOnWhite"]);
        zin::ActionSlotForList Slot15("Black", ActionIndex["SetCellsColorOnBlack"]);
        zin::ActionSlotForList Slot16("Red", ActionIndex["SetCellsColorOnRed"]);
        zin::ActionSlotForList Slot17("Green", ActionIndex["SetCellsColorOnGreen"]);
        zin::ActionSlotForList Slot18("Blue", ActionIndex["SetCellsColorOnBlue"]);
        zin::ActionSlotForList Slot19("Yellow", ActionIndex["SetCellsColorOnYellow"]);
        List04+=Slot14;
        List04+=Slot15;
        List04+=Slot16;
        List04+=Slot17;
        List04+=Slot18;
        List04+=Slot19;

    zin::List List05; // 'Back color' sublist
        zin::ActionSlotForList Slot20("White", ActionIndex["SetBackColorOnWhite"]);
        zin::ActionSlotForList Slot21("Black", ActionIndex["SetBackColorOnBlack"]);
        zin::ActionSlotForList Slot22("Red", ActionIndex["SetBackColorOnRed"]);
        zin::ActionSlotForList Slot23("Green", ActionIndex["SetBackColorOnGreen"]);
        zin::ActionSlotForList Slot24("Blue", ActionIndex["SetBackColorOnBlue"]);
        zin::ActionSlotForList Slot25("Yellow", ActionIndex["SetBackColorOnYellow"]);
        List05+=Slot20;
        List05+=Slot21;
        List05+=Slot22;
        List05+=Slot23;
        List05+=Slot24;
        List05+=Slot25;

    zin::List List06; // 'Grid color' sublist
        zin::ActionSlotForList Slot26("White", ActionIndex["SetGridColorOnWhite"]);
        zin::ActionSlotForList Slot27("Black", ActionIndex["SetGridColorOnBlack"]);
        zin::ActionSlotForList Slot28("Red", ActionIndex["SetGridColorOnRed"]);
        zin::ActionSlotForList Slot29("Green", ActionIndex["SetGridColorOnGreen"]);
        zin::ActionSlotForList Slot30("Blue", ActionIndex["SetGridColorOnBlue"]);
        zin::ActionSlotForList Slot31("Yellow", ActionIndex["SetGridColorOnYellow"]);
        List06+=Slot26;
        List06+=Slot27;
        List06+=Slot28;
        List06+=Slot29;
        List06+=Slot30;
        List06+=Slot31;

    zin::List List07; // 'Options' list
        zin::CheckSlotForList  Slot49("UniversalGravity", ActionIndex["GravityOff"], ActionIndex["GravityOn"], true); // Entrée cochable
        zin::CheckSlotForList  Slot46("AttractiveCursor", ActionIndex["NormalCursor"], ActionIndex["AttractiveCursor"]); // Entrée cochable
        zin::CheckSlotForList  Slot48("Collisions", ActionIndex["DisableCollisions"], ActionIndex["EnableCollisions"]);
        zin::CheckSlotForList  Slot47("Sandbox", ActionIndex["DisableSandbox"], ActionIndex["EnableSandbox"], true); // Entrée cochable
        zin::CheckSlotForList  Slot32("Grid", ActionIndex["DisableGrid"], ActionIndex["EnableGrid"]);
        zin::SwitchSlotForList Slot34("Random 10", ActionIndex["Random10"], "Random 100", ActionIndex["Random100"]);
        zin::ListSlotForList   Slot35("Objects color", List04);
        zin::ListSlotForList   Slot36("Back color", List05);
        zin::ListSlotForList   Slot37("Grid color", List06);
        Slot32.SetActivation(false);
        Slot35.SetActivation(false);
        Slot36.SetActivation(false);
        Slot37.SetActivation(false);
        List07+=Slot49;
        List07+=Slot46;
        List07+=Slot48;
        List07+=Slot47;
        List07+=Slot32;
        List07+=Slot34;
        List07+=Slot35;
        List07+=Slot36;
        List07+=Slot37;

    zin::List List08; // 'Help' list
        zin::ActionSlotForList Slot38("About", ActionIndex["About"]);
        Slot38.SetActivation(false);
        zin::ActionSlotForList Slot39("Licence", ActionIndex["Licence"]);
        Slot39.SetActivation(false);
        List08+=Slot38;
        List08+=Slot39;

////////////////////////////////////////////////////////////
/// Création et configuration du menu de l'interface
////////////////////////////////////////////////////////////
    zin::Menu Menu;
        zin::ListSlotForMenu Slot40("Simulation", List01);
        zin::ListSlotForMenu Slot41("View", List02);
        zin::ListSlotForMenu Slot42("Controls", List03);
        zin::ListSlotForMenu Slot43("Options", List07);
#ifdef ZINCONSOLE_ENABLED
        zin::ConsoleRibbonSlotForMenu Slot44;
#endif
        zin::ListSlotForMenu Slot45("Help", List08);
        Menu+=Slot40;
        Menu+=Slot41;
        Menu+=Slot42;
        Menu+=Slot43;
#ifdef ZINCONSOLE_ENABLED
        Menu+=Slot44;
#endif
        Menu+=Slot45;
#endif

////////////////////////////////////////////////////////////
/// Boucle d'éxecution principale
////////////////////////////////////////////////////////////
    while( App.IsOpened() )
    {
        sf::Sleep(0.02);

        sf::Event event; // Création des évènements
        zin::Events zevents(App.GetInput());
        zevents.MousePos = App.ConvertCoords(App.GetInput().GetMouseX(), App.GetInput().GetMouseY()); // Récupération temps-réel des coordonnées de la souris

    ////////////////////////////////////////////////////////////
    /// Interprétation des évènements
    ////////////////////////////////////////////////////////////
        while( App.GetEvent(event) )
        {
            zevents.Add(event);
            switch( event.Type )
            {
                case sf::Event::Closed :
                    App.Close();
                break; // Fermeture de la fenêtre
                case sf::Event::MouseWheelMoved : // Molette de la souris actionnée
                    Simulation.Update();
                    switch( event.MouseWheel.Delta ) {
                        case  1 :
                            Simulation.ZoomIn();
                        break;
                        case -1 :
                            Simulation.ZoomOut();
                        break;
                    }
                break;
                case sf::Event::MouseMoved :
                    Simulation.SetGravityPoint(AttractiveCursor, App.ConvertCoords(App.GetInput().GetMouseX(),App.GetInput().GetMouseY(), Simulation.GetView()));
                break;
                default : break;
            }
        }

    ////////////////////////////////////////////////////////////
    /// Mise à jour de la scène
    ////////////////////////////////////////////////////////////
        Simulation.Update(); // Mise à jour de la simulation
#ifdef MENU_ENABLED
        zin::ActionQueue Actions = Menu.Update(zevents); // Mise à jour du menu et réception des actions retournées
        Actions.Print(); // Affichage des actions retournées dans la console

    ////////////////////////////////////////////////////////////
    /// Traitement des actions de l'interface
    ////////////////////////////////////////////////////////////
        if( Actions.Contains(ActionIndex["Exit"]) )
            App.Close(); // Quitter le programme

        if( Actions.Contains(ActionIndex["Run"]) )
            Simulation.SetRunning(true);

        if( Actions.Contains(ActionIndex["Pause"]) )
            Simulation.SetRunning(false);

        if( Actions.Contains(ActionIndex["Clear"]) )
            Simulation.Clear();

        if( Actions.Contains(ActionIndex["New"]) )
            Simulation.Randomize(randomObjects);

        if( Actions.Contains(ActionIndex["Restart"]) )
            Simulation.Restart();

        if( Actions.Contains(ActionIndex["ZoomIn"]) )
            Simulation.ZoomIn(); // Retrécir la vue

        if( Actions.Contains(ActionIndex["ZoomOut"]) )
            Simulation.ZoomOut(); // Grossir la vue

        if( Actions.Contains(ActionIndex["ResetZoom"]) )
            Simulation.ResetZoom(); // Retablir la vue

        if( Actions.Contains(ActionIndex["AttractiveCursor"]) )
            AttractiveCursor = true;

        if( Actions.Contains(ActionIndex["NormalCursor"]) )
            AttractiveCursor = false;

        if( Actions.Contains(ActionIndex["EnableCollisions"]) )
            Simulation.SetCollisions(true);

        if( Actions.Contains(ActionIndex["DisableCollisions"]) )
            Simulation.SetCollisions(false);

        if( Actions.Contains(ActionIndex["Random100"]) )
            randomObjects = 100;

        if( Actions.Contains(ActionIndex["Random10"]) )
            randomObjects = 10;

        if( Actions.Contains(ActionIndex["GravityOn"]) )
            Simulation.SetGravitation(true);

        if( Actions.Contains(ActionIndex["GravityOff"]) )
            Simulation.SetGravitation(false);

        if( Actions.Contains(ActionIndex["EnableSandbox"]) )
            Simulation.SetSandbox(true, Sandbox);

        if( Actions.Contains(ActionIndex["DisableSandbox"]) )
            Simulation.SetSandbox(false);
#endif

    ////////////////////////////////////////////////////////////
    /// Dessin de la scène
    ////////////////////////////////////////////////////////////
        App.Clear(); // Effacement de la scène
#ifdef BACKGROUND_ENABLED
        BackgroundSprite.SetRotation(angus);
        angus-=0.02;
        App.Draw(BackgroundSprite);
#endif
        Simulation.Draw(App); // Dessin de la simulation
#ifdef MENU_ENABLED
        Menu.Draw(App); // Dessin de l'interface
#endif
        App.Display(); // Affichage du rendu final
    }

    return EXIT_SUCCESS;
}
