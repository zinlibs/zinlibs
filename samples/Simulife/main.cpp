////////////////////////////////////////////////////////////
/// Options
////////////////////////////////////////////////////////////
//#define ZINCONSOLE_ENABLED
#define MENU_ENABLED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zindows.hpp>
#include <Zin/Zintools.hpp>
#include "Simulife.hpp"

////////////////////////////////////////////////////////////
/// Point d'entrée de l'application
////////////////////////////////////////////////////////////
int main()
{

////////////////////////////////////////////////////////////
/// Création et configuration de la fenêtre de rendu SFML
////////////////////////////////////////////////////////////
    sf::RenderWindow App(sf::VideoMode(600, 400, 32), "Simulife"); // Création de la fenêtre de rendu
        App.SetFramerateLimit(60); // Limitation du framerate à 60

////////////////////////////////////////////////////////////
/// Création du flux de console Zindows
////////////////////////////////////////////////////////////
#ifdef ZINCONSOLE_ENABLED
    zin::ConsoleStream::GetSingleton();
#endif

////////////////////////////////////////////////////////////
/// Création et configuration de la simulation
////////////////////////////////////////////////////////////
    Simulife Simulife(100, 63); // Création de la simulation
        Simulife.Move(0, 21); // Déplacement de la simulation (en bas du futur menu)
        Simulife.SetZoom(6); // Agrandissement de la simulation

////////////////////////////////////////////////////////////
/// Obtention de l'index d'action
////////////////////////////////////////////////////////////
zin::ActionIndex& ActionIndex = *(zin::ActionIndex::GetSingleton());

////////////////////////////////////////////////////////////
/// Obtention du registre
////////////////////////////////////////////////////////////
zin::Register& Register = *(zin::Register::GetSingleton());
Register.Add("SimulifeIsRunning", true);

////////////////////////////////////////////////////////////
/// Création et configuration des listes de l'interface
////////////////////////////////////////////////////////////
#ifdef MENU_ENABLED
    zin::List List01; // "Game" list
        zin::ActionQueue Actions01;
        Actions01+=ActionIndex["New"];
        Actions01+=ActionIndex["Pause"];
        zin::ActionSlotForList Slot01("New", Actions01);
        zin::ActionSlotForList Slot02("Load", ActionIndex["Load"]);
        Slot02.SetActivation(false);
        zin::ActionSlotForList Slot03("Save", ActionIndex["Save"]);
        Slot03.SetActivation(false);
        zin::ActionSlotForList Slot04("Save as...", ActionIndex["SaveAs"]);
        Slot04.SetActivation(false);
        zin::ActionSlotForList Slot05("Exit", ActionIndex["Exit"]);
        List01+=Slot01;
        List01+=Slot02;
        List01+=Slot03;
        List01+=Slot04;
        List01+=Slot05;

    zin::List List02; // 'View' list
        zin::ActionSlotForList Slot06("Zoom in", ActionIndex["ZoomIn"]);
        zin::ActionSlotForList Slot07("Zoom out", ActionIndex["ZoomOut"]);
        zin::ActionSlotForList Slot08("Reset zoom", ActionIndex["ResetZoom"]);
        List02+=Slot06;
        List02+=Slot07;
        List02+=Slot08;

    zin::List List03; // 'Control' list
        zin::SwitchSlotForList Slot09("Run", ActionIndex["Run"], "Pause", ActionIndex["Pause"]); // Entrée basculable
        zin::ActionSlotForList Slot10("Previous step", ActionIndex["PreviousStep"]);
        zin::ActionSlotForList Slot11("Next step", ActionIndex["NextStep"]);
        zin::ActionSlotForList Slot12("Reset", ActionIndex["Reset"]);
        zin::ActionSlotForList Slot13("Clear", ActionIndex["Clear"]);
        List03+=Slot09;
        List03+=Slot10;
        List03+=Slot11;
        List03+=Slot12;
        List03+=Slot13;

    zin::List List04; // 'Cells color' sublist
        zin::ActionSlotForList Slot14("White", ActionIndex["SetCellsColorOnWhite"]);
        zin::ActionSlotForList Slot15("Black", ActionIndex["SetCellsColorOnBlack"]);
        zin::ActionSlotForList Slot16("Red", ActionIndex["SetCellsColorOnRed"]);
        zin::ActionSlotForList Slot17("Green", ActionIndex["SetCellsColorOnGreen"]);
        zin::ActionSlotForList Slot18("Blue", ActionIndex["SetCellsColorOnBlue"]);
        zin::ActionSlotForList Slot19("Yellow", ActionIndex["SetCellsColorOnYellow"]);
        List04+=Slot14;
        List04+=Slot15;
        List04+=Slot16;
        List04+=Slot17;
        List04+=Slot18;
        List04+=Slot19;

    zin::List List05; // 'Back color' sublist
        zin::ActionSlotForList Slot20("White", ActionIndex["SetBackColorOnWhite"]);
        zin::ActionSlotForList Slot21("Black", ActionIndex["SetBackColorOnBlack"]);
        zin::ActionSlotForList Slot22("Red", ActionIndex["SetBackColorOnRed"]);
        zin::ActionSlotForList Slot23("Green", ActionIndex["SetBackColorOnGreen"]);
        zin::ActionSlotForList Slot24("Blue", ActionIndex["SetBackColorOnBlue"]);
        zin::ActionSlotForList Slot25("Yellow", ActionIndex["SetBackColorOnYellow"]);
        List05+=Slot20;
        List05+=Slot21;
        List05+=Slot22;
        List05+=Slot23;
        List05+=Slot24;
        List05+=Slot25;

    zin::List List06; // 'Grid color' sublist
        zin::ActionSlotForList Slot26("White", ActionIndex["SetGridColorOnWhite"]);
        zin::ActionSlotForList Slot27("Black", ActionIndex["SetGridColorOnBlack"]);
        zin::ActionSlotForList Slot28("Red", ActionIndex["SetGridColorOnRed"]);
        zin::ActionSlotForList Slot29("Green", ActionIndex["SetGridColorOnGreen"]);
        zin::ActionSlotForList Slot30("Blue", ActionIndex["SetGridColorOnBlue"]);
        zin::ActionSlotForList Slot31("Yellow", ActionIndex["SetGridColorOnYellow"]);
        List06+=Slot26;
        List06+=Slot27;
        List06+=Slot28;
        List06+=Slot29;
        List06+=Slot30;
        List06+=Slot31;

    zin::List List07; // 'Options' list
        zin::CheckSlotForList  Slot32("Grid", ActionIndex["DisableGrid"], ActionIndex["EnableGrid"], true); // Entrée cochable
        zin::CheckSlotForList  Slot33("Torus", ActionIndex["DisableTorus"], ActionIndex["EnableTorus"], true);
        zin::SwitchSlotForList Slot34("Random 25%", ActionIndex["Random25"], "Random 50%", ActionIndex["Random50"]);
        zin::ListSlotForList   Slot35("Cells color", List04);
        zin::ListSlotForList   Slot36("Back color", List05);
        zin::ListSlotForList   Slot37("Grid color", List06);
        List07+=Slot32;
        List07+=Slot33;
        List07+=Slot34;
        List07+=Slot35;
        List07+=Slot36;
        List07+=Slot37;

    zin::List List08; // 'Help' list
        zin::ActionSlotForList Slot38("About", ActionIndex["About"]);
        Slot38.SetActivation(false);
        zin::ActionSlotForList Slot39("Licence", ActionIndex["Licence"]);
        Slot39.SetActivation(false);
        List08+=Slot38;
        List08+=Slot39;

////////////////////////////////////////////////////////////
/// Création et configuration du menu de l'interface
////////////////////////////////////////////////////////////
    zin::Menu Menu;
        zin::ListSlotForMenu Slot40("Game", List01);
        zin::ListSlotForMenu Slot41("View", List02);
        zin::ListSlotForMenu Slot42("Controls", List03);
        zin::ListSlotForMenu Slot43("Options", List07);
#ifdef ZINCONSOLE_ENABLED
        zin::ConsoleRibbonSlotForMenu Slot44;
#endif
        zin::ListSlotForMenu Slot45("Help", List08);
        Menu+=Slot40;
        Menu+=Slot41;
        Menu+=Slot42;
        Menu+=Slot43;
#ifdef ZINCONSOLE_ENABLED
        Menu+=Slot44;
#endif
        Menu+=Slot45;
#endif

////////////////////////////////////////////////////////////
/// Boucle d'éxecution principale
////////////////////////////////////////////////////////////
    while( App.IsOpened() )
    {
        sf::Sleep(0.02); // Repos du processeur

        sf::Event event; // Création des évènements
        zin::Events zevents(App.GetInput());
        zevents.MousePos = App.ConvertCoords(App.GetInput().GetMouseX(), App.GetInput().GetMouseY()); // Récupération temps-réel des coordonnées de la souris

     ////////////////////////////////////////////////////////////
    /// Interprétation des évènements
    ////////////////////////////////////////////////////////////
        while( App.GetEvent(event) )
        {
            zevents.Add(event);
            switch( event.Type )
            {
                case sf::Event::Closed :
                    App.Close();
                break; // Fermeture de la fenêtre
                case sf::Event::MouseWheelMoved : // Molette de la souris actionnée
                    switch( event.MouseWheel.Delta )
                    {
                        case  1 :
                            Simulife.ZoomIn();
                        break;
                        case -1 :
                            Simulife.ZoomOut();
                        break;
                    }
                break;
                case sf::Event::MouseMoved :
                    Simulife.SetZoomCenter(event.MouseMove.X, event.MouseMove.Y);
                break;
                default : break;
            }
        }

    ////////////////////////////////////////////////////////////
    /// Mise à jour de la scène
    ////////////////////////////////////////////////////////////
        if( App.GetInput().IsMouseButtonDown(sf::Mouse::Left) ) // Dessin de nouvelles cellules à la souris
            Simulife.CreateCell(zevents.MousePos.x, zevents.MousePos.y);

        else if( App.GetInput().IsMouseButtonDown(sf::Mouse::Right) ) // Effacement de cellules à la souris
            Simulife.DeleteCell(zevents.MousePos.x, zevents.MousePos.y);

        Simulife.Update(); // Mise à jour de la simulation
#ifdef MENU_ENABLED
        zin::ActionQueue Actions = Menu.Update(zevents); // Mise à jour du menu et réception des actions retournées
        Actions.Print(); // Affichage des actions retournées dans la console

    ////////////////////////////////////////////////////////////
    /// Traitement des actions de l'interface
    ////////////////////////////////////////////////////////////
        if( Actions.Contains(ActionIndex["Exit"]) )
            App.Close(); // Quitter le programme

        if( Actions.Contains(ActionIndex["Random25"]) )
            Simulife.SetRandom(.25); // Hasard à 25%

        if( Actions.Contains(ActionIndex["Random50"]) )
            Simulife.SetRandom(.50); // Hasard à 50%

        if( Actions.Contains(ActionIndex["ZoomIn"]) )
            Simulife.ZoomIn(); // Retrécir la vue

        if( Actions.Contains(ActionIndex["ZoomOut"]) )
            Simulife.ZoomOut(); // Grossir la vue

        if( Actions.Contains(ActionIndex["ResetZoom"]) )
            Simulife.ResetZoom(); // Retablir la vue

        if( Actions.Contains(ActionIndex["Clear"]) )
            Simulife.Clear(); // Effacer

        if( Actions.Contains(ActionIndex["Run"]) )
            Simulife.SetRunning(true); // Lecture

        if( Actions.Contains(ActionIndex["Pause"]) ) {
            Simulife.SetRunning(false); // Pause
            Register.Disable("SimulifeIsRunning");
        }

        if( Actions.Contains(ActionIndex["New"]) )
            Simulife.Reset(); // Nouveau

        if( Actions.Contains(ActionIndex["Reset"]) )
            Simulife.Restart(); // Recommencer

        if( Actions.Contains(ActionIndex["PreviousStep"]) )
            Simulife.PreviousStep(); // Précédent

        if( Actions.Contains(ActionIndex["NextStep"]) )
            Simulife.NextStep(); // Suivant

        if( Actions.Contains(ActionIndex["EnableGrid"]) )
            Simulife.SetGrid(true); // Activer la grille

        if( Actions.Contains(ActionIndex["DisableGrid"]) )
            Simulife.SetGrid(false); // Désactiver la grille

        if( Actions.Contains(ActionIndex["EnableTorus"]) )
            Simulife.SetTorus(true); // Activer le bouclement

        if( Actions.Contains(ActionIndex["DisableTorus"]) )
            Simulife.SetTorus(false); // Désactiver le bouclement

        if( Actions.Contains(ActionIndex["SetCellsColorOnWhite"]) )
            Simulife.SetCellsColor(sf::Color::White); // Couleurs des cellules

        if( Actions.Contains(ActionIndex["SetCellsColorOnBlack"]) )
            Simulife.SetCellsColor(sf::Color::Black);

        if( Actions.Contains(ActionIndex["SetCellsColorOnRed"]) )
            Simulife.SetCellsColor(sf::Color::Red);

        if( Actions.Contains(ActionIndex["SetCellsColorOnGreen"]) )
            Simulife.SetCellsColor(sf::Color::Green);

        if( Actions.Contains(ActionIndex["SetCellsColorOnBlue"]) )
            Simulife.SetCellsColor(sf::Color::Blue);

        if( Actions.Contains(ActionIndex["SetCellsColorOnYellow"]) )
            Simulife.SetCellsColor(sf::Color::Yellow);

        if( Actions.Contains(ActionIndex["SetBackColorOnWhite"]) )
            Simulife.SetBackColor(sf::Color::White); // Couleurs du fond

        if( Actions.Contains(ActionIndex["SetBackColorOnBlack"]) )
            Simulife.SetBackColor(sf::Color::Black);

        if( Actions.Contains(ActionIndex["SetBackColorOnRed"]) )
            Simulife.SetBackColor(sf::Color::Red);

        if( Actions.Contains(ActionIndex["SetBackColorOnGreen"]) )
            Simulife.SetBackColor(sf::Color::Green);

        if( Actions.Contains(ActionIndex["SetBackColorOnBlue"]) )
            Simulife.SetBackColor(sf::Color::Blue);

        if( Actions.Contains(ActionIndex["SetBackColorOnYellow"]) )
            Simulife.SetBackColor(sf::Color::Yellow);

        if( Actions.Contains(ActionIndex["SetGridColorOnWhite"]) )
            Simulife.SetGridColor(sf::Color::White); // Couleurs de la grille

        if( Actions.Contains(ActionIndex["SetGridColorOnBlack"]) )
            Simulife.SetGridColor(sf::Color::Black);

        if( Actions.Contains(ActionIndex["SetGridColorOnRed"]) )
            Simulife.SetGridColor(sf::Color::Red);

        if( Actions.Contains(ActionIndex["SetGridColorOnGreen"]) )
            Simulife.SetGridColor(sf::Color::Green);

        if( Actions.Contains(ActionIndex["SetGridColorOnBlue"]) )
            Simulife.SetGridColor(sf::Color::Blue);

        if( Actions.Contains(ActionIndex["SetGridColorOnYellow"]) )
            Simulife.SetGridColor(sf::Color::Yellow);
#endif

    ////////////////////////////////////////////////////////////
    /// Dessin de la scène
    ////////////////////////////////////////////////////////////
        App.Clear(); // Effacement de la scène
        App.Draw(Simulife); // Dessin de la simulation
#ifdef MENU_ENABLED
        Menu.Draw(App); // Dessin de l'interface
#endif
        App.Display(); // Affichage du rendu final
    }

    return EXIT_SUCCESS;
}
