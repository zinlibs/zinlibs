/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Simulife - Copyright (c) 2010-2011 Pierre-Emmanuel Brian
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "Simulife.hpp"

////////////////////////////////////////////////////////////
/// class Simulife definition
////////////////////////////////////////////////////////////
Simulife::Simulife(float Width, float Height) :
isRunning(false),
isGridEnabled(true),
isTorusEnabled(true),
m_CellsColor(sf::Color::Green),
m_GridColor(sf::Color::Black),
m_BackColor(sf::Color::Black),
m_Size(Width, Height),
m_Zoom(4),
m_RandomPercent(.50) {
    m_Cells.assign(m_Size.x*m_Size.y, false);
    Randomize();
}

void Simulife::Clear() {
    m_Cells.clear();
    m_Cells.assign(m_Size.x*m_Size.y, false);
}

void Simulife::Randomize() {
    srand(time(NULL));
    unsigned int n(0);
    unsigned int CellsNbMax = m_Size.x*m_Size.y*m_RandomPercent;
    while( n != CellsNbMax ) {
        unsigned int x = rand()%static_cast<unsigned int>(m_Size.x);
        unsigned int y = rand()%static_cast<unsigned int>(m_Size.y);
        if ( !m_Cells[x+m_Size.x*y] ) {
            n++;
            m_Cells[x+m_Size.x*y] = true;
        }
    }
    m_FirstCells = m_PreviousCells = m_Cells;
}

void Simulife::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

void Simulife::Move(sf::Vector2f Shift) {
    m_Pos+=Shift;
}

void Simulife::SetPosition(float x, float y) {
    SetPosition(sf::Vector2f(x, y));
}

void Simulife::SetPosition(sf::Vector2f Pos) {
    m_Pos = Pos;
}

void Simulife::SetCellsColor(sf::Color Color) {
    m_CellsColor = Color;
}

void Simulife::SetBackColor(sf::Color Color) {
    m_BackColor = Color;
}

void Simulife::SetGridColor(sf::Color Color) {
    m_GridColor = Color;
}

void Simulife::SetZoomCenter(int x, int y) {
    m_ZoomCenter = sf::Vector2f(static_cast<float>(x), static_cast<float>(y));
}

void Simulife::Resize(float x, float y) {
    Resize(sf::Vector2f(x, y));
}

void Simulife::Resize(sf::Vector2f Size) {
    m_Size = Size;
}

void Simulife::Reset() {
    m_Cells.clear();
    m_PreviousCells.clear();
    m_Cells.assign(m_Size.x*m_Size.y, false);
    Randomize();
}

void Simulife::Restart() {
    m_Cells.clear();
    m_Cells = m_FirstCells;
}

void Simulife::SetRandom(float RandomPercent) {
    if( RandomPercent > 0 && RandomPercent < 1 )
        m_RandomPercent = RandomPercent;
}

void Simulife::SetGrid(bool Required) {
    isGridEnabled = Required;
}

void Simulife::SetZoom(unsigned int Zoom) {
    m_Zoom = Zoom;
}

void Simulife::ZoomIn() {
    m_Zoom++;
    //m_Pos+=(m_ZoomCenter-m_Pos);
}

void Simulife::ZoomOut() {
    if( m_Zoom > 1 )
        m_Zoom--;
}

void Simulife::ResetZoom() {
    m_Zoom = 1;
}

void Simulife::SetTorus(bool Required) {
    isTorusEnabled = Required;
}

void Simulife::SetRunning(bool Enabled) {
    isRunning = Enabled;
}

void Simulife::PreviousStep() {
    m_Cells = m_PreviousCells;
}

void Simulife::Update() {
    if( isRunning ) {
        NextStep();
    }
}

void Simulife::NextStep() {
    m_PreviousCells = m_Cells;
    std::vector<bool> Cache = m_Cells;
    for( size_t k(0); k < m_Cells.size(); ++k )
        Cache[k] = UpdateCell(k) ? true : false;
    m_Cells = Cache;
}

void Simulife::Render(sf::RenderTarget& target, sf::Renderer& renderer) const {
/// Dessin du fond :
    sf::Shape Background = sf::Shape::Rectangle(0, 0, m_Size.x*m_Zoom,  m_Size.y*m_Zoom, m_BackColor);
    Background.Move(m_Pos);
    target.Draw(Background);
/// Dessin des cellules :
    for( size_t k(0); k < m_Cells.size(); ++k ) {
        if( m_Cells[k] ) {
            sf::Vector2i CellPos = GetCellPos(k);
            sf::Shape Cell = sf::Shape::Rectangle(CellPos.x, CellPos.y, 1, 1, m_CellsColor);
            Cell.Move(m_Pos);
            Cell.Scale(m_Zoom, m_Zoom);
            target.Draw(Cell);
        }
    }
/// Dessin de la grille :
    if( isGridEnabled && m_Zoom != 1) {
        for( size_t k(0); k <= m_Size.x; ++k ) { /// h
            sf::Shape Line = sf::Shape::Line(m_Zoom*k, 0, m_Zoom*k, m_Zoom*m_Size.y, 1, m_GridColor);
            Line.Move(m_Pos);
            target.Draw(Line);
        }
        for( size_t k(0); k <= m_Size.y; ++k ) { /// v
            sf::Shape Line = sf::Shape::Line(0, m_Zoom*k, m_Zoom*m_Size.x, m_Zoom*k, 1, m_GridColor);
            Line.Move(m_Pos);
            target.Draw(Line);
        }
    }
}

void Simulife::CreateCell(int x, int y) {
    x-=m_Pos.x;
    y-=m_Pos.y;
    for( size_t k(0); k < m_Cells.size(); ++k ) {
        sf::Vector2i CellPos = GetCellPos(k);
        if( x >= m_Zoom*CellPos.x && x < m_Zoom*(CellPos.x + 1) &&
            y >= m_Zoom*CellPos.y && y < m_Zoom*(CellPos.y + 1) ) {
                m_Cells[k] = true;
        }
    }
}

void Simulife::DeleteCell(int x, int y) {
    x-=m_Pos.x;
    y-=m_Pos.y;
    for( size_t k(0); k < m_Cells.size(); ++k ) {
        sf::Vector2i CellPos = GetCellPos(k);
        if( x >= m_Zoom*CellPos.x && x < m_Zoom*(CellPos.x+1) &&
            y >= m_Zoom*CellPos.y && y < m_Zoom*(CellPos.y+1) ) {
                m_Cells[k] = false;
        }
    }
}

sf::Vector2i Simulife::GetCellPos(unsigned int Indice) const {
    sf::Vector2i Pos;
    Pos.x = Indice%static_cast<unsigned int>(m_Size.x);
    Pos.y = (Indice-Pos.x)/m_Size.x;
    return Pos;
}

bool Simulife::UpdateCell(unsigned int Indice) {
    unsigned int NbVoisin(0);
    unsigned int NeighboursIndices[] = { -m_Size.x-1, -m_Size.x, -m_Size.x+1, -1, 1, m_Size.x-1, m_Size.x, m_Size.x+1 };
    sf::Vector2i CellPos = GetCellPos(Indice);
/// Bouclage :
    if( isTorusEnabled ) {
        if( CellPos.x == 0 ) {
            NeighboursIndices[0]+=m_Size.x;
            NeighboursIndices[3]+=m_Size.x;
            NeighboursIndices[5]+=m_Size.x;
        }
        if( CellPos.x == m_Size.x-1 ) {
            NeighboursIndices[2]-=m_Size.x;
            NeighboursIndices[4]-=m_Size.x;
            NeighboursIndices[7]-=m_Size.x;
        }
        if( CellPos.y == 0 ) {
            NeighboursIndices[0]+=(m_Size.x*m_Size.y);
            NeighboursIndices[1]+=(m_Size.x*m_Size.y);
            NeighboursIndices[2]+=(m_Size.x*m_Size.y);
        }
        if( CellPos.y == m_Size.y-1 ) {
            NeighboursIndices[5]-=(m_Size.x*m_Size.y);
            NeighboursIndices[6]-=(m_Size.x*m_Size.y);
            NeighboursIndices[7]-=(m_Size.x*m_Size.y);
        }
    }
    else {
        if( CellPos.x == 0 or CellPos.x == m_Size.x-1 or CellPos.y == 0 or CellPos.y == m_Size.y-1 ) {
            return false;
        }
    }
/// Calcul du nombre de voisins :
    for( int k = 0; k < 8; k++ ) {
        if( m_Cells[Indice + NeighboursIndices[k]] ) {
            NbVoisin++;
        }
    }
/// Une cellule vivante poss�dant deux ou trois voisines vivantes le reste, sinon elle meurt :
    if( m_Cells[Indice] ) {
        return NbVoisin == 2 || NbVoisin == 3;
    }
/// Une cellule morte poss�dant exactement trois voisines vivantes devient vivante (elle na�t) :
    else {
        return NbVoisin == 3;
    }
}
