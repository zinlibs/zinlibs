#ifndef SIMULIFE_HPP_INCLUDED
#define SIMULIFE_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>

////////////////////////////////////////////////////////////
/// class Simulife declaration
////////////////////////////////////////////////////////////
class Simulife : public sf::Drawable {
////////////////////////////////////
  public :
////////////////////////////////////
    Simulife(float Width = 100, float Height = 100);
    void Move(float x, float y);
    void Move(sf::Vector2f Shift);
    void SetPosition(float x, float y);
    void SetPosition(sf::Vector2f Pos);
    void SetZoomCenter(int x, int y);
    void Resize(float x, float y);
    void Resize(sf::Vector2f Size);
    void SetZoom(unsigned int Zoom);
    void ZoomIn();
    void ZoomOut();
    void ResetZoom();
    void SetRandom(float RandomPercent);
    void SetGrid(bool Enabled);
    void SetTorus(bool Enabled);
    void SetRunning(bool Enabled);
    void SetCellsColor(sf::Color Color);
    void SetBackColor(sf::Color Color);
    void SetGridColor(sf::Color Color);
    void CreateCell(int x, int y);
    void DeleteCell(int x, int y);
    void Reset();
    void Restart();
    void NextStep();
    void PreviousStep();
    void Update();
    void Clear();
////////////////////////////////////
  protected :
////////////////////////////////////
    void Render(sf::RenderTarget& target, sf::Renderer& renderer) const;
    bool UpdateCell(unsigned int Indice);
    void Randomize();
    sf::Vector2i GetCellPos(unsigned int Indice) const;
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::Vector2f m_Size;
    sf::Vector2f m_Pos;
    std::vector<bool> m_Cells;
    std::vector<bool> m_PreviousCells;
    std::vector<bool> m_FirstCells;
    bool isRunning;
    bool isGridEnabled;
    bool isTorusEnabled;
    sf::Color m_GridColor;
    sf::Color m_CellsColor;
    sf::Color m_BackColor;
    unsigned int m_Zoom;
    sf::Vector2f m_ZoomCenter;
    float m_RandomPercent;
};

#endif // SIMULIFE_HPP_INCLUDED

