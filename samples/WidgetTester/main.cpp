////////////////////////////////////////////////////////////
/// Options
////////////////////////////////////////////////////////////
#define ZINCONSOLE_ENABLED
#define MENU_ENABLED
#define TEXTEDIT_ENABLED
#define BACKGROUND_ENABLED
#define BUTTON_ENABLED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zindows.hpp>
#include <Zin/Zintools.hpp>

////////////////////////////////////////////////////////////
/// Entry point of the application
////////////////////////////////////////////////////////////
int main()
{

////////////////////////////////////////////////////////////
/// Création et configuration de la fenêtre de rendu SFML
////////////////////////////////////////////////////////////
    sf::RenderWindow App(sf::VideoMode(800, 600, 32), "WidgetTester"); // Création de la fenêtre de rendu
        App.SetFramerateLimit(40); // Limitation du framerate à 40

        sf::View view = App.GetDefaultView();
        view.SetViewport(sf::FloatRect(0, 0, .5f, .5f));
        App.SetView(view);
////////////////////////////////////////////////////////////
/// Création du flux de console Zindows
////////////////////////////////////////////////////////////
#ifdef BACKGROUND_ENABLED

////////////////////////////////////////////////////////////
/// Création et configuration du fond d'écran
////////////////////////////////////////////////////////////
    sf::Image BackImage;
    if( !BackImage.LoadFromFile("datas/Libre.jpg") )
        exit(0);
    sf::Sprite BackSprite;
        BackSprite.SetImage(BackImage);
        BackSprite.Resize(800, 600);
#endif

float step = 0;

////////////////////////////////////////////////////////////
/// Création et configuration du texte éditable
////////////////////////////////////////////////////////////
#ifdef TEXTEDIT_ENABLED
    std::string str;
    std::ifstream fichier("datas/Latin.txt");
    if( fichier ) { // ce test échoue si le fichier n'est pas ouvert
        std::stringstream buffer; // variable contenant l'intégralité du fichier & copier l'intégralité du fichier dans le buffer
        buffer << fichier.rdbuf(); // nous n'avons plus besoin du fichier !
        fichier.close(); // manipulations du buffer...
        str = buffer.str();
    }
    else str = "Fichier introuvable !";

    zin::Text Text(str);
       Text.Move(50, 50);

    zin::TextBox TextBox(Text, 600, 400);

#endif

////////////////////////////////////////////////////////////
/// Obention de l'index d'action
////////////////////////////////////////////////////////////
    zin::ActionIndex& ActionIndex = *zin::ActionIndex::GetSingleton();

////////////////////////////////////////////////////////////
/// Création et configuration de boutons
////////////////////////////////////////////////////////////
#ifdef BUTTON_ENABLED
zin::TextButton TextButton("Hello world !");
    TextButton.Move(sf::Vector2f(250, 480));
    TextButton.SetAction(zin::ActionQueue(ActionIndex.Get("Test")));

sf::Image DragonButtonImage;
    if( !DragonButtonImage.LoadFromFile("datas/DragonButton.tga") )
        exit(0);

sf::Image DragonButtonPressedImage;
    if( !DragonButtonPressedImage.LoadFromFile("datas/DragonButtonPressed.tga") )
        exit(0);

zin::SpriteButton DragonButton(&DragonButtonImage, &DragonButtonPressedImage);
    DragonButton.Move(sf::Vector2f(60, 480));

zin::Slider Slider(200, true, 0, 6);
    Slider.Move(400, 520);

zin::Scroller Scroller(200, zin::Scroller::Horizontal);
    Scroller.Move(400, 480);
#endif

#ifdef MENU_ENABLED
////////////////////////////////////////////////////////////
/// Création et configuration des listes du menu
////////////////////////////////////////////////////////////
    zin::List List01;
        zin::ActionSlotForList Slot01("Exit", ActionIndex.Get("Exit"));
        List01+=Slot01;

    zin::List List02;
        zin::ActionSlotForList Slot02("Undo", ActionIndex.Get("Undo"));
        zin::ActionSlotForList Slot03("Redo", ActionIndex.Get("Redo"));
        zin::ActionSlotForList Slot04("Cut", ActionIndex.Get("Cut"));
        zin::ActionSlotForList Slot05("Copy", ActionIndex.Get("Copy"));
        zin::ActionSlotForList Slot06("Paste", ActionIndex.Get("Paste"));
        zin::ActionSlotForList Slot07("Select all", ActionIndex.Get("SelectAll"));
        zin::ActionSlotForList Slot08("Erase", ActionIndex.Get("Erase"));
        List02+=Slot02;
        List02+=Slot03;
        List02+=Slot04;
        List02+=Slot05;
        List02+=Slot06;
        List02+=Slot07;
        List02+=Slot08;

////////////////////////////////////////////////////////////
/// Création et configuration du menu
////////////////////////////////////////////////////////////
    zin::Menu Menu;
        zin::ListSlotForMenu Slot09("Program", List01);
        zin::Ribbon Ribbon;
        zin::TextButton ButtonTest("Test");
        Ribbon.Add(ButtonTest);
        zin::RibbonSlotForMenu Slot12("Test", Ribbon);
#ifdef ZINCONSOLE_ENABLED
        zin::ConsoleRibbonSlotForMenu Slot10;
#endif
        zin::ListSlotForMenu Slot11("Edition", List02);
        Menu+=(Slot09);
#ifdef ZINCONSOLE_ENABLED
        Menu+=(Slot10);
#endif
        Menu+=(Slot11);
        Menu+=(Slot12);
#endif

////////////////////////////////////////////////////////////
/// Boucle d'éxecution principale
////////////////////////////////////////////////////////////
    while( App.IsOpened() )
    {
        sf::Sleep(0.02); // Repos du processeur

        sf::Event event;
        zin::Events zevents(App.GetInput());
        zevents.MousePos = App.ConvertCoords(App.GetInput().GetMouseX(),App.GetInput().GetMouseY());

    ////////////////////////////////////////////////////////////
    /// Interprétation des évènements
    ////////////////////////////////////////////////////////////
        while( App.GetEvent(event) )
        {
            zevents.Add(event);
            switch( event.Type )
            {
                case sf::Event::Closed : App.Close(); break; // Fermeture de la fenêtre
            }
        }

    ////////////////////////////////////////////////////////////
    /// Traitement des actions de l'interface
    ////////////////////////////////////////////////////////////
        zin::ActionQueue Actions;
#ifdef BUTTON_ENABLED
        Actions+=TextButton.Update(zevents);
        Actions+=DragonButton.Update(zevents);
        Actions+=Slider.Update(zevents); // Mise à jour du menu et réception des actions retournées
        Actions+=Scroller.Update(zevents);

#endif
#ifdef MENU_ENABLED
        Actions+=Menu.Update(zevents); // Mise à jour du menu et réception des actions retournées
#endif
#ifdef TEXTEDIT_ENABLED
        Actions+=TextBox.Update(zevents);
#endif
        Actions.Print(); // Affichage des actions retournées dans la console

        zin::Action action;

        while( Actions.Get(action) ) {
            if( action == ActionIndex["Exit"] )
                App.Close();
        }

#ifdef TEXTEDIT_ENABLED
 //       if( Actions.Contains(ActionIndex.Get("SelectAll")) )
//            Text.SelectAll();

 //       if( Actions.Contains(ActionIndex.Get("Erase")) )
 //       Text.EraseSelection();
#endif
    ////////////////////////////////////////////////////////////
    /// Rendu
    ////////////////////////////////////////////////////////////
#ifdef BACKGROUND_ENABLED
        App.Draw(BackSprite);
#endif
#ifdef TEXTEDIT_ENABLED
        TextBox.Draw(App);
#endif
#ifdef MENU_ENABLED
        Menu.Draw(App);
#endif
#ifdef BUTTON_ENABLED
        TextButton.Draw(App);
        DragonButton.Draw(App);
        Slider.Draw(App);
        Scroller.Draw(App);
#endif
        App.Display();
        App.Clear();
    }
/// Fin du programme :
    return EXIT_SUCCESS;
}
