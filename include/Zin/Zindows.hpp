#ifndef ZINDOWS_HPP_INCLUDED
#define ZINDOWS_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zindows/TextBox.hpp>
#include <Zin/Zindows/Text.hpp>
#include <Zin/Zindows/Label.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Zindows/WidgetContainer.hpp>
#include <Zin/Zindows/Trigger.hpp>
#include <Zin/Zindows/Slot.hpp>
#include <Zin/Zindows/Slider.hpp>
#include <Zin/Zindows/Scroller.hpp>
#include <Zin/Zindows/List.hpp>
#include <Zin/Zindows/Button.hpp>
#include <Zin/Zindows/Menu.hpp>
#include <Zin/Zindows/Ribbon.hpp>
#include <Zin/Zindows/Theme.hpp>
#include <Zin/Zindows/WidgetFactory.hpp>

#endif // ZINDOWS_HPP_INCLUDED
