#ifndef ZINTOOLS_SYSTEM_HPP_INCLUDED
#define ZINTOOLS_SYSTEM_HPP_INCLUDED

#include <Zintools/System/Container.hpp>
#include <Zintools/System/Exeptions.hpp>
#include <Zintools/System/Singleton.hpp>
#include <Zintools/System/Settings.hpp>
#include <Zintools/System/StringStream.hpp>
#include <Zintools/System/Timer.hpp>
#include <Zintools/System/Trigger.hpp>
#include <Zintools/System/TriggerList.hpp>
#include <Zintools/System/Triggerer.hpp>

#endif // ZINTOOLS_SYSTEM_HPP_INCLUDED
