#ifndef ZINTOOLS_DRAWING_HPP_INCLUDED
#define ZINTOOLS_DRAWING_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/System/Container.hpp>
#include <Zin/Config.hpp>
#include <list>

namespace zin {

////////////////////////////////////////////////////////////
/// class Drawing declaration
////////////////////////////////////////////////////////////
class ZIN_API Drawing : public sf::Drawable {
////////////////////////////////////
  public :
////////////////////////////////////
                          ~Drawing();
    void                   Add(const sf::Shape& shape);
    void operator          +=(const sf::Shape& shape);
    void                   Add(const sf::Text& text);
    void operator          +=(const sf::Text& text);
    void                   Add(const sf::Sprite& sprite);
    void operator          +=(const sf::Sprite& sprite);
    void                   Move(const sf::Vector2f& shift);
    void                   Move(float dx, float dy);
    void                   SetPosition(const sf::Vector2f& pos);
    void                   SetPosition(float px, float py);
    void                   Clear();
////////////////////////////////////
  protected :
////////////////////////////////////
    void                   Render(sf::RenderTarget& target, sf::Renderer& renderer) const;
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::Vector2f              m_Pos;
    std::list<sf::Drawable*>  m_Drawables;
};

}

#endif // ZINTOOLS_DRAWING_HPP_INCLUDED
