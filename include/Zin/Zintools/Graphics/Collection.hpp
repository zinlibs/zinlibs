#ifndef ZINTOOLS_COLLECTION_HPP_INCLUDED
#define ZINTOOLS_COLLECTION_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <string>
#include <map>
#include <iostream>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Collection declaration
////////////////////////////////////////////////////////////
template<typename T>
class ZIN_API Collection {
////////////////////////////////////
  public :
////////////////////////////////////
    const T& Get(const std::string& resourceName, const std::string& resourcePath = "");
    const T& Get(const std::string& resourceName, T* resource_ptr);
////////////////////////////////////
  protected :
////////////////////////////////////
                    ~Collection();
                     Collection();
    virtual const T& Load(const std::string& resourcePath) {}
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    std::map<std::string, const T*> m_Resources;
};

}

#include <Zin/Zintools/Graphics/Collection.inl>

#endif // ZINTOOLS_COLLECTION_HPP_INCLUDED
