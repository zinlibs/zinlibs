/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    Please help to support the project at 'https://code.google.com/p/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace zin {

////////////////////////////////////////////////////////////
/// class Collection definition
////////////////////////////////////////////////////////////
template <typename T>
Collection<T>::Collection() {
    std::cout << "[zintools] Collection created (" << this << ")" << std::endl;
}

template <typename T>
Collection<T>::~Collection() {
    std::cout << "[zintools] Collection deleted (" << this << ")" << std::endl;
/// Destruction des Resources contenues dans la map :
    while( m_Resources.begin() != m_Resources.end() ) {
        delete m_Resources.begin()->second;
        m_Resources.erase(m_Resources.begin());
    }
}

template <typename T>
const T& Collection<T>::Get(const std::string& resourceName, const std::string& resourcePath) {
/// Recherche d'un �ventuel doublon :
    typename std::map<std::string, const T*>::iterator it = m_Resources.find(resourceName);
    if( it != m_Resources.end() )
    /// Si le doublon est trouv� on le renvoit :
        return *it->second;
/// Sinon on fait l'acquisition de la ressource, on la stocke et on la fournit � l'utilisateur :
    const T* ptr = &Load(resourcePath);
    m_Resources.insert(std::pair<std::string, const T*>(resourceName, ptr));
    return *ptr;
}

template <typename T>
const T& Collection<T>::Get(const std::string& resourceName, T* resource_ptr) {
/// Recherche d'un �ventuel doublon :
    typename std::map<std::string, const T*>::iterator it = m_Resources.find(resourceName);
    if( it != m_Resources.end() )
    /// Si le doublon est trouv� on le renvoit :
        return *it->second;
/// Sinon on fait l'acquisition de la ressource, on la stocke et on la fournit � l'utilisateur :
    const T* ptr = resource_ptr;
    m_Resources.insert(std::pair<std::string, const T*>(resourceName, ptr));
    return *ptr;
}

}
