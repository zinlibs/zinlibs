#ifndef ZINTOOLS_TEXTTOOLS_HPP_INCLUDED
#define ZINTOOLS_TEXTTOOLS_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class TextTools declaration
////////////////////////////////////////////////////////////
class ZIN_API TextTools {
////////////////////////////////////
  public :
////////////////////////////////////
    static const sf::Vector2f& GetSize(const sf::Text& text);
    static float GetWidth(const sf::Text& text);
    static float GetHeight(const sf::Text& text);
    static float GetCharWidth(char c, const sf::Font& font, float fontSize);
    static void  Center(sf::Text& text, float width, float height);
};

}

#endif // ZINTOOLS_TEXTTOOLS_HPP_INCLUDED
