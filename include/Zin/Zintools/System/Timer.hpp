#ifndef ZINTOOLS_TIMER_HPP_INCLUDED
#define ZINTOOLS_TIMER_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/System.hpp>
#include <iostream>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Timer declaration
////////////////////////////////////////////////////////////
class ZIN_API Timer {
////////////////////////////////////
  public :
////////////////////////////////////
              Timer();
    void      Reset();
    void      Start();
    void      Pause();
    void      Update();
    void      Print();
    void      SaveCurrentTime();
    float     GetTime();
    float     GiveTimeElapsed();
    bool      IsRunning();
////////////////////////////////////
  private :
////////////////////////////////////
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    float     m_Time;
    float     m_T;
    float     m_StartTime;
    float     m_TimePause;
    bool      m_IsRunning;
    bool      m_IsFirst;
    sf::Clock m_Clock;
};

}

#endif // ZINTOOLS_TIMER_HPP_INCLUDED
