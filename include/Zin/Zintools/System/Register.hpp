#ifndef ZINTOOLS_SETTINGS_HPP_INCLUDED
#define ZINTOOLS_SETTINGS_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <map>
#include <SFML/System.hpp>
#include <iostream>
#include <Zin/Zintools/System/Singleton.hpp>
#include <Zin/Config.hpp>

namespace zin {

struct Value;

////////////////////////////////////////////////////////////
/// class Settings declaration
////////////////////////////////////////////////////////////
class ZIN_API Register : public Singleton<Register> {
////////////////////////////////////
  public :
////////////////////////////////////
    void                         Switch(const sf::String& string);
    void                         Enable(const sf::String& string);
    void                         Disable(const sf::String& string);
    void                         Add(const sf::String& Key, bool enabled);
    void                         Change(const sf::String& string, bool enabled);
    bool                         IsTrue(const sf::String& string);
    bool                         Contains(const sf::String& string);
    bool                         IsChanged(const sf::String& string);
    void                         Print();
////////////////////////////////////
  protected :
////////////////////////////////////
    friend class                 Singleton<Register>;
                                 Register();
    struct Value {
    ////////////////////////////////////
      public :
    ////////////////////////////////////
                                 Value(bool Enabled) : Status(Enabled), Changed(false) {}
      ////////////////////////////////////
      // Data members :
      ////////////////////////////////////
        bool                     Status;
        bool                     Changed;
    };
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    std::map<sf::String, Value> m_Map;
};

}

#endif // ZINTOOLS_SETTINGS_HPP_INCLUDED

