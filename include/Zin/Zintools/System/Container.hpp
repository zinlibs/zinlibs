#ifndef ZINTOOLS_CONTAINER_HPP_INCLUDED
#define ZINTOOLS_CONTAINER_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Container declaration
////////////////////////////////////////////////////////////
template<typename T>
class ZIN_API Container : public std::vector<T*>/*, public sf::NonCopyable*/ {
////////////////////////////////////
  public :
////////////////////////////////////
                  Container(sf::Vector2f pos = sf::Vector2f(0, 0));
                 ~Container();
    void          Add(T &t);
    void          Add(T *t);
    void operator +=(T &t);
    void operator +=(T *t);
    void          Clear();
    virtual void  Move(const sf::Vector2f& shift);
    virtual void  Move(float x, float y);
    virtual void  SetPosition(const sf::Vector2f pos);
    virtual void  SetPosition(float x, float y);
////////////////////////////////////
  protected :
////////////////////////////////////
    virtual void  Set() = 0;
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::Vector2f  m_NextPos;
};

}

#include <Zin/Zintools/System/Container.inl>

#endif // ZINTOOLS_CONTAINER_HPP_INCLUDED
