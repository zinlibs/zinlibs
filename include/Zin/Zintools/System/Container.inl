/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    Please help to support the project at 'https://code.google.com/p/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace zin {

////////////////////////////////////////////////////////////
/// class Container definition
////////////////////////////////////////////////////////////
template <typename T>
Container<T>::Container(sf::Vector2f pos) :
m_NextPos(pos) {
}

template <typename T>
Container<T>::~Container() {
    this->clear();
    for( size_t k(0); k < this->size(); ++k )
        delete this->at(k);
}

template <typename T>
void Container<T>::Add(T& t) {
    push_back(&t);
    Set();
}

template <typename T>
void Container<T>::Add(T *t) {
    push_back(t);
    Set();
}

template <typename T>
void Container<T>::operator+=(T& t) {
    Add(t);
}

template <typename T>
void Container<T>::operator+=(T* t) {
    Add(t);
}

template <typename T>
void Container<T>::Clear() {
    ~Container();
}

template <typename T>
void Container<T>::Move(const sf::Vector2f& shift) {
    m_NextPos+=shift;
    for( size_t k(0); k < this->size(); ++k )
        this->at(k)->Move(shift);
}

template <typename T>
void Container<T>::Move(float dx, float dy) {
    Move(sf::Vector2f(dx, dy));
}

template <typename T>
void Container<T>::SetPosition(sf::Vector2f pos) {
    if( !this->empty() ) {
        sf::Vector2f currentPos = this->front()->GetPosition();
        sf::Vector2f shift;
        shift.x = pos.x-currentPos.x;
        shift.y = pos.y-currentPos.y;
        Move(shift);
    }
    else m_NextPos = pos;
}

template <typename T>
void Container<T>::SetPosition(float x, float y) {
    sf::Vector2f pos(x, y);
    SetPosition(pos);
}

}
