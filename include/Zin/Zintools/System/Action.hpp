#ifndef ZINTOOLS_ACTION_HPP_INCLUDED
#define ZINTOOLS_ACTION_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <Zin/Zintools/System/Singleton.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// struct Action declaration
////////////////////////////////////////////////////////////
class ZIN_API Action {
////////////////////////////////////
  public :
////////////////////////////////////
    Action(const unsigned int Id = 0);
    Action& operator=(const Action& action);
    bool operator==(const Action& action);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    unsigned int Id;
};

////////////////////////////////////////////////////////////
/// class Index declaration
////////////////////////////////////////////////////////////
class ZIN_API ActionIndex : public Singleton<ActionIndex> {
////////////////////////////////////
  public :
////////////////////////////////////
    const Action&               Get(const std::string& String);
    const std::string&          Get(Action action);
    void                        Add(const std::string& String);
    void                        operator+=(const std::string& String);
    const Action&               operator()(const std::string& String);
    const Action&               operator[](const std::string& String);
    void                        Print(Action action);
////////////////////////////////////
  protected :
////////////////////////////////////
    friend class                 Singleton<ActionIndex>;
                                 ActionIndex();
                                ~ActionIndex();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    const std::string             m_UnknowActionNotification;
    unsigned int                  m_ActionCount;
    std::map<std::string, Action> m_Map;
};

////////////////////////////////////////////////////////////
/// class Queue declaration
////////////////////////////////////////////////////////////
class ZIN_API ActionQueue {
////////////////////////////////////
  public :
////////////////////////////////////
                        ActionQueue();
                        ActionQueue(Action action);
    void operator       +=(const ActionQueue& queue);
    void operator       +=(Action action);
    void                Print();
    static ActionQueue  GetEmpty();
    bool                Get(Action& action);
    bool                Empty();
    bool                Contains(const Action& action);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    std::vector<Action> m_Actions;
};

}

#endif // ZINTOOLS_ACTION_HPP_INCLUDED
