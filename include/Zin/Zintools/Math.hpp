#ifndef ZINTOOLS_MATH_HPP_INCLUDED
#define ZINTOOLS_MATH_HPP_INCLUDED

#include <Zin/Zintools/Math/Intersection.hpp>
#include <Zin/Zintools/Math/VectorOperation.hpp>
#include <Zin/Zintools/Math/BasicOperation.hpp>

#endif // ZINTOOLS_MATH_HPP_INCLUDED
