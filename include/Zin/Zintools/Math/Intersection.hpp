#ifndef ZINTOOLS_INTERSECTION_HPP_INCLUDED
#define ZINTOOLS_INTERSECTION_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Zin/Zintools/Math/BasicOperation.hpp>
#include <Zin/Zintools/Math/VectorOperation.hpp>
#include <SFML/System.hpp>

namespace zin {

namespace Math {

bool OnIntersectCircles(const sf::Vector2f& center1, float radius1, const sf::Vector2f& center2, float radius2);
bool OnIntersectSegments(const sf::Vector2f& s1p1, const sf::Vector2f& s1p2, const sf::Vector2f& s2p1, const sf::Vector2f& s2p2);

}

}

#endif // ZINTOOLS_INTERSECTION_HPP_INCLUDED

