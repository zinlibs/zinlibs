#ifndef ZINTOOLS_BASICOPERATION_HPP_INCLUDED
#define ZINTOOLS_BASICOPERATION_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <cmath>

namespace zin {

namespace Math {

bool  IsBelongingTo(float intervalStart, float intervalEnd, float nb);
float Root(float nb);

}

}

#endif // ZINTOOLS_BASICOPERATION_HPP_INCLUDED

