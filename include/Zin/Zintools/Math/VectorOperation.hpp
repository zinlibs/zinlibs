#ifndef ZINTOOLS_VECTOROPERATION_HPP_INCLUDED
#define ZINTOOLS_VECTOROPERATION_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/System.hpp>
#include <cmath>

namespace zin {

namespace Math {

float GetDistance(const sf::Vector2f& point1, const sf::Vector2f& point2);
float GetLength(const sf::Vector2f& vector);

}

}

#endif // ZINTOOLS_VECTOROPERATION_HPP_INCLUDED

