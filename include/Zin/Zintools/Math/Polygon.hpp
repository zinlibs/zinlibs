#ifndef ZINTOOLS_POLYGON_HPP_INCLUDED
#define ZINTOOLS_POLYGON_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>

namespace zin {

namespace Math {

////////////////////////////////////////////////////////////
/// class Polygon declaration
////////////////////////////////////////////////////////////
class Polygon {
////////////////////////////////////
  public :
////////////////////////////////////
    virtual ~Polygon() = 0;
    virtual bool Contains(sf::Vector2f) = 0;
    virtual bool Contains(sf::Vector2i) = 0;
    virtual sf::FloatRect Intersect(sf::FloatRect) = 0;
    virtual sf::IntRect Intersect(sf::IntRect) = 0;
    virtual void Offset(sf::Vector2f) = 0;
    virtual void Offset(sf::Vector2i) = 0;
};

}

}

#endif // ZINTOOLS_POLYGON_HPP_INCLUDED

