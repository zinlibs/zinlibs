#ifndef ZIN_CONFIG_HPP
#define ZIN_CONFIG_HPP

#include <SFML/Config.hpp>

    #if defined(SFML_SYSTEM_WINDOWS)

	#ifdef ZIN_DYNAMIC

	    // Windows platforms
	    #ifdef ZIN_EXPORT

		// From DLL side, we must export
		#define ZIN_API __declspec(dllexport)

	    #else

		// From client application side, we must import
		#define ZIN_API __declspec(dllimport)

	    #endif

	    // For Visual C++ compilers, we also need to turn off this annoying C4251 warning.
	    // You can read lots ot different things about it, but the point is the code will
	    // just work fine, and so the simplest way to get rid of this warning is to disable it
	    #ifdef _MSC_VER

		#pragma warning(disable : 4251)

	    #endif

	#else

	    // No specific directive needed for static build
	    #define ZIN_API

	#endif

    #else

	// Other platforms don't need to define anything
	#define ZIN_API

    #endif
#endif
