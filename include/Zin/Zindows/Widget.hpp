#ifndef ZINDOWS_WIDGET_HPP_INCLUDED
#define ZINDOWS_WIDGET_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zintools/System/Register.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Zindows/Theme.hpp>
#include <Zin/Zintools/Graphics/Drawing.hpp>
#include <Zin/Zindows/Trigger.hpp>
#include <map>
#include <sstream>
#include <typeinfo>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Widget declaration
////////////////////////////////////////////////////////////
class ZIN_API Widget : public sf::NonCopyable {
////////////////////////////////////
  public :
////////////////////////////////////
    virtual            ~Widget();
    const sf::Vector2f& GetPosition();
    const sf::Vector2f& GetSize();
    bool                GetFocus();
    bool                GetLock();
    bool                GetSelection();
    bool                GetVisibility();
    bool                GetActivation();
    Drawing&            GetDrawing();
    virtual ActionQueue Update(Events events);
    virtual void        Move(const sf::Vector2f& shift);
    void                Move(float x, float y);
    void                SetPosition(const sf::Vector2f& pos);
    void                SetPosition(float x, float y);
    void                SetSize(float x, float y);
    virtual void        SetSize(const sf::Vector2f& size);
    void                SetWidth(float width);
    void                SetHeight(float height);
    void                SetMaxSize(float x, float y);
    void                SetMaxSize(const sf::Vector2f& maxSize);
    void                SetFocus(bool focused);
    void                SetLock(bool locked);
    void                SetVisibility(bool visible);
    virtual void        SetActivation(bool activated);
    virtual void        Draw(sf::RenderTarget& target) const;
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    static unsigned int Count;
    static const Action ActionMouseOver;
    static const Action ActionMouseOut;
    static const Action ActionMouseLeftPressed;
    static const Action ActionMouseLeftClicked;
    static const Action ActionMouseLeftReleased;
	static const Action ActionFocused;
	static const Action ActionUnFocused;
	static const Action ActionKeyPressed;
	static const Action ActionKeyReleased;
	static const Action ActionTextEntered;
    static ActionIndex& m_ActionIndex;
    static Theme        m_Theme;
    static Register&    m_Register;
////////////////////////////////////
  protected :
////////////////////////////////////
                        Widget();
    virtual void        BuildDrawing();
    void                Add(Widget& child, const sf::String& childName = "Unnamed");
    void                Remove(const sf::String& childName);
    virtual void        Set(const sf::String& childName);
    Widget&             Get(const sf::String& childName);
    bool                Contains(const sf::String& childName);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    Trigger             m_Trigger;
    sf::Vector2f        m_Size;
    sf::Vector2f        m_MaxSize;
    sf::Vector2f        m_Pos;
    bool                m_IsSelected;
    bool                m_IsFocused; //< Le widget est actif
    bool                m_IsVisible; //< Le widget est visible
    bool                m_IsActivated; //< Le widget est vérouillé
    bool                m_IsLocked;
    sf::Vector2f        m_NextChildPos;
    Drawing             m_Drawing;
////////////////////////////////////
  private :
////////////////////////////////////
    std::map<sf::String, Widget*>  m_Children;
};

}

#endif // ZINDOWS_WIDGET_HPP_INCLUDED

