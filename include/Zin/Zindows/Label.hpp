#ifndef ZINTOOLS_LABEL_HPP_INCLUDED
#define ZINTOOLS_LABEL_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <string>
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/Graphics/TextTools.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Label declaration
////////////////////////////////////////////////////////////
class ZIN_API Label : public sf::Text {
////////////////////////////////////
  public :
////////////////////////////////////
                        Label();
                        Label(const sf::String& string, const sf::Font& font = sf::Font::GetDefaultFont(), unsigned int characterSize = 30);
    void                SetWidth(float x);
    virtual void        SetString(const sf::String& string);
    virtual void        SetCharacterSize(unsigned int size);
    const sf::Vector2f& GetSize();
    float               GetWidth();
    float               GetHeight();
////////////////////////////////////
  protected :
////////////////////////////////////
    void                AutoResize();
    virtual void        Format();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::Vector2f        m_Size;
    std::string         m_UnformatedString;
};

}

#endif // ZINTOOLS_LABEL_HPP_INCLUDED
