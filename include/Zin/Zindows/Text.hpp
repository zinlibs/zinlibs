#ifndef ZINTOOLS_TEXT_HPP_INCLUDED
#define ZINTOOLS_TEXT_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/Graphics/TextTools.hpp>
#include <Zin/Zindows/Label.hpp>
#include <Zin/Zintools/Graphics/Drawing.hpp>
#include <Zin/Zintools/System/Timer.hpp>
#include <Zin/Zindows/Scroller.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Text declaration
////////////////////////////////////////////////////////////
class ZIN_API Text : public Label {
////////////////////////////////////
  public :
////////////////////////////////////
         Text();
         Text(const sf::String& string, const sf::Font& font = sf::Font::GetDefaultFont(), unsigned int characterSize = 30);
    void SetPosition(float px, float py);
    void SetPosition(const sf::Vector2f& pos);
    void SetSize(float x, float y);
    void SetSize(const sf::Vector2f& size);
    void SetCharacterSize(unsigned int size);
    void AddStringAtBegin(const sf::String& string);
    void AddStringAtEnd(const sf::String& string);
    void SelectAll();
    void EraseSelection();
    void Move(float dx, float dy);
    void Move(const sf::Vector2f& shift);
    void SetString(const sf::String& string);
    ActionQueue Update(Events events);

////////////////////////////////////
  protected :
////////////////////////////////////
    void Render(sf::RenderTarget& target, sf::Renderer& renderer) const;
    void BuildSelectionDrawing();
    void Format();
    void ComputeLineHeight();
    size_t GetOnMouseCharNb(Events events) const;
    size_t GetMatchingLine(sf::Vector2f pos) const;
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    struct Range {
        ////////////////////////////////////
          public :
        ////////////////////////////////////
             Range();
             Range(size_t begin, size_t end);
             Range(const Range& p);
        void Sort();
      ////////////////////////////////////
      // Data members :
      ////////////////////////////////////
        size_t             Begin;
        size_t             End;
    };
    bool                   m_IsFormated;
    bool                   m_CarretIsActivated;
    bool                   m_CarretIsShowed;
    sf::Shape              m_CarretDrawing;
    bool                   m_CarretFlashingIsEnabled;
    Timer                  m_CarretTimer;
    unsigned int           m_CharSize;
    const sf::Font&        m_Font;
    float                  m_LineHeight;
    std::vector<Range>     m_LineRangeIndex;
    const sf::Vector2f&    m_Pos;
    std::vector<sf::Shape> m_SelectionDrawing;
    bool                   m_SelectionIsPerformed;
    bool                   m_SelectionIsStarted;
    Range                  m_SelectionRange;
    sf::Color              m_SelectionColor;
    const sf::String&      m_String;
};

}

#endif // ZINTOOLS_TEXT_HPP_INCLUDED
