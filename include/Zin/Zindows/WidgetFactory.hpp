#ifndef ZINDOWS_DEFAULTTHEME_HPP_INCLUDED
#define ZINDOWS_DEFAULTTHEME_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/System.hpp>
#include <Zin/Zintools/Graphics/Drawing.hpp>
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zindows/Menu.hpp>
#include <typeinfo>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class WidgetDrawing declaration
////////////////////////////////////////////////////////////
struct ZIN_API WidgetFactory {
////////////////////////////////////
  public :
////////////////////////////////////
    static void Build(Widget& widget) {
        if( typeid(widget) == typeid(Menu) )
            BuildMenu(widget);
    }
////////////////////////////////////
  protected :
////////////////////////////////////
    static void BuildMenu(Widget& widget);
};

}

#endif // ZINDOWS_DEFAULTTHEME_HPP_INCLUDED
