#ifndef THEME_HPP_INCLUDED
#define THEME_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/System/Singleton.hpp>
#include <Zin/Zintools/Graphics/FontCollection.hpp>
#include <Zin/Zintools/Graphics/ImageCollection.hpp>
#include <Zin/Zintools/Graphics/ColorCollection.hpp>
#include <Zin/Zintools/Graphics/Drawing.hpp>
#include <vector>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Theme declaration
////////////////////////////////////////////////////////////
class ZIN_API Theme {
////////////////////////////////////
  public :
////////////////////////////////////
                    Theme();
                   ~Theme();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    FontCollection  Fonts;
    ImageCollection Images;
    ColorCollection Colors;
    const sf::Image& GetImage(const sf::String& name);
    const sf::Font& GetFont(const sf::String& name);
    const sf::Color& GetColor(const sf::String& name);
////////////////////////////////////
  protected :
////////////////////////////////////
    void             SetImages();
    void             SetFonts();
    void             SetColors();
    std::vector<sf::String> m_ImagesPath;
};

}

#endif // THEME_HPP_INCLUDED
