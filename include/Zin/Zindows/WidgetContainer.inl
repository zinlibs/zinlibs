/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace zin {

////////////////////////////////////////////////////////////
/// class WidgetContainer definition
////////////////////////////////////////////////////////////
template <typename T>
WidgetContainer<T>::WidgetContainer(sf::Vector2f Pos) :
Container<T>(Pos) {
}

template <typename T>
WidgetContainer<T>::~WidgetContainer() {
    this->clear();
    for( size_t k(0); k < this->size(); ++k )
        delete this->at(k);
}

template <typename T>
ActionQueue WidgetContainer<T>::Update(Events events) {
    ActionQueue actions;
    for( size_t k(0); k < this->size(); ++k )
        actions+=this->at(k)->Update(events);
    return actions;
}

template <typename T>
void WidgetContainer<T>::Move(const sf::Vector2f& shift) {
    Container<T>::Move(shift);
}

template <typename T>
void WidgetContainer<T>::Move(float x, float y) {
    sf::Vector2f shift(x, y);
    Container<T>::Move(shift);
}

template <typename T>
void WidgetContainer<T>::SetPosition(const sf::Vector2f& shift) {
    Container<T>::SetPosition(shift);
}

template <typename T>
void WidgetContainer<T>::SetPosition(float x, float y) {
    sf::Vector2f pos(x, y);
    Container<T>::SetPosition(pos);
}

template <typename T>
void WidgetContainer<T>::Draw(sf::RenderTarget& target) const {
    for( size_t k(0); k < this->size(); ++k )
        (*(this->at(k))).Draw(target);
}

}
