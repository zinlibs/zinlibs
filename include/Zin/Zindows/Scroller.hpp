#ifndef ZINDOWS_SCROLLER_HPP_INCLUDED
#define ZINDOWS_SCROLLER_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zindows/Button.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Scroller declaration
////////////////////////////////////////////////////////////
class ZIN_API Scroller : public Widget {
////////////////////////////////////
  public :
////////////////////////////////////
                 Scroller(float length = 100, unsigned int disposition = Horizontal);
                ~Scroller();
    ActionQueue  Update(Events events);
    unsigned int GetStep();
    void         SetSize(const sf::Vector2f& size);
    void         SetLength(float length);
    enum {
        Horizontal,
        Vertical
    };
////////////////////////////////////
  protected :
////////////////////////////////////
    void         BuildDrawing();
    void         ComputeCoeff();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    bool         m_IsFirstScrubberPressed;
    bool         m_IsSecondScrubberPressed;
    bool         m_IsSelectorPressed;
    unsigned int m_Disposition;
    unsigned int m_Step;
    float        m_Coeff;
};

}

#endif // ZINDOWS_SCROLLER_HPP_INCLUDED
