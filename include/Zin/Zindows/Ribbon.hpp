#ifndef ZINDOWS_RIBBON_HPP_INCLUDED
#define ZINDOWS_RIBBON_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Zindows/Text.hpp>
#include <Zin/Zintools/Graphics/Drawing.hpp>
#include <Zin/Zindows/Button.hpp>
#include <Zin/Config.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

namespace zin {

////////////////////////////////////////////////////////////
/// class Ribbon declaration
////////////////////////////////////////////////////////////
class ZIN_API Ribbon : public Widget {
////////////////////////////////////
  public :
////////////////////////////////////
                        Ribbon();
                       ~Ribbon();
    void                SetWidth(float Width);
    void                Add(TextButton& button);
////////////////////////////////////
  protected :
////////////////////////////////////
    virtual void        BuildDrawing();
};

////////////////////////////////////////////////////////////
/// class ConsoleRibbon declaration
////////////////////////////////////////////////////////////
class ZIN_API ConsoleRibbon : public Ribbon {
////////////////////////////////////
  public :
////////////////////////////////////
                       ConsoleRibbon();
    ActionQueue        Update(Events events);
    void               Draw(sf::RenderTarget& target) const;
////////////////////////////////////
  protected :
////////////////////////////////////
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    std::ostringstream    m_Stream;
    zin::Text             m_Text;
    const std::streambuf& m_CoutStreambuf;
};

}


#endif // ZINDOWS_RIBBON_HPP_INCLUDED
