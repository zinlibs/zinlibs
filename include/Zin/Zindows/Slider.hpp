#ifndef SLIDER_HPP_INCLUDED
#define SLIDER_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zindows/Button.hpp>
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Slider declaration
////////////////////////////////////////////////////////////
class ZIN_API Slider : public Widget {
////////////////////////////////////
  public :
////////////////////////////////////
                  Slider(float length = 100, bool isHorizontal = true, unsigned int startStep = 0, unsigned int steps = 0);
                 ~Slider();
    ActionQueue   Update(Events events);
    float         GetStep();
////////////////////////////////////
  protected :
////////////////////////////////////
    virtual void  BuildDrawing();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    bool         m_IsSelectorPressed;
    bool         m_IsHorizontal;
    unsigned int m_StepsNb;
    unsigned int m_Step;
};

}

#endif // SLIDER_HPP_INCLUDED
