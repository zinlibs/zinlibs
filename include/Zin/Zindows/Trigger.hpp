#ifndef ZINDOWS_TRIGGER_HPP_INCLUDED
#define ZINDOWS_TRIGGER_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <Zin/Zintools/System/Container.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Config.hpp>
#include <queue>

namespace zin {

////////////////////////////////////////////////////////////
/// class Trigger declaration
////////////////////////////////////////////////////////////
class ZIN_API Trigger {
////////////////////////////////////
  public :
////////////////////////////////////
                         Trigger();
                         Trigger(float left, float top, float right, float bottom);
    const sf::Vector2f&  GetPosition();
    const sf::Vector2f&  GetSize();
    const sf::FloatRect& GetRect();
    bool                 GetTextEntered(sf::Uint32& charac);
    bool                 GetSpecialKeyEntered(sf::Uint32& charac);
    void                 SetSize(const sf::Vector2f& size);
    void                 SetPosition(const sf::Vector2f& pos);
    void                 SetMouseClickTest(bool enabled);
    void                 Move(const sf::Vector2f& shift);
    void                 Move(float x, float y);
    void                 Reset();
    void                 SetLock(bool locked);
    ActionQueue          Update(Events events);
////////////////////////////////////
  protected :
////////////////////////////////////
    ActionQueue          OnMouseOver(const sf::Vector2f& pos);
    ActionQueue          OnMouseWheel(const sf::Event& event);
    ActionQueue          OnMouseClick(const sf::Event& event);
    ActionQueue          OnCharKey(const sf::Event& event);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::FloatRect        m_Area;
    std::queue<sf::Uint32> m_TextEntered;
    std::queue<sf::Uint32> m_SpecialKeyEntered;
    bool                 m_IsLocked;
    bool                 m_IsCharKeyTested;
    bool                 m_IsMouseClickTested;
    bool                 m_IsMouseOver;
    bool                 m_IsMouseLeftPressed;
    bool                 m_IsMouseLeftPressedOut;
	bool 				 m_IsFocused;
	bool				 m_HasData;
};

}

#endif // ZINDOWS_TRIGGER_HPP_INCLUDED
