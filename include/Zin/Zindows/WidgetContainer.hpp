#ifndef ZINDOWS_WIDGETCONTAINER_HPP_INCLUDED
#define ZINDOWS_WIDGETCONTAINER_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Zintools/System/Container.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class WidgetContainer declaration
////////////////////////////////////////////////////////////
template<typename T>
class ZIN_API WidgetContainer : public Widget, public Container<T> {
////////////////////////////////////
  public :
////////////////////////////////////
                        WidgetContainer(sf::Vector2f Pos = sf::Vector2f(0, 0));
                       ~WidgetContainer();
    virtual ActionQueue Update(Events events);
    virtual void        Move(const sf::Vector2f& shift);
    virtual void        Move(float x, float y);
    virtual void        SetPosition(const sf::Vector2f& shift);
    virtual void        SetPosition(float x, float y);
    virtual void        Draw(sf::RenderTarget& target) const;
};

}

#include <Zin/Zindows/WidgetContainer.inl>

#endif // ZINDOWS_WIDGETCONTAINER_HPP_INCLUDED
