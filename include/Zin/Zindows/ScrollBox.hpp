#ifndef ZINDOWS_SCROLLBOX_HPP_INCLUDED
#define ZINDOWS_SCROLLBOX_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zindows/Scroller.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class ScrollBox declaration
////////////////////////////////////////////////////////////
class ZIN_API ScrollBox : public Widget {
////////////////////////////////////
  public :
////////////////////////////////////
                ~ScrollBox();
    void         Move(const sf::Vector2f& shift);
    virtual void Draw(sf::RenderTarget& target) const;
////////////////////////////////////
  protected :
////////////////////////////////////
                 ScrollBox(float width, float height);
    virtual void BuildDrawing();
    void         Set(const sf::String& childName);
    sf::Vector2f AutoSet(const sf::Vector2f& objectSize);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::View     m_View;
    bool         m_IsVScroller;
    bool         m_IsHScroller;
};

}

#endif // ZINDOWS_SCROLLBOX_HPP_INCLUDED
