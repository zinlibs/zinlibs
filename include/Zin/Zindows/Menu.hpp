#ifndef ZINDOWS_MENU_HPP_INCLUDED
#define ZINDOWS_MENU_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Zin/Zindows/WidgetContainer.hpp>
#include <Zin/Zindows/Slot.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Zindows/List.hpp>
#include <Zin/Zindows/Ribbon.hpp>
#include <Zin/Config.hpp>
#include <iostream>

namespace zin {

////////////////////////////////////////////////////////////
/// class Menu declaration
////////////////////////////////////////////////////////////
class ZIN_API Menu : public WidgetContainer<SlotForMenu> {
////////////////////////////////////
  public :
////////////////////////////////////
                   Menu();
                  ~Menu();
    ActionQueue    Update(Events events);
    void           AddConsoleRibbon();
    void           Draw(sf::RenderTarget& target) const;
////////////////////////////////////
  protected :
////////////////////////////////////
    void           Set();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    unsigned int   m_Bascule;
    bool           m_IsSensitive;
    unsigned int   m_SelectedSlot;
};

}

#endif // ZINDOWS_MENU_HPP_INCLUDED
