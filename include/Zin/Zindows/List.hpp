#ifndef ZINDOWS_LIST_HPP_INCLUDED
#define ZINDOWS_LIST_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <vector>
#include <Zin/Zintools/Graphics/TextTools.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zindows/Trigger.hpp>
#include <Zin/Zindows/Slot.hpp>
#include <Zin/Zintools/Graphics/Drawing.hpp>
#include <Zin/Zintools/System/Action.hpp>
#include <Zin/Zindows/WidgetContainer.hpp>
#include <Zin/Zindows/Events.hpp>
#include <Zin/Config.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class List declaration
////////////////////////////////////////////////////////////
class ZIN_API List : public WidgetContainer<SlotForList> {
////////////////////////////////////
  public :
////////////////////////////////////
                 List();
                ~List();
    void         Move(float x, float y);
    void         Move(const sf::Vector2f& shift);
    void         SetPosition(float x, float y);
    void         SetPosition(const sf::Vector2f& pos);
    ActionQueue  Update(Events events);
    void         Draw(sf::RenderTarget& target) const;
////////////////////////////////////
  protected :
////////////////////////////////////
    void         Set();
    void         BuildDrawing();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    size_t       m_SelectedSlot;
    bool         m_IsSelected;
};

}

#endif // ZINDOWS_LIST_HPP_INCLUDED
