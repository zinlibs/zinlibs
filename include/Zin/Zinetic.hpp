#ifndef ZINETIC_HPP_INCLUDED
#define ZINETIC_HPP_INCLUDED

#include <Zin/Zinetic/Object.hpp>
#include <Zin/Zinetic/Particle.hpp>
#include <Zin/Zinetic/Simulation.hpp>
#include <Zin/Zinetic/Sandbox.hpp>

#endif // ZINETIC_HPP_INCLUDED
