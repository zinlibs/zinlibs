#ifndef ZINETIC_SANDBOX
#define ZINETIC_SANDBOX

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Zin/Zintools.hpp>
#include <SFML/Graphics.hpp>
#include <list>
#include <iostream>
#include <Zin/Zinetic/Object.hpp>
#include <Zin/Zinetic/Particle.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// struct Sandbox declaration
////////////////////////////////////////////////////////////
struct ZIN_API Sandbox {
    sf::FloatRect Rect;
    sf::Shape     Shape;
    float         AttenuationCoeff;
};

}

#endif // ZINETIC_SIMULATION
