#ifndef ZINETIC_PARTICLE_HPP_INCLUDED
#define ZINETIC_PARTICLE_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools.hpp>
#include <iostream>

namespace zin {

////////////////////////////////////////////////////////////
/// class Particle declaration
////////////////////////////////////////////////////////////
class ZIN_API Particle {
////////////////////////////////////
  public :
////////////////////////////////////
                        Particle(const sf::Vector2f& pos, float mass);
    void                Move(float x, float y);
    virtual void        Move(const sf::Vector2f& shift);
    void                SetPosition(float x, float y);
    void                SetPosition(const sf::Vector2f& pos);
    void                SetSpeed(const sf::Vector2f& speed);
    void                SetForce(const sf::Vector2f& force);
    void                SetMass(float mass);
    const sf::Vector2f& GetGravitationForce(const Particle& particle, const float G = 1);
    const sf::Vector2f& GetPosition() const;
    const sf::Vector2f& GetSpeed() const;
    const sf::Vector2f& GetForce() const;
    float               GetMass() const;
    void                Update(float timeLapse);
    virtual void        Draw(sf::RenderTarget& target) const;
////////////////////////////////////
  protected :
////////////////////////////////////
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    float               m_Mass;
    sf::Vector2f        m_Pos;
    sf::Vector2f        m_Speed;
    sf::Vector2f        m_Force;
    sf::Shape           m_Mark;
};

}

#endif // ZINETIC_PARTICLE_HPP_INCLUDED
