#ifndef ZINETIC_OBJECT_HPP_INCLUDED
#define ZINETIC_OBJECT_HPP_INCLUDED

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <Zin/Zintools.hpp>
#include <Zin/Zinetic/Particle.hpp>
#include <Zin/Zinetic/Sandbox.hpp>
#include <iostream>

namespace zin {

////////////////////////////////////////////////////////////
/// class Object declaration
////////////////////////////////////////////////////////////
class ZIN_API Object : public Particle {
////////////////////////////////////
  public :
////////////////////////////////////
    virtual void OnSandboxCollide(const Sandbox& sandbox) = 0;
    virtual void Move(const sf::Vector2f& shift);
    virtual void SetColor(const sf::Color& color);
    void Draw(sf::RenderTarget& target) const;
    virtual void SetImage(const sf::Image& image);
////////////////////////////////////
  protected :
////////////////////////////////////
    Object(const sf::Vector2f& pos, float mass);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::Shape m_Shell;
    sf::Sprite* m_Sprite;
    float m_Rotation;
};

////////////////////////////////////////////////////////////
/// class CircleObject declaration
////////////////////////////////////////////////////////////
class CircleObject : public Object {
////////////////////////////////////
  public :
////////////////////////////////////
    CircleObject(const sf::Vector2f& pos, float mass, float radius);
    void OnSandboxCollide(const Sandbox& sandbox);
    bool OnCollide(CircleObject& incoming, bool applyCollision = false);
    float GetRadius() const;
    void SetImage(const sf::Image& image);
////////////////////////////////////
  protected :
////////////////////////////////////
    void ComputeCollision(CircleObject& incoming);
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    float m_Radius;
};

}

#endif // ZINETIC_OBJECT_HPP_INCLUDED
