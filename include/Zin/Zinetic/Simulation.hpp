#ifndef ZINETIC_SIMULATION
#define ZINETIC_SIMULATION

////////////////////////////////////////////////////////////
/// Headers
////////////////////////////////////////////////////////////
#include <Zin/Zintools.hpp>
#include <SFML/Graphics.hpp>
#include <list>
#include <vector>
#include <iostream>
#include <Zin/Zinetic/Object.hpp>
#include <Zin/Zinetic/Particle.hpp>
#include <Zin/Zinetic/Sandbox.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Simulation declaration
////////////////////////////////////////////////////////////
class ZIN_API Simulation {
////////////////////////////////////
  public :
////////////////////////////////////
         Simulation(float width, float height);
        ~Simulation();
    void Move(float x, float y);
    void Move(const sf::Vector2f& shift);
    void SetPosition(float x, float y);
    void SetPosition(const sf::Vector2f& pos);
    void SetSandbox(bool enable, sf::FloatRect sandboxRect = sf::FloatRect());
    void SetObjectsImage(const sf::Image& image);
    void Resize(float x, float y);
    void Resize(const sf::Vector2f& size);
    void SetRunning(bool running);
    void SetZoom(unsigned int Zoom);
    void SetGravityPoint(bool enable, const sf::Vector2f& mousPos = sf::Vector2f(0, 0), float mass = 8000);
    void SetGravitation(bool gravitation);
    void SetCollisions(bool enable);
    const sf::View& GetView();
    void ZoomIn();
    void ZoomOut();
    void ResetZoom();
    void Clear();
    void Restart();
    void Update();
    void Draw(sf::RenderTarget& target);
    void Randomize(size_t ObjectNb = 100, float maxMass = 1000);
////////////////////////////////////
  protected :
////////////////////////////////////
    void ComputeCollisions();
    void ComputeGravity();
  ////////////////////////////////////
  // Data members :
  ////////////////////////////////////
    sf::Vector2f             m_Size;
    sf::Vector2f             m_Pos;
    std::list<CircleObject*> m_Objects;
    zin::Timer               m_Clock;
    sf::View                 m_View;
    Particle*                m_GravityPoint;
    Sandbox*                 m_Sandbox;
    bool                     m_IsCollisions;
    bool                     m_IsGravitation;
    unsigned int             m_Seed;
    unsigned int             m_ZoomLevel;
    bool                     m_IsCollisionSuggested;
};

}

#endif // ZINETIC_SIMULATION
