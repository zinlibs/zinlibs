.PHONY: clean samples


all: Zintools Zindows Zinetic

install: Zindows_install Zintools_install Zinetic_install

Zintools:
	(cd build/Zintools && cmake -G "Unix Makefiles" && make)

Zintools_install: Zintools
	(cd build/Zintools && make install)

Zindows: Zintools
	(cd build/Zindows && cmake -G "Unix Makefiles" && make)

Zindows_install: Zindows
	(cd build/Zindows &&  make install)

Zinetic: Zintools
	(cd build/Zinetic && cmake -G "Unix Makefiles" && make)

Zinetic_install: Zinetic
	(cd build/Zinetic && make install)


samples: 
	(cd build/Samples && make all)



clean:
	(cd build/Zinetic && make clean)
	(cd build/Zintools && make clean)
	(cd build/Zindows && make clean)
	
