/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Theme.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Theme definition
////////////////////////////////////////////////////////////
Theme::Theme() {
    SetImages();
    SetFonts();
    SetColors();
    std::cout << "[zindows] Theme created (" << this << ")" << std::endl;
}

Theme::~Theme() {
    std::cout << "[zindows] Theme deleted (" << this << ")" << std::endl;
}

const sf::Image& Theme::GetImage(const sf::String& name) {

}

const sf::Font& Theme::GetFont(const sf::String& name) {

}

const sf::Color& Theme::GetColor(const sf::String& name) {

}

void Theme::SetImages() {
    std::cout << "[zindows] Loading images... ";
    sf::Image* ImageCheck = new sf::Image;
    ImageCheck->Create(6, 8, sf::Color(0, 0, 0, 0));
    ImageCheck->SetPixel(0, 3, sf::Color::Black);
    ImageCheck->SetPixel(0, 4, sf::Color::Black);
    ImageCheck->SetPixel(0, 5, sf::Color::Black);
    ImageCheck->SetPixel(1, 4, sf::Color::Black);
    ImageCheck->SetPixel(1, 5, sf::Color::Black);
    ImageCheck->SetPixel(1, 6, sf::Color::Black);
    ImageCheck->SetPixel(2, 5, sf::Color::Black);
    ImageCheck->SetPixel(2, 6, sf::Color::Black);
    ImageCheck->SetPixel(2, 7, sf::Color::Black);
    ImageCheck->SetPixel(3, 3, sf::Color::Black);
    ImageCheck->SetPixel(3, 4, sf::Color::Black);
    ImageCheck->SetPixel(3, 5, sf::Color::Black);
    ImageCheck->SetPixel(3, 6, sf::Color::Black);
    ImageCheck->SetPixel(4, 1, sf::Color::Black);
    ImageCheck->SetPixel(4, 2, sf::Color::Black);
    ImageCheck->SetPixel(4, 3, sf::Color::Black);
    ImageCheck->SetPixel(4, 4, sf::Color::Black);
    ImageCheck->SetPixel(5, 0, sf::Color::Black);
    ImageCheck->SetPixel(5, 1, sf::Color::Black);
    ImageCheck->SetPixel(5, 2, sf::Color::Black);
    Images.Get("Check", ImageCheck);
    Images.Get("SliderSelectorH",        "datas/SliderSelectorH.tga");
    Images.Get("SliderSelectorV",        "datas/SliderSelectorV.tga");
    Images.Get("SliderSelectorHPressed", "datas/SliderSelectorHPressed.tga");
    Images.Get("SliderSelectorVPressed", "datas/SliderSelectorVPressed.tga");
    Images.Get("ScrubberLeft",           "datas/ScrubberLeft.tga");
    Images.Get("ScrubberLeftPressed",    "datas/ScrubberLeftPressed.tga");
    Images.Get("ScrubberRight",          "datas/ScrubberRight.tga");
    Images.Get("ScrubberRightPressed",   "datas/ScrubberRightPressed.tga");
    Images.Get("ScrubberUp",             "datas/ScrubberUp.tga");
    Images.Get("ScrubberUpPressed",      "datas/ScrubberUpPressed.tga");
    Images.Get("ScrubberDown",           "datas/ScrubberDown.tga");
    Images.Get("ScrubberDownPressed",    "datas/ScrubberDownPressed.tga");
    Images.Get("ScrollButtonV",          "datas/ScrollButtonV.tga");
    Images.Get("ScrollButtonVPressed",   "datas/ScrollButtonVPressed.tga");
    Images.Get("ScrollButtonH",          "datas/ScrollButtonH.tga");
    Images.Get("ScrollButtonHPressed",   "datas/ScrollButtonHPressed.tga");
    m_ImagesPath.push_back("SliderSelectorH.tga");
    m_ImagesPath.push_back("SliderSelectorV.tga");
    m_ImagesPath.push_back("ScrubberLeft.tga");
    m_ImagesPath.push_back("ScrubberRight.tga");
    m_ImagesPath.push_back("ScrubberUp.tga");
    m_ImagesPath.push_back("ScrubberDown.tga");
    m_ImagesPath.push_back("ScrollButtonV.tga");
    m_ImagesPath.push_back("ScrollButtonH.tga");
    std::cout << "Done" << std::endl;
}

void Theme::SetFonts() {
    std::cout << "[zindows] Loading fonts... ";
    std::cout << "Done" << std::endl;
}

void Theme::SetColors() {
    std::cout << "[zindows] Loading colors... ";
    Colors.Get("SlotMouseOutBodyColor",          new sf::Color(  0,   0,   0,   0));
    Colors.Get("SlotMouseOverBodyColor",         new sf::Color(255, 255, 255));
    Colors.Get("SlotMouseOutLabelColor",         new sf::Color(255, 255, 255));
    Colors.Get("SlotMouseOverLabelColor",        new sf::Color(  0,   0,   0));
    Colors.Get("SlotForMenuMouseOutBodyColor",   new sf::Color(  0,   0,   0,   0));
    Colors.Get("SlotForMenuMouseOverBodyColor",  new sf::Color(255, 255, 255));
    Colors.Get("SlotForMenuMouseOutLabelColor",  new sf::Color(255, 255, 255));
    Colors.Get("SlotForMenuMouseOverLabelColor", new sf::Color(  0,   0,   0));
    Colors.Get("SlotForListMouseOutBodyColor",   new sf::Color(  0,   0,   0,   0));
    Colors.Get("SlotForListMouseOverBodyColor",  new sf::Color(255,   0,   0, 140));
    Colors.Get("SlotForListMouseOutLabelColor",  new sf::Color(  0,   0,   0));
    Colors.Get("SlotForListMouseOverLabelColor", new sf::Color(255, 255, 255));
    Colors.Get("TopBorderColor",                 new sf::Color(196, 196, 196));
    Colors.Get("MedianBorderColor",              new sf::Color(161, 161, 161));
    Colors.Get("DownBorderColor",                new sf::Color(118, 118, 118));
    Colors.Get("TextBoxBody",                    new sf::Color(255, 255, 255, 100));
    Colors.Get("TextBoxText",                    new sf::Color(255, 255, 255));
    std::cout << "Done" << std::endl;
}

}
