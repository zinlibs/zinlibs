/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Trigger.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Trigger definition
////////////////////////////////////////////////////////////
Trigger::Trigger() :
m_IsMouseOver(false),
m_IsMouseLeftPressed(false),
m_IsCharKeyTested(false),
m_IsLocked(false) {
    m_Area.Left = 0;
    m_Area.Top = 0;
    m_Area.Width = 100;
    m_Area.Height = 100;
}

Trigger::Trigger(float left, float top, float width, float height) :
m_IsMouseOver(false),
m_IsMouseLeftPressed(false),
m_IsCharKeyTested(false) {
    m_Area.Left = left;
    m_Area.Top = top;
    m_Area.Width = width;
    m_Area.Height = height;
}

void Trigger::Reset() {
    m_IsMouseOver = false;
    m_IsMouseLeftPressed = false;
}

const sf::Vector2f& Trigger::GetPosition() {
    return sf::Vector2f(m_Area.Left, m_Area.Top);
}

const sf::Vector2f& Trigger::GetSize() {
    return sf::Vector2f(m_Area.Width, m_Area.Height);
}

const sf::FloatRect& Trigger::GetRect() {
    return m_Area;
}

bool Trigger::GetTextEntered(sf::Uint32& charac) {
    if(m_TextEntered.size() == 0)
        return false;
    charac = m_TextEntered.front();
    m_TextEntered.pop();
    return true;
}

bool Trigger::GetSpecialKeyEntered(sf::Uint32& charac) {
    if(m_SpecialKeyEntered.size() == 0)
        return false;
    charac = m_SpecialKeyEntered.front();
    m_SpecialKeyEntered.pop();
    return true;
}

void Trigger::Move(const sf::Vector2f& shift) {
    Move(shift.x, shift.y);
}

void Trigger::Move(float dx, float dy) {
    m_Area.Left+=dx;
    m_Area.Top+=dy;
}

void Trigger::SetPosition(const sf::Vector2f& pos) {
    m_Area.Left = pos.x;
    m_Area.Top  = pos.y;
}

void Trigger::SetSize(const sf::Vector2f& size) {
    if( size.x > 0 && size.y > 0 ) {}
    m_Area.Width  = size.x;
    m_Area.Height = size.y;
}

void Trigger::SetLock(bool locked) {
    m_IsLocked = locked;
}

void Trigger::SetMouseClickTest(bool enabled) {
    m_IsMouseClickTested = enabled;
}

ActionQueue Trigger::Update(Events events) {
    ActionQueue actions;
    if( !m_IsLocked ) {
        actions = OnMouseOver(events.MousePos);
        sf::Event event;
        while( events.Get(event) ) {
            if( m_IsMouseClickTested )
                actions+=OnMouseClick(event);
            if( m_IsCharKeyTested ) {
                OnCharKey(event);
            }
        }
        actions+=OnMouseWheel(event);
    }
    return actions;
}

ActionQueue Trigger::OnMouseOver(const sf::Vector2f& pos) {
    ActionQueue actions;
    zin::ActionIndex& ActionIndex = *zin::ActionIndex::GetSingleton();
    bool isMouseOver = m_Area.Contains(pos.x, pos.y);
    if( isMouseOver != m_IsMouseOver ) {
        actions+=(isMouseOver ? ActionIndex["MouseOver"] : ActionIndex["MouseOut"]);
        m_IsMouseOver = isMouseOver;
    }
    return actions;
}

ActionQueue Trigger::OnMouseWheel(const sf::Event& event) {
    ActionQueue actions;
    zin::ActionIndex& ActionIndex = *zin::ActionIndex::GetSingleton();
    if( m_IsMouseOver && event.Type == sf::Event::MouseWheelMoved) {
        if(event.MouseWheel.Delta == 1)
            actions+=ActionIndex["MouseWheelUp"];
        else
            actions+=ActionIndex["MouseWheelDown"];
    }

    return actions;
}

ActionQueue Trigger::OnMouseClick(const sf::Event& event) {
    ActionQueue actions;
    zin::ActionIndex& ActionIndex = *(zin::ActionIndex::GetSingleton());
    if( event.MouseButton.Button == sf::Mouse::Left ) {
        if( event.Type == sf::Event::MouseButtonPressed && m_IsMouseLeftPressed == false ) {
                m_IsMouseLeftPressed = true;
                actions+=ActionIndex["MouseLeftPressed"];
                if( m_IsMouseOver )
                    actions+=ActionIndex["MouseLeftPressedInside"];
                else actions+=ActionIndex["MouseLeftPressedOutside"];
        }
        if( event.Type == sf::Event::MouseButtonReleased && m_IsMouseLeftPressed == true ) {
            m_IsMouseLeftPressed = false;
                actions+=ActionIndex["MouseLeftReleased"];
                    actions+=ActionIndex["MouseLeftClicked"];
            if( m_IsMouseOver )
                actions+=ActionIndex["MouseLeftReleasedInside"];
            else actions+=ActionIndex["MouseLeftReleasedOutside"];
        }
    }
    return actions;
}

ActionQueue Trigger::OnCharKey(const sf::Event& event) {
	ActionQueue actions;
	zin::ActionIndex& ActionIndex = *(zin::ActionIndex::GetSingleton());
    if( event.Type == sf::Event::TextEntered) {
        const sf::String charset( L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890,.-;:_+#*'~^°!\"§$%&/()=?`²³{[]}\\\x00df\x00e4\x00f6\x00fc\x00c4\x00d6\x00dc<>|@µ\x20ac " );

        if (charset.Find(event.Text.Unicode) != sf::String::InvalidPos) {
             m_TextEntered.push(event.Text.Unicode);
             actions+=ActionIndex["TextEntered"];
        }
        else {
             m_SpecialKeyEntered.push(event.Text.Unicode);
             actions+=ActionIndex["SpecialKeyEntered"];        
        }
    }
	return actions;
}

}
