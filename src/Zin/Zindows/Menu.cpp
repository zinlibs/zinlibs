/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Menu.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Menu definition
////////////////////////////////////////////////////////////
Menu::Menu() :
m_IsSensitive(false),
m_Bascule(0),
m_SelectedSlot(0) {
    m_Trigger.SetMouseClickTest(true);
    SetSize(800, 21);
    std::cout << "[Zindows] Menu created (" << this << ")" << std::endl;
}

Menu::~Menu() {
    std::cout << "[Zindows] Menu deleted (" << this << ")" << std::endl;
}

void Menu::Set() {
    back()->Widget::Move(m_NextPos.x, 1);
    m_NextPos.x+=(back()->GetSize()).x;
    m_NextPos.x++;
}

ActionQueue Menu::Update(Events events) {
    ActionQueue actions;
    if( m_IsSensitive ) {
        size_t SelectedSlot = 0;
        for( size_t k(0); k < size(); ++k ) {
            ActionQueue ac = at(k)->Update(events);
            if( ac.Contains(Widget::ActionMouseOver) ) {
                SelectedSlot = k + 1;
                if( m_SelectedSlot != SelectedSlot ) {
                    if( m_SelectedSlot > 0 ) {
                        at(m_SelectedSlot-1)->SetLock(false);
                        m_SelectedSlot = 0;
                    }
                    at(SelectedSlot-1)->SetLock(true);
                    m_SelectedSlot = SelectedSlot;
                }
            }
            else if( !ac.Contains(Widget::ActionMouseOut) )
                actions+=ac;
        }
    }
    ActionQueue internalActions = m_Trigger.Update(events);
    /// Enfoncement 1 :
    if( m_Bascule == 0 && internalActions.Contains(m_ActionIndex.Get("MouseLeftPressedInside")) ) {
        m_Bascule = 1;
        m_IsSensitive = true;
    }
    /// Relachement 1 :
    else if( m_Bascule == 1 && internalActions.Contains(m_ActionIndex.Get("MouseLeftReleased")) )
        m_Bascule = 2;
    /// Enfoncement 2 :
    else if( m_Bascule == 2 && (internalActions.Contains(Widget::ActionMouseLeftPressed)
          || internalActions.Contains(m_ActionIndex.Get("MouseLeftPressed"))) )
        m_Bascule = 3;
    /// Relachement 2 :
    if( m_Bascule == 3 && internalActions.Contains(m_ActionIndex.Get("MouseLeftReleased"))/*internalActions.Contains(Widget::ActionMouseLeftClicked)*/) {
        m_Bascule = 0;
        m_IsSensitive = false;
        for( size_t k(0); k < size(); ++k ) {
            at(k)->ForceMouseOut();
            at(k)->SetLock(false);
        }
        m_SelectedSlot = 0;
    }
    return actions;
}

void Menu::Draw(sf::RenderTarget& target) const {
    target.Draw(m_Drawing);
    WidgetContainer<SlotForMenu>::Draw(target);
}

}
