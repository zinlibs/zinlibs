/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Widget.hpp>
#include <Zin/Zindows/WidgetFactory.hpp>


namespace zin {

////////////////////////////////////////////////////////////
/// class Widget definition
////////////////////////////////////////////////////////////
Widget::Widget() :
m_IsSelected(false),
m_IsFocused(false),
m_IsVisible(true),
m_IsActivated(true),
m_IsLocked(false) {
    Count++;
    BuildDrawing();
}

////////////////////////////////////////////////////////////
Widget::~Widget() {
    Count--;
    std::map<sf::String, Widget*>::iterator it = m_Children.begin();
    m_Children.clear();
    if( Count == 0 ) {
        std::cout << "[zindows] Memory correctly released" << std::endl;
    }
}

////////////////////////////////////////////////////////////
const sf::Vector2f& Widget::GetPosition() {
    return m_Pos;
}

////////////////////////////////////////////////////////////
const sf::Vector2f& Widget::GetSize() {
    return m_Size;
}

////////////////////////////////////////////////////////////
bool Widget::GetFocus() {
    return m_IsFocused;
}

////////////////////////////////////////////////////////////
bool Widget::GetLock() {
    return m_IsLocked;
}

////////////////////////////////////////////////////////////
bool Widget::GetSelection() {
    return m_IsSelected;
}

bool Widget::GetVisibility() {
    return m_IsVisible;
}

////////////////////////////////////////////////////////////
bool Widget::GetActivation() {
    return m_IsActivated;
}

////////////////////////////////////////////////////////////
Drawing& Widget::GetDrawing() {
    return m_Drawing;
}

////////////////////////////////////////////////////////////
void Widget::Move(const sf::Vector2f& shift) {
    m_Pos+=shift;
    m_Drawing.Move(shift);
    m_Trigger.Move(shift);
    for( std::map<sf::String, Widget*>::iterator it = m_Children.begin(); it != m_Children.end(); ++it )
        (*(*it).second).Move(shift);
    //BuildDrawing();
}

////////////////////////////////////////////////////////////
void Widget::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

////////////////////////////////////////////////////////////
void Widget::SetPosition(const sf::Vector2f& pos) {
    Move(pos-m_Pos);
}

////////////////////////////////////////////////////////////
void Widget::SetPosition(float x, float y) {
    SetPosition(sf::Vector2f(x, y));
}

////////////////////////////////////////////////////////////
void Widget::SetSize(const sf::Vector2f& size) {
    /*sf::Vector2f correctSize;
    if( m_MaxSize.x != 0 && m_MaxSize.y != 0 ) {
        if( size.x > m_MaxSize.x )
            correctSize.x = m_MaxSize.x;
        if( size.y > m_MaxSize.y )
            correctSize.y = m_MaxSize.y;
    }*/
    m_Size = size/*correctSize*/;
    m_Trigger.SetSize(m_Size);
    BuildDrawing();
}

////////////////////////////////////////////////////////////
void Widget::SetSize(float x, float y) {
    SetSize(sf::Vector2f(x, y));
}

////////////////////////////////////////////////////////////
void Widget::SetMaxSize(const sf::Vector2f& maxSize) {
    m_MaxSize = maxSize;
}

////////////////////////////////////////////////////////////
void Widget::SetMaxSize(float x, float y) {
    SetMaxSize(sf::Vector2f(x, y));
}

////////////////////////////////////////////////////////////
void Widget::SetWidth(float width) {
    SetSize(width, m_Size.y);
}

////////////////////////////////////////////////////////////
void Widget::SetHeight(float height) {
    SetSize(m_Size.x, height);
}

////////////////////////////////////////////////////////////
void Widget::SetFocus(bool focused) {
    m_IsFocused = focused;
}

////////////////////////////////////////////////////////////
void Widget::SetLock(bool locked) {
    m_IsLocked = locked;
    m_Trigger.SetLock(locked);
}

////////////////////////////////////////////////////////////
void Widget::SetVisibility(bool visible) {
    m_IsVisible = visible;
}

////////////////////////////////////////////////////////////
void Widget::SetActivation(bool activated) {
    m_IsActivated = activated;
}

////////////////////////////////////////////////////////////
void Widget::BuildDrawing() {
    m_Drawing.Clear();
    WidgetFactory::Build(*this);
}

////////////////////////////////////////////////////////////
void Widget::Set(const sf::String& childName) {
}

////////////////////////////////////////////////////////////
void Widget::Add(Widget& child, const sf::String& childName) {
    const sf::String& type = typeid(child).name();
    sf::String name = childName;
/// Nommage automatique du widget anonyme
    if( name == "Unnamed" ) {
        size_t typeCount = 0;
        std::map<sf::String, Widget*>::iterator it = m_Children.begin();
        while( it != m_Children.end() ) {
            if( type == typeid(*(*it).second).name() )
                ++typeCount;
            ++it;
        }
        std::ostringstream oss;
        oss << typeCount;
        name = type + oss.str();
    }
/// Insertion de l'objet
    std::map<sf::String, Widget*>::iterator it = m_Children.find(name);
    if( it == m_Children.end() ) {
        m_Children.insert(std::pair<sf::String, Widget*>(name, &child));
        Set(childName);
        std::cout << "[Zindows] " << type.ToAnsiString() << " " << name.ToAnsiString() << " (" << &child << ") added in " << typeid(*this).name() << " (" << this << ")" << std::endl;
    }
    else { // Remplacement de l'objet déjà existant
        (*it).second = &child;
        Set(childName);
        std::cout << "[Zindows] " << type.ToAnsiString() << " " << name.ToAnsiString() << " (" << &child << ") added in " << typeid(*this).name() << " (" << this << ")" << std::endl;
    }
}

////////////////////////////////////////////////////////////
void Widget::Remove(const sf::String& childName) {
    std::map<sf::String, Widget*>::iterator it = m_Children.find(childName);
    if( it != m_Children.end() ) {
        delete (*it).second;
        m_Children.erase(it);
    }
}

////////////////////////////////////////////////////////////
Widget& Widget::Get(const sf::String& childName) {
    std::map<sf::String, Widget*>::iterator it = m_Children.find(childName);
    if( it != m_Children.end() )
        return *(*it).second;
    else {
        std::cout << "[Zindows] Fatal error : No child named " << childName.ToAnsiString() << " in " << typeid(*this).name() << " (" << this << ") !" << std::endl;
        exit(0);
    }
}

////////////////////////////////////////////////////////////
bool Widget::Contains(const sf::String& childName) {
    std::map<sf::String, Widget*>::iterator it = m_Children.find(childName);
    return it != m_Children.end();
}

////////////////////////////////////////////////////////////
void Widget::Draw(sf::RenderTarget& target) const {
    if( m_IsVisible ) {
        target.Draw(m_Drawing);
        for( std::map<sf::String, Widget*>::const_iterator it = m_Children.begin(); it != m_Children.end(); ++it )
            (*(*it).second).Draw(target);
    }
}

////////////////////////////////////////////////////////////
ActionQueue Widget::Update(Events events) {
    ActionQueue actions;
    for( std::map<sf::String, Widget*>::iterator it = m_Children.begin(); it != m_Children.end(); ++it )
        actions+=(*(*it).second).Update(events);
    return actions;
}

////////////////////////////////////////////////////////////
unsigned int       Widget::Count =                  0;
Theme              Widget::m_Theme;
Register&          Widget::m_Register =             *Register::GetSingleton();
ActionIndex&       Widget::m_ActionIndex =          *ActionIndex::GetSingleton();
const Action       Widget::ActionMouseOver =         m_ActionIndex.Get("MouseOver");
const Action       Widget::ActionMouseOut =          m_ActionIndex.Get("MouseOut");
const Action       Widget::ActionMouseLeftPressed =  m_ActionIndex.Get("MouseLeftPressed");
const Action       Widget::ActionMouseLeftClicked =  m_ActionIndex.Get("MouseLeftClicked");
const Action       Widget::ActionMouseLeftReleased = m_ActionIndex.Get("MouseLeftReleased");
const Action       Widget::ActionFocused = 			 m_ActionIndex.Get("Focused");
const Action       Widget::ActionUnFocused =         m_ActionIndex.Get("UnFocused");
const Action       Widget::ActionKeyPressed = 		 m_ActionIndex.Get("KeyPressed");
const Action       Widget::ActionKeyReleased =       m_ActionIndex.Get("KeyReleased");
const Action       Widget::ActionTextEntered = 		 m_ActionIndex.Get("TextEntered");

}
