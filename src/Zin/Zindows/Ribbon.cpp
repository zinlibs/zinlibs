/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Ribbon.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Ribbon definition
////////////////////////////////////////////////////////////
Ribbon::Ribbon() {
    SetSize(800, 140);
    BuildDrawing();
    std::cout << "[Zindows] Ribbon created (" << this << ")" << std::endl;
}

Ribbon::~Ribbon() {
    std::cout << "[Zindows] Ribbon deleted (" << this << ")" << std::endl;
}

void Ribbon::SetWidth(float Width) {
    m_Size.x = Width;
    m_Drawing.Clear();
    BuildDrawing();
}

void Ribbon::BuildDrawing() {
    m_Drawing.Clear();
    sf::Shape gradient;
      gradient.AddPoint(       0, 0,                                                   sf::Color::White);
      gradient.AddPoint(m_Size.x, 0,                                                   sf::Color::White);
      gradient.AddPoint(m_Size.x, 30,                                                  sf::Color(255, 255, 255, 200));
      gradient.AddPoint(       0, 30,                                                  sf::Color(255, 255, 255, 200));
    sf::Shape body      = sf::Shape::Rectangle(0, 30,         m_Size.x, m_Size.y-30-5, sf::Color(255, 255, 255, 200));
    sf::Shape topLineBorder  = sf::Shape::Line(0, m_Size.y-4, m_Size.x, m_Size.y-4, 1, sf::Color(196, 196, 196));
    sf::Shape border    = sf::Shape::Rectangle(0, m_Size.y-4, m_Size.x, 3,             sf::Color(160, 160, 160));
    sf::Shape downLineBorder = sf::Shape::Line(0, m_Size.y,   m_Size.x, m_Size.y, 1,   sf::Color(132, 132, 132));
    m_Drawing+=gradient;
    m_Drawing+=body;
    m_Drawing+=topLineBorder;
    m_Drawing+=border;
    m_Drawing+=downLineBorder;
    Move(0, 21);
}

void Ribbon::Add(TextButton& button) {
    Widget::Add(button);
}

////////////////////////////////////////////////////////////
/// class ConsoleRibbon definition
////////////////////////////////////////////////////////////
ConsoleRibbon::ConsoleRibbon() :
Ribbon(),
m_CoutStreambuf(*std::cout.rdbuf()) {
    m_Text.Move(0, 21);
    m_Text.SetSize(sf::Vector2f(m_Size.x, m_Size.y - 5));
    m_Text.SetColor(sf::Color::Black);
    m_Text.SetCharacterSize(13);
    std::cout.rdbuf(m_Stream.rdbuf());
}

ActionQueue ConsoleRibbon::Update(Events events) {
    ActionQueue Actions;
    std::string string = m_Stream.str();
    m_Stream.str("");
    if( !string.empty() )
        m_Text.AddStringAtBegin(string);
    m_Text.SetSize(m_Size.x, m_Size.y-5);
    return Actions;
}

void ConsoleRibbon::Draw(sf::RenderTarget& target) const {
    Widget::Draw(target);
    target.Draw(m_Text);
}

}
