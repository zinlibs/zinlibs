/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Button.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Button definition
////////////////////////////////////////////////////////////
Button::Button(const sf::String& type) :
m_IsPressed(false) {
    m_Trigger.SetMouseClickTest(true);
}

Button::~Button() {
    std::cout << "[Zindows] Button deleted (" << this << ")" << std::endl;
}

void Button::SetAction(const ActionQueue action) {
    m_Actions = action;
}

void Button::SetAction(const Action& action) {
    m_Actions+=action;
}

ActionQueue Button::Update(Events events) {
    ActionQueue actions;
    ActionQueue triggerActions = m_Trigger.Update(events);
    if( !triggerActions.Empty() ) {
        if( triggerActions.Contains(Widget::ActionMouseOver) ) {
            actions+=m_ActionIndex["ButtonOvered"];
            m_IsSelected = true;
        }
        if( triggerActions.Contains(Widget::ActionMouseOut) ) {
            actions+=m_ActionIndex["ButtonLeaved"];
            m_IsSelected = false;
        }
        if( triggerActions.Contains(Widget::ActionMouseLeftPressed) && m_IsSelected ) {
            m_IsPressed = true;
            BuildDrawing();
            actions+=m_ActionIndex["ButtonPressed"];
        }
        if( triggerActions.Contains(Widget::ActionMouseLeftClicked) && m_IsPressed ) {
            actions = m_Actions;
            m_IsPressed = false;
            BuildDrawing();
            actions+=m_ActionIndex["ButtonReleased"];
        }
    }
    return actions;
}

////////////////////////////////////////////////////////////
/// class TextButton definition
////////////////////////////////////////////////////////////
TextButton::TextButton(const std::string& string) :
Button("TextButton") {
    std::cout << "[Zindows] TextButton created (" << this << ")" << std::endl;
    m_Text.SetString(string);
    m_Text.SetCharacterSize(14);
    m_Text.SetColor(sf::Color::Black);
    m_Text.Move(5, 4);
    SetSize(sf::Vector2f(TextTools::GetWidth(m_Text)+10, 24));
    BuildDrawing();
}

void TextButton::BuildDrawing() {
    m_Drawing.Clear();
/// Rect1 :
    sf::Color color1 = m_IsPressed ? sf::Color(186, 186, 186) : sf::Color(69, 69, 69);
    m_Drawing+=sf::Shape::Rectangle(0, 0, m_Size.x, m_Size.y, color1);
/// Rect2 :
    sf::Color color2 = m_IsPressed ? sf::Color(117, 117, 117) : sf::Color(138, 138, 138);
    m_Drawing+=sf::Shape::Rectangle(1, 1, m_Size.x - 2, m_Size.y - 2, color2);
/// Rect3 :
    sf::Color color3 = m_IsPressed ? sf::Color(0, 0, 0) : sf::Color(255, 255, 255);
    m_Drawing+=sf::Shape::Rectangle(1, 1, m_Size.x - 3, m_Size.y - 3, color3);
/// Rect4 :
    sf::Color color4 = m_IsPressed ? sf::Color(82, 82, 82) : sf::Color(173, 173, 173);
    m_Drawing+=sf::Shape::Rectangle(2, 2, m_Size.x - 4, m_Size.y - 4, color4);
/// Rect5 :
    sf::Color color5 = m_IsPressed ? sf::Color(28, 28, 28) : sf::Color(227, 227, 227);
    m_Drawing+=sf::Shape::Rectangle(2, 2, m_Size.x - 5, m_Size.y - 5, color5);
/// Rect6 :
    sf::Color color6 = m_IsPressed ? sf::Color(51, 51, 51) : sf::Color(204, 204, 204);
    m_Drawing+=sf::Shape::Rectangle(3, 3, m_Size.x - 6, m_Size.y - 6, color6);
/// On repositionne
    m_Drawing.Move(m_Pos);
/// Coloration du texte
    m_Text.SetColor(m_IsPressed ? sf::Color::White : sf::Color::Black);
}

void TextButton::Draw(sf::RenderTarget& target) const {
    target.Draw(m_Drawing);
    target.Draw(m_Text);
}

void TextButton::Move(const sf::Vector2f& shift) {
    Widget::Move(shift);
    m_Text.Move(shift);
}

////////////////////////////////////////////////////////////
/// class SpriteButton definition
////////////////////////////////////////////////////////////
SpriteButton::SpriteButton(const sf::Image* imageReleased, const sf::Image* imagePressed) :
Button("SpriteButton"),
m_ImagePressed(imagePressed),
m_ImageReleased(imageReleased ? imageReleased : imagePressed) {
    std::cout << "[Zindows] SpriteButton created (" << this << ")" << std::endl;
    m_Sprite.SetImage(*m_ImageReleased);
    SetSize(m_Sprite.GetSize());
    BuildDrawing();
}

void SpriteButton::BuildDrawing() {
    m_Drawing.Clear();
    m_Sprite.SetImage(m_IsPressed ? *m_ImagePressed : *m_ImageReleased);
}

void SpriteButton::Draw(sf::RenderTarget& target) const {
    target.Draw(m_Sprite);
}

void SpriteButton::Move(const sf::Vector2f& shift) {
    Widget::Move(shift);
    m_Sprite.Move(shift);
}

}
