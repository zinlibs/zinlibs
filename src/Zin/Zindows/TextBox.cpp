/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/TextBox.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class TextBox definition
////////////////////////////////////////////////////////////
TextBox::TextBox(Text& text, float width, float height) :
ScrollBox(width, height),
m_Text(text) {
    m_Text.SetSize(width, height);
    Move(m_Text.GetPosition());
    m_Text.SetSize(AutoSet(m_Text.GetSize()));
    std::cout << "[Zindows] TextBox created (" << this << ")" << std::endl;
}

TextBox::~TextBox() {
    std::cout << "[Zindows] TextBox deleted (" << this << ")" << std::endl;
}

////////////////////////////////////////////////////////////
void TextBox::Draw(sf::RenderTarget& target) const {
    target.SetView(m_View);
    target.Draw(m_Text);
    Widget::Draw(target);
    target.SetView(target.GetDefaultView());
}

////////////////////////////////////////////////////////////
ActionQueue TextBox::Update(Events events) {
    ActionQueue actions;

    m_Text.Update(events);

    ActionQueue internalActions = Widget::Update(events);

    sf::Vector2f textSize      = m_Text          .GetSize();
    sf::Vector2f hScrollerSize = Get("ScrollerH").GetSize();
    sf::Vector2f vScrollerSize = Get("ScrollerV").GetSize();

    sf::Vector2f coeff(textSize.x / hScrollerSize.x - 1,  textSize.y / vScrollerSize.y - 1);

    Action action;

    while( internalActions.Get(action) ) {

        if( action == m_ActionIndex["ScrollerUp"] )
            m_Text.Move(0, coeff.y);

        if( action == m_ActionIndex["ScrollerDown"] )
            m_Text.Move(0, -coeff.y);

        if( action == m_ActionIndex["ScrollerLeft"] )
            m_Text.Move(coeff.x, 0);

        if( action == m_ActionIndex["ScrollerRight"] )
            m_Text.Move(-coeff.x, 0);
    }

    return actions;
}

}
