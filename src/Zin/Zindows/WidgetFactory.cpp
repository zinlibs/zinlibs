/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/WidgetFactory.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class WidgetFactory definition
////////////////////////////////////////////////////////////
void WidgetFactory::BuildMenu(Widget& widget) {
    sf::Vector2f size = widget.GetSize();
    Drawing& drawing = widget.GetDrawing();
    drawing.Clear();
    drawing+=sf::Shape::Rectangle(0, 1, size.x, 20, sf::Color(161, 161, 161)); // corps
    drawing+=sf::Shape::Line(0, 1, size.x, 1, 1, sf::Color(196, 196, 196)); // haut
    drawing+=sf::Shape::Line(0, 21, size.x, 21, 1, sf::Color(118, 118, 118)); // bas
}

}
