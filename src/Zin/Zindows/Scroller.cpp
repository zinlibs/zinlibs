/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Scroller.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Scroller definition
////////////////////////////////////////////////////////////
Scroller::Scroller(float length, unsigned int disposition) :
m_Step(0),
m_Disposition(disposition == Horizontal || disposition == Vertical ? disposition : Horizontal),
m_IsSelectorPressed(false),
m_IsFirstScrubberPressed(false),
m_IsSecondScrubberPressed(false) {

/// Ajout des boutons :

    Add(*(new SpriteButton(m_Disposition == Horizontal ? &m_Theme.Images.Get("ScrollButtonH")        : &m_Theme.Images.Get("ScrollButtonV"),
                           m_Disposition == Horizontal ? &m_Theme.Images.Get("ScrollButtonHPressed") : &m_Theme.Images.Get("ScrollButtonVPressed"))),
                           "Selector");
    Add(*(new SpriteButton(m_Disposition == Horizontal ? &m_Theme.Images.Get("ScrubberLeft")         : &m_Theme.Images.Get("ScrubberUp"),
                           m_Disposition == Horizontal ? &m_Theme.Images.Get("ScrubberLeftPressed")  : &m_Theme.Images.Get("ScrubberUpPressed"))),
                           "FirstScrubber");
    Add(*(new SpriteButton(m_Disposition == Horizontal ? &m_Theme.Images.Get("ScrubberRight")        : &m_Theme.Images.Get("ScrubberDown"),
                           m_Disposition == Horizontal ? &m_Theme.Images.Get("ScrubberRightPressed") : &m_Theme.Images.Get("ScrubberDownPressed"))),
                           "SecondScrubber");

/// Détermination des dimensions du plus grand bouton :

    const sf::Vector2f& firstScrubberSize  = Get("FirstScrubber") .GetSize();
    const sf::Vector2f& secondScrubberSize = Get("SecondScrubber").GetSize();
    const sf::Vector2f& selectorSize       = Get("Selector")      .GetSize();

    float maxWidth = firstScrubberSize.x;
    float maxHeight = firstScrubberSize.y;

    if( maxWidth > secondScrubberSize.x )
        maxWidth = secondScrubberSize.x;
    if( maxWidth > selectorSize.x )
        maxWidth = selectorSize.x;

    if( maxHeight > secondScrubberSize.y )
        maxHeight = secondScrubberSize.y;
    if( maxHeight > selectorSize.y )
        maxHeight = selectorSize.y;

/// Détermination des dimensions du scroller :

    if( m_Disposition == Horizontal )
        m_Size = sf::Vector2f(length, maxHeight);

    if( m_Disposition == Vertical )
        m_Size = sf::Vector2f(maxWidth, length);

/// Positionnement des boutons :

    if( m_Disposition == Horizontal  ) {

        Get("FirstScrubber") .SetPosition(                          0, 0);
        Get("Selector")      .SetPosition(        firstScrubberSize.x, 0);
        Get("SecondScrubber").SetPosition(length-secondScrubberSize.x, 0);
    }

    if( m_Disposition == Vertical ) {

        Get("FirstScrubber") .SetPosition(0,                           0);
        Get("Selector")      .SetPosition(0,         firstScrubberSize.y);
        Get("SecondScrubber").SetPosition(0, length-secondScrubberSize.y);
    }

/// Construction du dessin du fond :

    BuildDrawing();

/// Calcul du coefficient :

    ComputeCoeff();

/// Notification de création du widget :

    std::cout << "[Zindows] Scroller created (" << this << ")" << std::endl;
}

////////////////////////////////////////////////////////////
Scroller::~Scroller() {

/// Suppression des objets dynamiques :

    delete &Get("FirstScrubber");
    delete &Get("SecondScrubber");
    delete &Get("Selector");

/// Notification de supression du widget :

    std::cout << "[Zindows] Scroller deleted (" << this << ")" << std::endl;

}

////////////////////////////////////////////////////////////
void Scroller::SetSize(const sf::Vector2f& size) {
}

////////////////////////////////////////////////////////////
void Scroller::SetLength(float length) {
    // Reconstruire le scroller ici !
    ComputeCoeff();
}

////////////////////////////////////////////////////////////

void Scroller::ComputeCoeff() {

    // Obtention d'informations sur les boutons
    const sf::Vector2f& selectorSize       = Get("Selector")      .GetSize();
    const sf::Vector2f& firstScrubberSize  = Get("FirstScrubber") .GetSize();
    const sf::Vector2f& secondScrubberSize = Get("SecondScrubber").GetSize();

    if( m_Disposition == Horizontal )
        m_Coeff = m_Size.x / (m_Size.x - selectorSize.x - firstScrubberSize.x - secondScrubberSize.x);

    if( m_Disposition == Vertical )
        m_Coeff = m_Size.y / (m_Size.y - selectorSize.y - firstScrubberSize.y - secondScrubberSize.y);
}

////////////////////////////////////////////////////////////
void Scroller::BuildDrawing() {

/// Effacement de l'ancien dessin

    m_Drawing.Clear();

/// Construction du dessin

    if( m_Disposition == Horizontal )
        m_Drawing+=sf::Shape::Rectangle(0, m_Size.y/2 - 7, m_Size.x-1, 14, sf::Color(227, 227, 227), 1, sf::Color(69, 69, 69));

    if( m_Disposition == Vertical )
        m_Drawing+=sf::Shape::Rectangle(m_Size.x/2 - 7, 0, 14,  m_Size.y-1, sf::Color(227, 227, 227), 1, sf::Color(69, 69, 69));
}

////////////////////////////////////////////////////////////
unsigned int Scroller::GetStep() {
    return m_Step;
}

////////////////////////////////////////////////////////////
ActionQueue Scroller::Update(Events events) {

    ActionQueue actions;

    if( !m_IsLocked ) {

    /// Mise à jour du selecteur :

        ActionQueue internalActions = Get("Selector").Update(events);

    // Obtention d'informations sur les boutons
        const sf::Vector2f& selectorPos        = Get("Selector")      .GetPosition();
        const sf::Vector2f& selectorSize       = Get("Selector")      .GetSize();
        const sf::Vector2f& firstScrubberSize  = Get("FirstScrubber") .GetSize();
        const sf::Vector2f& secondScrubberSize = Get("SecondScrubber").GetSize();

    // Si le selecteur rapporte son enfoncement on enregistre cet état
        if( internalActions.Contains(m_ActionIndex["ButtonPressed"]) )
            m_IsSelectorPressed = true;

    // Si le selecteur rapporte son relachement on enregistre cet état
        if( internalActions.Contains(m_ActionIndex["ButtonReleased"]) )
            m_IsSelectorPressed = false;

    // Le selecteur est pressé
        if( m_IsSelectorPressed ) {

        // Si le scroller est horizontal
            if( m_Disposition == Horizontal ) {

            // On mémorise la position de la souris dans une variable
                float X = events.MousePos.x;

            // On ramène cette variable entre les bornes autorisées : borne inférieure
                if( X < m_Pos.x + selectorSize.x/2 + firstScrubberSize.x )
                    X = m_Pos.x + selectorSize.x/2 + firstScrubberSize.x;

            // Borne supérieure
                if( X > m_Pos.x + m_Size.x - selectorSize.x/2 - secondScrubberSize.x )
                    X = m_Pos.x + m_Size.x - selectorSize.x/2 - secondScrubberSize.x;

            // On calcule le step courant
                unsigned int step = m_Coeff * (selectorPos.x - m_Pos.x - firstScrubberSize.x);

            // Le step courant est-il inférieur à l'ancien step ?
                if( step < m_Step )
                    for( unsigned int k(0); k < m_Step - step; ++k )
                        actions+=m_ActionIndex["ScrollerLeft"];

            // Le step courant est-il supérieur à l'ancien step ?
                if( step > m_Step )
                    for( unsigned int k(0); k < step - m_Step; ++k )
                        actions+=m_ActionIndex["ScrollerRight"];

            // On met à jour le step
                m_Step = step;

            // On applique le nouveau step au selecteur
                Get("Selector").Widget::SetPosition(X - selectorSize.x/2, selectorPos.y);
            }

        // Si le scroller est vertical
            if( m_Disposition == Vertical ) {

            // On mémorise la position de la souris dans une variable
                float Y = events.MousePos.y;

            // On ramène cette variable entre les bornes autorisées : borne inférieure
                if( Y < m_Pos.y + selectorSize.y/2 + firstScrubberSize.y )
                    Y = m_Pos.y + selectorSize.y/2 + firstScrubberSize.y;

            // Borne supérieure
                if( Y > m_Pos.y + m_Size.y - selectorSize.y/2 - secondScrubberSize.y )
                    Y = m_Pos.y + m_Size.y - selectorSize.y/2 - secondScrubberSize.y;

            // On calcule le step courant
                unsigned int step = m_Coeff * (selectorPos.y - m_Pos.y - firstScrubberSize.y);

            // Le step courant est-il inférieur à l'ancien step ?
                if( step < m_Step )
                    for( unsigned int k(0); k < m_Step - step; ++k )
                        actions+=m_ActionIndex["ScrollerUp"];

            // Le step courant est-il supérieur à l'ancien step ?
                if( step > m_Step )
                    for( unsigned int k(0); k < step - m_Step; ++k )
                        actions+=m_ActionIndex["ScrollerDown"];

            // On met à jour le step
                m_Step = step;

            // On applique le nouveau step au selecteur
                Get("Selector").Widget::SetPosition(selectorPos.x, Y - selectorSize.y/2);
            }
        }

    /// Mise à jour des scrubbers :

        ActionQueue firstScrubberActions  = Get("FirstScrubber") .Update(events);
        ActionQueue secondScrubberActions = Get("SecondScrubber").Update(events);

    // Si le scrubber notifie son enfoncement alors on enregistre son état
        if( firstScrubberActions.Contains(m_ActionIndex["ButtonPressed"]))
            m_IsFirstScrubberPressed = true;

    // Si le scrubber notifie son relachement alors on enregistre son état
        if( firstScrubberActions.Contains(m_ActionIndex["ButtonReleased"]))
            m_IsFirstScrubberPressed = false;

    // Si le scrubber notifie son enfoncement alors on enregistre son état
        if( secondScrubberActions.Contains(m_ActionIndex["ButtonPressed"]))
            m_IsSecondScrubberPressed = true;

    // Si le scrubber notifie son relachement alors on enregistre son état
        if( secondScrubberActions.Contains(m_ActionIndex["ButtonReleased"]))
            m_IsSecondScrubberPressed = false;

    // Si le premier srubber est enfoncé
        if( m_IsFirstScrubberPressed ) {

        // Si le scroller est vertical
            if( m_Disposition == Horizontal )
                if( selectorPos.x - m_Pos.x > firstScrubberSize.x ) {
                    Get("Selector").Widget::Move(-1, 0);
                    unsigned int step = m_Coeff * (selectorPos.x - m_Pos.x - firstScrubberSize.x);
                    for( unsigned int k(0); k < m_Step - step; ++k )
                        actions+=m_ActionIndex["ScrollerLeft"];
                    m_Step = step;
                }

        // Si le scroller est vertical
            if( m_Disposition == Vertical )
                if( selectorPos.y - m_Pos.y > firstScrubberSize.y ) {
                    Get("Selector").Widget::Move(0, -1);
                    unsigned int step = m_Coeff * (selectorPos.y - m_Pos.y - firstScrubberSize.y);
                    for( unsigned int k(0); k < m_Step - step; ++k )
                        actions+=m_ActionIndex["ScrollerUp"];
                    m_Step = step;
                }
        }

    // Si le second srubber est enfoncé
        if( m_IsSecondScrubberPressed ) {

        // Si le scroller est horizontal
            if( m_Disposition == Horizontal )
                if( selectorPos.x - m_Pos.x < m_Size.x - selectorSize.x - secondScrubberSize.x ) {
                    Get("Selector").Widget::Move(1, 0);
                    unsigned int step = m_Coeff * (selectorPos.x - m_Pos.x - firstScrubberSize.x);
                    for( unsigned int k(0); k < step - m_Step; ++k )
                        actions+=m_ActionIndex["ScrollerRight"];
                    m_Step = step;
                }

        // Si le scroller est vertical
            if( m_Disposition == Vertical )
                if( selectorPos.y - m_Pos.y < m_Size.y - selectorSize.y - secondScrubberSize.y ) {
                    Get("Selector").Widget::Move(0, 1);
                    unsigned int step = m_Coeff * (selectorPos.y - m_Pos.y - firstScrubberSize.y);
                    for( unsigned int k(0); k < step - m_Step; ++k )
                        actions+=m_ActionIndex["ScrollerDown"];
                    m_Step = step;
                }
        }
    }

    return actions;
}

}
