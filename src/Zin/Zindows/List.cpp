/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/List.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class List definition
////////////////////////////////////////////////////////////
List::List() :
WidgetContainer<SlotForList>(),
m_SelectedSlot(0) {
    SetSize(100, 0);
    std::cout << "[Zindows] List created (" << this << ")" << std::endl;
}

List::~List() {
    std::cout << "[Zindows] List deleted (" << this << ")" << std::endl;
}

void List::Set() {
    back()->Widget::Move(m_NextPos.x, m_NextPos.y);
    m_NextPos.y+=20;
    m_Size.y+=20;
    float SlotLength = back()->GetSize().x;
    if( SlotLength > m_Size.x ) {
        m_Size.x = SlotLength;
        for( size_t k(0); k < size(); ++k )
            at(k)->SetSize(sf::Vector2f(m_Size.x-2, 20));
    }
    else if( SlotLength < m_Size.x )
        back()->SetSize(sf::Vector2f(m_Size.x-2, 20));
    BuildDrawing();
    m_Trigger.SetSize(m_Size);
}

void List::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

void List::Move(const sf::Vector2f& shift) {
    m_Drawing.Move(shift);
    m_Trigger.Move(shift);
    WidgetContainer<SlotForList>::Move(shift);
}

void List::SetPosition(float px, float py) {
    SetPosition(sf::Vector2f(px, py));
}

void List::SetPosition(const sf::Vector2f& pos) {
    sf::Vector2f shift = pos - m_Drawing.GetPosition();
    Move(shift);
}

void List::BuildDrawing() {
    m_Drawing.Clear();
    if( size() == 1 ) {
        m_Drawing+=sf::Shape::Rectangle(0, 0, m_Size.x-1, m_Size.y, sf::Color::White);
        m_Drawing+=sf::Shape::Line(m_Size.x-1, 0, m_Size.x-1, m_Size.y, 1, sf::Color(118, 118, 118)); // Right border line
        m_Drawing+=sf::Shape::Line(0, m_Size.y, m_Size.x-1, m_Size.y, 1, sf::Color(118, 118, 118)); // Bottom border line
    }
    else {
        sf::Shape gradient;
          gradient.AddPoint(       0, 0, sf::Color::White);
          gradient.AddPoint(m_Size.x-1, 0, sf::Color::White);
          gradient.AddPoint(m_Size.x-1, 30, sf::Color(255, 255, 255, 200));
          gradient.AddPoint(       0, 30, sf::Color(255, 255, 255, 200));
        sf::Shape body = sf::Shape::Rectangle(0, 30, m_Size.x-1, m_Size.y-30, sf::Color(255, 255, 255, 200));
        m_Drawing+=gradient;
        m_Drawing+=body;
        m_Drawing+=sf::Shape::Line(0, 0, 0, m_Size.y, 1, sf::Color(118, 118, 118)); // Left border line
        m_Drawing+=sf::Shape::Line(m_Size.x-1, 0, m_Size.x-1, m_Size.y, 1, sf::Color(118, 118, 118)); // Right border line
        m_Drawing+=sf::Shape::Line(0, m_Size.y, m_Size.x-1, m_Size.y, 1, sf::Color(118, 118, 118)); // Bottom border line
        m_Drawing+=sf::Shape::Line(0, 0, m_Size.x-1, 0, 1, sf::Color(118, 118, 118)); // Top border line
    }
}

void List::Draw(sf::RenderTarget& target) const {
    target.Draw(m_Drawing);
    WidgetContainer<SlotForList>::Draw(target);
}

ActionQueue List::Update(Events events) {
    ActionQueue actions = m_Trigger.Update(events);
    for( size_t k(0); k < size(); ++k ) {
        ActionQueue slotActions = at(k)->Update(events);
        actions+=slotActions;
    }
    return actions;
}

}
