/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Text.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Text definition
////////////////////////////////////////////////////////////
Text::Text() :
Label(),
m_IsFormated(true),
m_CarretIsActivated(false),
m_CarretIsShowed(true),
m_CarretDrawing(sf::Shape::Line(0, 0, 0, GetCharacterSize()*1.15, 2, sf::Color::Red)),
m_CarretFlashingIsEnabled(true),
m_CharSize(30),
m_Font(GetFont()),
m_LineHeight(35),
m_Pos(GetPosition()),
m_SelectionIsPerformed(false),
m_SelectionIsStarted(false),
m_SelectionColor(sf::Color(20, 0, 255, 120)),
m_String(GetString()) {
}

Text::Text(const sf::String& string, const sf::Font& font, unsigned int charSize) :
Label(string, font, charSize),
m_IsFormated(true),
m_CarretIsActivated(false),
m_CarretIsShowed(true),
m_CarretDrawing(sf::Shape::Line(0, 0, 0, charSize*1.15, 2, sf::Color::Red)),
m_CarretFlashingIsEnabled(true),
m_CharSize(charSize),
m_Font(GetFont()),
m_LineHeight(35),
m_Pos(GetPosition()),
m_SelectionIsPerformed(false),
m_SelectionIsStarted(false),
m_SelectionColor(sf::Color(20, 0, 255, 120)),
m_String(GetString()) {
}

void Text::SetSize(float x, float y) {
    SetSize(sf::Vector2f(x, y));
}

void Text::SetSize(const sf::Vector2f& size) {
    if( size.x > 0 &&
        size.x != m_Size.x &&
        size.y > 0 &&
        size.y != m_Size.y ) {
        m_Size.x = size.x;
        m_Size.y = size.y;
        Format();
    }
}

void Text::SetPosition(float px, float py) {
    SetPosition(sf::Vector2f(px, py));
}

void Text::SetPosition(const sf::Vector2f& pos) {
    sf::Drawable::SetPosition(pos);
    sf::Vector2f p = m_CarretDrawing.GetPosition();
    m_CarretDrawing.Move(pos-p);
}

void Text::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

void Text::Move(const sf::Vector2f& shift) {
    sf::Drawable::Move(shift);
    //m_CarretDrawing.Move(shift);
}

void Text::SetCharacterSize(unsigned int size) {
    if( size > 0 ) {
        sf::Text::SetCharacterSize(size);
        Format();
    }
    if( size ) {
        m_CharSize = size;
        ComputeLineHeight();
        BuildSelectionDrawing();
        sf::Vector2f pos = m_CarretDrawing.GetPosition();
        m_CarretDrawing = sf::Shape::Line(0, 0, 0, m_LineHeight, 2, sf::Color::Red);
        m_CarretDrawing.SetPosition(GetCharacterPos(m_SelectionRange.End));
    }
}

void Text::SetString(const sf::String& string) {
    Label::SetString(string);
    Format();
    ComputeLineHeight();
}

void Text::AddStringAtBegin(const sf::String& string) {
    const sf::String& currentString = Label::m_UnformatedString;
    std::string newString = string + currentString;
    size_t shift = string.GetSize();
    m_SelectionRange.Begin+=shift;
    m_SelectionRange.End+=shift;
    Text::SetString(newString);
    BuildSelectionDrawing();
}

void Text::AddStringAtEnd(const sf::String& string) {
    const sf::String& currentString = Label::m_UnformatedString;
    std::string newString = currentString + string;
    Text::SetString(newString);
}

size_t Text::GetMatchingLine(sf::Vector2f pos) const { // On peut simplifier avec un module %
    for( size_t LineNb(0); LineNb <= m_LineRangeIndex.size(); ++LineNb ) {
        sf::FloatRect LineRect(m_Pos.x, m_Pos.y + LineNb*m_LineHeight, Label::m_Size.x, m_LineHeight);
        if( LineRect.Contains(pos) )
            return LineNb < m_LineRangeIndex.size() ?
                LineNb+1 : LineNb;
    }
    return 0;
}

size_t Text::GetOnMouseCharNb(Events events) const {
/// Les intervalles de ligne sont parcourues afin de réduire la taille de la zone de recherche
    size_t lineNb = GetMatchingLine(events.MousePos);
    if( lineNb ) { // Le pointeur de la souris est compris dans l'intervalle représentant la ligne parcourue
        lineNb--; // Normalisation du numéro de ligne
    /// Les caractères de la ligne sont parcourus afin de trouver le caractère recherché
        for( size_t charNb(m_LineRangeIndex[lineNb].Begin); charNb <= m_LineRangeIndex[lineNb].End; ++charNb ) {
            float charWidth = TextTools::GetCharWidth(m_String[charNb], m_Font, m_CharSize);
            sf::FloatRect charRect(m_Pos+GetCharacterPos(charNb), sf::Vector2f(charWidth, m_LineHeight));
        /// Le caractère pointé par la souris est trouvé, s'il est sur la partie droite du caractère on déplace le curseur à droite
            if( charRect.Contains(events.MousePos) )
                return events.MousePos.x <= charRect.Left + charWidth/2 ?
                    charNb+1 : charNb+2;
        }
        // Il faut detecter ici si on est à la toute fin du texte pour éviter les dépassement de mémoire
        size_t charNb = m_LineRangeIndex[lineNb].End;
        return charNb == m_String.GetSize()-1 ?
            charNb+2 : charNb+1; // /!\ si la ligne est detectée mais pas le caractère, on place en fin de ligne (bugué quand deux retours à la ligne)
    }
    return 0;
}

void Text::ComputeLineHeight() {
    m_LineHeight = m_LineRangeIndex.size() > 1 ?
        GetCharacterPos(m_LineRangeIndex[1].Begin).y-GetCharacterPos(m_LineRangeIndex[0].Begin).y : m_CharSize*1.15;
}

void Text::BuildSelectionDrawing() { // /!\ Bugué pour la sélection à l'envers
    m_SelectionDrawing.clear();
/// On s'assure qu'il y a bien sélection
    if( m_SelectionRange.Begin != m_SelectionRange.End ) {
    /// On recherche l'intervale de ligne où est contenue la sélection
        size_t beginLine = GetMatchingLine(GetCharacterPos(m_SelectionRange.Begin) + m_Pos);
        size_t endLine = GetMatchingLine(GetCharacterPos(m_SelectionRange.End) + m_Pos);
    /// On s'assure que les lignes renvoyées ne sont pas incorrectes et on les normalise
        if( !beginLine && !endLine )
            return;
        beginLine--;
        endLine--;
        Range lineRange(beginLine, endLine);
    /// Sélection mono-ligne, la sélection s'étend de son propre début à sa propre fin
        if( lineRange.Begin == lineRange.End ) {
            sf::Vector2f shapePos = GetCharacterPos(m_SelectionRange.Begin);
            sf::Vector2f shapeSize;
            shapeSize.x = GetCharacterPos(m_SelectionRange.End).x - shapePos.x;
            shapeSize.y = m_LineHeight;
            m_SelectionDrawing.push_back(sf::Shape::Rectangle(shapePos.x, shapePos.y, shapeSize.x, shapeSize.y, m_SelectionColor));
            return;
        }
    /// Sélection multi-ligne (on parcours toutes les lignes de l'intervale)
        for( size_t LineNb(lineRange.Begin); LineNb <= lineRange.End; ++LineNb ) {
        /// A la première ligne, la sélection s'étend de son propre début à la fin de la ligne
            Range selectionRange(m_SelectionRange);
            if( LineNb == lineRange.Begin ) {
                sf::Vector2f shapePos = GetCharacterPos(selectionRange.Begin);
                sf::Vector2f shapeSize;
                sf::Vector2f lastCharPos = GetCharacterPos(m_LineRangeIndex[LineNb].End);
                shapeSize.x = lastCharPos.x - shapePos.x;
                shapeSize.y = m_LineHeight;
                m_SelectionDrawing.push_back(sf::Shape::Rectangle(shapePos.x, shapePos.y, shapeSize.x, shapeSize.y, m_SelectionColor));
                continue;
            }
        /// A la dernière ligne, la sélection s'étend du début de la ligne à sa propre fin
            if( LineNb == lineRange.End ) {
                sf::Vector2f shapePos = GetCharacterPos(m_LineRangeIndex[LineNb].Begin);
                sf::Vector2f shapeSize;
                sf::Vector2f lastCharPos = GetCharacterPos(selectionRange.End);
                shapeSize.x = lastCharPos.x - shapePos.x;
                shapeSize.y = m_LineHeight;
                m_SelectionDrawing.push_back(sf::Shape::Rectangle(shapePos.x, shapePos.y, shapeSize.x, shapeSize.y, m_SelectionColor));
                continue;
            }
        /// Pour les lignes intermédiaires, la sélection s'étend du début de la ligne à la fin de la ligne
            sf::Vector2f shapePos = GetCharacterPos(m_LineRangeIndex[LineNb].Begin);
            sf::Vector2f shapeSize;
            sf::Vector2f lastCharPos = GetCharacterPos(m_LineRangeIndex[LineNb].End);
            shapeSize.x = lastCharPos.x - shapePos.x;
            shapeSize.y = m_LineHeight;
            m_SelectionDrawing.push_back(sf::Shape::Rectangle(shapePos.x, shapePos.y, shapeSize.x, shapeSize.y, m_SelectionColor));
        }
    }
}

void Text::Render(sf::RenderTarget& target, sf::Renderer& renderer) const {
    for( size_t k(0); k < m_SelectionDrawing.size(); ++k )
        target.Draw(m_SelectionDrawing[k]);
    sf::Text::Render(target, renderer);
    if( m_CarretIsActivated && m_CarretIsShowed )
        target.Draw(m_CarretDrawing);
}

void Text::SelectAll() {
    m_SelectionRange.Begin = 0;
    m_SelectionRange.End   = Label::m_UnformatedString.size();
    m_CarretDrawing.SetPosition(GetCharacterPos(m_SelectionRange.End));
    BuildSelectionDrawing();
}

void Text::EraseSelection() {
    Range selectionRange(m_SelectionRange);
    Label::m_UnformatedString.erase(selectionRange.Begin, selectionRange.End - selectionRange.Begin);
    m_SelectionDrawing.clear();
    Format();
    if( m_SelectionRange.End >= m_SelectionRange.Begin )
        m_SelectionRange.End = m_SelectionRange.Begin;
    else m_SelectionRange.Begin = m_SelectionRange.End;
    m_CarretDrawing.SetPosition(GetCharacterPos(m_SelectionRange.End));
}

void Text::Format() {
/// Copie de la chaîne non formatée :
    std::string str = m_UnformatedString;
/// Obtention des informations du Texte :
    const sf::Font font = GetFont();
    const float fontSize = GetCharacterSize();
    const float coeff = 1.15; // Rapport Taille de caractère / Distance entre deux lignes
    const unsigned int nbLinesMax = static_cast<const unsigned int>(m_Size.y/(static_cast<float>(fontSize)*coeff)); // Calcul du nombre maximum de lignes autorisées
/// Initialisation des variables :
    unsigned int lineLength = 0;
    unsigned int nbLines = 0;
/// Traitement de la chaîne :
    for( size_t k(0); k < str.size(); ++k ) {
        if( str[k] == '\n' ) { // Rencontre d'un saut de ligne
            nbLines++; // On incrémente le nombre de lignes
            lineLength = 0; // On réinitialise la largeur de la ligne

            /*if( nbLines >= nbLinesMax ) { // Le nombre de lignes parcourues dépasse le maximum de lignes autorisées
                str.erase(str.begin()+k, str.end()); // On découpe le Texte superflux
                break; // On quitte la boucle
            }*/
        }
        else {
            lineLength+=TextTools::GetCharWidth(str[k], font, fontSize); // Incrémentation de la taille du caractère courant à la taille de la chaîne parcourue
            if( lineLength > m_Size.x ) { // La largeur de la ligne est supérieure à la largeur de la zone de Texte
                if( !m_IsFormated ) {
                    int i = k;
                    while( 1 ) {
                        if( str[i] == '\n' ) { // Rencontre d'un saut de ligne
                            str.erase(str.begin()+k, str.begin()+k+1); // On efface un caractère
                            k--;
                            break;
                        }
                        i++;
                    }
                }
                else {
                    size_t i = 1;
                    while( 1 ) {
                        if( str[k-i] == ' ' ) { // Rencontre d'un espace
                            str[k-i] = '\n'; // Remplacement de l'espace par un retour à la ligne
                            k = k-i-1;
                            break;
                        }
                        if( str[k-i] == '\n' or k == i ) {
                            str.insert(str.begin()+k, '\n');
                            k = k-1;
                            break;
                        }
                        i++;
                    }
                }
            }
        }
    }
/// Mise à jour du Text :
    sf::Text::SetString(str);
    sf::Vector2f size = GetSize();

    m_LineRangeIndex.clear();
    for( size_t CharNb(0); CharNb <= m_String.GetSize(); ++CharNb ) {
        if( CharNb < m_String.GetSize() ) {
            if( m_String[CharNb] == '\n' ) {
                size_t beginCharNb = m_LineRangeIndex.empty() ?
                    0 : m_LineRangeIndex.back().End+1;
                m_LineRangeIndex.push_back(Range(beginCharNb, CharNb));
                continue;
            }
        }
        else m_LineRangeIndex.push_back(m_LineRangeIndex.empty() ?
            Range(0, m_String.GetSize()-1) : Range(m_LineRangeIndex.back().End+1, m_String.GetSize()-1));
    }
}

ActionQueue Text::Update(Events events) {
    ActionQueue Action;
    sf::Event event;
    while( events.Get(event) ) {
    /// Si le bouton gauche est enfoncé et que la selection n'est pas activée alors on active la selection
        sf::FloatRect m_Area(m_Pos, Label::m_Size);
        bool isSelected = m_Area.Contains(events.MousePos);
        if( isSelected && event.Type == sf::Event::MouseButtonPressed && event.MouseButton.Button == sf::Mouse::Left && !m_SelectionIsPerformed ) {
            m_SelectionIsPerformed = true;
            m_SelectionIsStarted = true;
            m_SelectionDrawing.clear();
        }
    /// Si la selection est activée et que le bouton gauche est relaché alors on desactive la sélection
        if( event.Type == sf::Event::MouseButtonReleased && event.MouseButton.Button == sf::Mouse::Left && m_SelectionIsPerformed )
            m_SelectionIsPerformed = false;
    /// Si la selection est activée et que le bouton gauche est enfoncé alors on procede a la selection
        if( isSelected && events.Input.IsMouseButtonDown(sf::Mouse::Left) && m_SelectionIsPerformed ) {
            size_t charNb = GetOnMouseCharNb(events); // Obtention de la position du caractère situé sous le curseur de la souris
            if( charNb ) { // Le caractère renvoyé est valide
                charNb--;
                if( !m_CarretIsActivated ) {
                    m_CarretIsActivated = true; // Le curseur est activé si ce n'est pas déjà fait
                    if( m_CarretFlashingIsEnabled )
                        m_CarretTimer.Start();
                }
                if( m_SelectionIsStarted ) { // On commence la sélection en plaçant le curseur à l'endroit ou le curseur est enfoncé
                    m_SelectionRange.Begin = charNb;
                    m_SelectionRange.End   = charNb;
                    m_CarretDrawing.SetPosition(GetCharacterPos(m_SelectionRange.Begin));
                    sf::Vector2f charPoss = GetCharacterPos(m_SelectionRange.Begin);
                }
                else if( m_SelectionRange.End != charNb ) {
                    m_SelectionRange.End = charNb;
                    m_CarretDrawing.SetPosition(GetCharacterPos(m_SelectionRange.End)); // Le curseur est positionné
                    BuildSelectionDrawing();
                }
            }
        }
        if( m_SelectionIsStarted )
            m_SelectionIsStarted = false;
    }
/// Clignotement du curseur
    if( m_CarretIsActivated ) {
        if( m_CarretFlashingIsEnabled && m_SelectionRange.End <= m_String.GetSize() ) {
            m_CarretTimer.Update();
            float time = m_CarretTimer.GetTime();
            if( time > .3 ) {
                m_CarretTimer.Reset();
                m_CarretTimer.Start();
                m_CarretIsShowed = !m_CarretIsShowed;
            }
        }
        else m_CarretIsShowed = m_SelectionRange.End <= m_String.GetSize();
    }
    return Action;
}

Text::Range::Range() :
Begin(0),
End(0) {
}

Text::Range::Range(size_t begin, size_t end) :
Begin(begin < end ? begin : end),
End(end > begin ? end : begin) {
}

Text::Range::Range(const Range& p) {
    Begin = p.Begin;
    End = p.End;
    Sort();
}

void Text::Range::Sort() {
    if( Begin > End ) {
        size_t tmp = Begin;
        Begin = End;
        End = tmp;
    }
}

}
