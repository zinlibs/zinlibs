/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Slider.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Slider definition
////////////////////////////////////////////////////////////
Slider::Slider(float length, bool isHorizontal, unsigned int startStep, unsigned int stepsNb) :
m_IsSelectorPressed(false),
m_StepsNb(stepsNb),
m_Step(startStep),
m_IsHorizontal(isHorizontal) {
/// Dimensionnement
    SetSize(isHorizontal ? length : 5, isHorizontal ? 5 : length);
/// Ajout du selecteur
    Add(*(new SpriteButton(m_IsHorizontal ? &m_Theme.Images.Get("SliderSelectorH")        : &m_Theme.Images.Get("SliderSelectorV"),
                           m_IsHorizontal ? &m_Theme.Images.Get("SliderSelectorHPressed") : &m_Theme.Images.Get("SliderSelectorVPressed"))),
                           "Selector");
/// Correction des variables et positionnement du selecteur
    sf::Vector2f size = Get("Selector").GetSize();
    if( m_IsHorizontal ) { // horizontal
        if( m_StepsNb > 0 ) { // cranté
            if( m_Step > m_StepsNb )
                m_Step = m_StepsNb;
            Get("Selector").SetPosition(m_Pos.x + m_Step*(m_Size.x - size.x)/m_StepsNb, m_Pos.y + m_Size.y/2 - size.y/2);
        }
        else { // lisse
            if( m_Step > m_Size.x )
                m_Step = m_Size.x;
            Get("Selector").SetPosition(m_Pos.x + m_Step, m_Pos.y + m_Size.y/2 - size.y/2);
        }
    }
    else { // vertical
        if( m_StepsNb > 0 ) { // cranté
            if( m_Step > m_StepsNb )
                m_Step = m_StepsNb;
            Get("Selector").SetPosition(m_Pos.x + m_Size.x/2 - size.x/2, m_Pos.y + m_Step*(m_Size.y - size.y)/m_StepsNb);
        }
        else { // lisse
            if( m_Step > m_Size.y )
                m_Step = m_Size.y;
            Get("Selector").SetPosition(m_Pos.x + m_Size.x/2 - size.x/2, m_Pos.y + m_Step);
        }
    }

    std::cout << "[Zindows] Slider created (" << this << ")" << std::endl;
}

Slider::~Slider() {
    delete &Get("Selector");
    std::cout << "[Zindows] Slider deleted (" << this << ")" << std::endl;
}

float Slider::GetStep() {
    return m_Step;
}

void Slider::BuildDrawing() {
    m_Drawing.Clear();
    sf::Shape body;
    body.AddPoint(0,        0,        sf::Color(30, 30, 30));
    body.AddPoint(m_Size.x, 0,        m_IsHorizontal ? sf::Color(30, 30, 30) : sf::Color(227, 227, 227));
    body.AddPoint(m_Size.x, m_Size.y, m_IsHorizontal ? sf::Color(227, 227, 227) : sf::Color(30, 30, 30));
    body.AddPoint(0,        m_Size.y, sf::Color(227, 227, 227));
    m_Drawing+=body;
}

ActionQueue Slider::Update(Events events) {
    ActionQueue actions;
/// On met à jour le bouton
    ActionQueue internalActions = Get("Selector").Update(events);
/// Si le bouton est pressé il peut être déplacé
    if( !internalActions.Empty() ) {
        if( internalActions.Contains(m_ActionIndex.Get("ButtonPressed")) )
            m_IsSelectorPressed = true;
        if( internalActions.Contains(m_ActionIndex.Get("ButtonReleased")) )
            m_IsSelectorPressed = false;
    }
/// On enregistre la position X de la souris dans une variable
    if( m_IsSelectorPressed ) {
        sf::Vector2f pos = Get("Selector").GetPosition();
        sf::Vector2f size = Get("Selector").GetSize();
/// On teste cette variable et on la ramène à une valeur comprise entre les limites autorisées et au cran inférieur
        if( m_IsHorizontal ) {
            float X = events.MousePos.x;
            if( X < m_Pos.x + size.x/2 )
                X = m_Pos.x + size.x/2;
            if( X > m_Pos.x + m_Size.x - size.x/2 )
                X = m_Pos.x + m_Size.x - size.x/2;
        /// Slider cranté
            if( m_StepsNb != 0 ) {
                unsigned int step = static_cast<unsigned int>((X - m_Pos.x) / ((m_Size.x - size.x) / m_StepsNb));
                if( step < m_Step )
                    actions+=m_ActionIndex.Get("Left");
                if( step > m_Step )
                    actions+=m_ActionIndex.Get("Right");
                m_Step = step;
                Get("Selector").Widget::SetPosition(m_Pos.x + step*(m_Size.x - size.x)/m_StepsNb, pos.y);
            }
        /// Slider lisse
            else {
                unsigned int step = (1 - size.x / m_Size.x) * (pos.x - m_Pos.x);
                if( step < m_Step )
                    for( unsigned int k(0); k < m_Step - step; ++k )
                        actions+=m_ActionIndex.Get("Left");
                if( step > m_Step )
                    for( unsigned int k(0); k < step - m_Step; ++k )
                        actions+=m_ActionIndex.Get("Right");
                m_Step = step;
                Get("Selector").Widget::SetPosition(X - size.x/2, pos.y);
            }
        }
    /// Slider vertical
        else {
            float Y = events.MousePos.y;
            if( Y < m_Pos.y + size.y/2 )
                Y = m_Pos.y + size.y/2;
            if( Y > m_Pos.y + m_Size.y - size.y/2 )
                Y = m_Pos.y + m_Size.y - size.y/2;
        /// Slider cranté
            if( m_StepsNb != 0 ) {
                unsigned int step = static_cast<unsigned int>((Y - m_Pos.y) / ((m_Size.y - size.y) / m_StepsNb));
                if( step < m_Step )
                    actions+=m_ActionIndex.Get("Up");
                if( step > m_Step )
                    actions+=m_ActionIndex.Get("Down");
                m_Step = step;
                Get("Selector").Widget::SetPosition(pos.x, m_Pos.y + step*(m_Size.y - size.y)/m_StepsNb);
            }
        /// Slider lisse
            else {
                unsigned int step = (1 - size.y / m_Size.y) * (pos.y - m_Pos.y);
                if( step < m_Step )
                    for( unsigned int k(0); k < m_Step - step; ++k )
                        actions+=m_ActionIndex.Get("Up");
                if( step > m_Step )
                    for( unsigned int k(0); k < step - m_Step; ++k )
                        actions+=m_ActionIndex.Get("Down");
                m_Step = step;
                Get("Selector").Widget::SetPosition(pos.x, Y - size.y/2);
            }
        }
    }
    return actions;
}

}
