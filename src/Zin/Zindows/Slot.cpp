/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Slot.hpp>
#include <Zin/Zindows/List.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Slot definition
////////////////////////////////////////////////////////////
Slot::Slot(const sf::String& string) :
m_Label(string, sf::Font::GetDefaultFont(), 14) {
    m_Label.Move(4, 1);
    m_Label.SetColor(m_MouseOutLabelColor);
    m_Trigger = Trigger(0, 0, m_Label.GetRect().Width + 8, 20);
    m_Size = sf::Vector2f(m_Label.GetRect().Width + 8, 20);
    BuildDrawing();
    std::cout << "[Zindows] Slot created (" << this << ")" << std::endl;
}

Slot::~Slot() {
    std::cout << "[Zindows] Slot deleted (" << this << ")" << std::endl;
}

void Slot::BuildDrawing() {
    m_Drawing.Clear();
    m_Drawing+=sf::Shape::Rectangle(m_Pos.x, m_Pos.y, m_Size.x, m_Size.y, m_IsSelected ? m_MouseOverBodyColor : m_MouseOutBodyColor);
}

void Slot::ForceMouseOut() {
    m_IsSelected = false;
    m_Label.SetColor(m_MouseOutLabelColor);
    m_Trigger.Reset();
    BuildDrawing();
}

void Slot::SetActivation(bool activated) {
    if( activated ) {
        m_Label.SetColor(sf::Color::Black);
        m_IsActivated = true;
    }
    else {
        m_Label.SetColor(sf::Color(127, 127, 127));
        m_IsActivated = false;
    }
}

void Slot::SetLock(bool locked) {
    m_IsLocked = locked;
}

void Slot::Move(const sf::Vector2f& shift) {
    m_Trigger.Move(shift);
    m_Label.Move(shift);
    m_Pos+=shift;
    BuildDrawing();
}

void Slot::SetSize(const sf::Vector2f& size) {
    m_Trigger.SetSize(size);
    m_Size = size;
    BuildDrawing();
}

void Slot::Draw(sf::RenderTarget& target) const {
    target.Draw(m_Drawing);
    target.Draw(m_Label);
}

ActionQueue Slot::Update(Events events) {
    ActionQueue actions;
/// Si le slot est vérouillé, on empêche le changement de son état :
    if( !m_IsLocked && m_IsActivated ) {
        actions+=m_Trigger.Update(events);
        if( !actions.Empty() ) {
            if( actions.Contains(Widget::ActionMouseOver) ) {
                m_Label.SetColor(m_MouseOverLabelColor);
                m_IsSelected = true;
                BuildDrawing();
            }
            else if( actions.Contains(Widget::ActionMouseOut) ) {
                m_Label.SetColor(m_MouseOutLabelColor);
                m_IsSelected = false;
                BuildDrawing();
            }
        }
    }
    return actions;
}

////////////////////////////////////////////////////////////
/// class SlotForMenu definition
////////////////////////////////////////////////////////////
SlotForMenu::SlotForMenu(const sf::String& string) :
Slot(string) {
    m_MouseOutLabelColor = m_Theme.Colors.Get("SlotForMenuMouseOutLabelColor");
    m_MouseOverLabelColor = m_Theme.Colors.Get("SlotForMenuMouseOverLabelColor");
    m_MouseOutBodyColor = m_Theme.Colors.Get("SlotForMenuMouseOutBodyColor");
    m_MouseOverBodyColor = m_Theme.Colors.Get("SlotForMenuMouseOverBodyColor");
    m_Label.SetColor(m_MouseOutLabelColor);
}

////////////////////////////////////////////////////////////
/// class RibbonSlotForMenu definition
////////////////////////////////////////////////////////////
RibbonSlotForMenu::RibbonSlotForMenu(const sf::String& string, Ribbon& ribbon) :
SlotForMenu(string) {
    m_Ribbon = &ribbon;
}

RibbonSlotForMenu::~RibbonSlotForMenu() {
}

void RibbonSlotForMenu::Draw(sf::RenderTarget& target) const {
    Slot::Draw(target);
    if( m_IsSelected )
        (*m_Ribbon).Draw(target);
}

ActionQueue RibbonSlotForMenu::Update(Events events) {
    ActionQueue actions ;
    if( !m_IsLocked && m_IsActivated ) {
        actions = Slot::Update(events);
        m_Ribbon->Update(events);
    }
    return actions;
}


////////////////////////////////////////////////////////////
/// class ConsoleRibbonSlotForMenu definition
////////////////////////////////////////////////////////////
ConsoleRibbonSlotForMenu::ConsoleRibbonSlotForMenu() :
RibbonSlotForMenu("Console", *(new ConsoleRibbon)) {
}

////////////////////////////////////////////////////////////
/// class ListSlotForMenu definition
////////////////////////////////////////////////////////////
ListSlotForMenu::ListSlotForMenu(const sf::String& string, List& list) :
SlotForMenu(string) {
    m_List = &list;
    m_List->Move(m_Pos.x, m_Pos.y+20);
}

ListSlotForMenu::~ListSlotForMenu() {
}

void ListSlotForMenu::Move(const sf::Vector2f& shift) {
    Slot::Move(shift);
    m_List->Move(shift);
}

void ListSlotForMenu::Draw(sf::RenderTarget& target) const {
    if( m_IsSelected )
        (*m_List).Draw(target);
    Slot::Draw(target);

}

ActionQueue ListSlotForMenu::Update(Events events) {
    ActionQueue actions;
    actions+=Slot::Update(events);
    if( m_IsSelected && m_IsActivated )
        actions+=m_List->Update(events);
    return actions;
}
////////////////////////////////////////////////////////////
/// SlotForList
////////////////////////////////////////////////////////////
SlotForList::SlotForList(const sf::String& string) :
Slot(string) {
    m_MouseOutLabelColor = m_Theme.Colors.Get("SlotForListMouseOutLabelColor");
    m_MouseOverLabelColor = m_Theme.Colors.Get("SlotForListMouseOverLabelColor");
    m_MouseOutBodyColor = m_Theme.Colors.Get("SlotForListMouseOutBodyColor");
    m_MouseOverBodyColor = m_Theme.Colors.Get("SlotForListMouseOverBodyColor");
    m_Label.SetColor(m_MouseOutLabelColor);
}

////////////////////////////////////////////////////////////
/// class ListSlotForList definition
////////////////////////////////////////////////////////////
ListSlotForList::ListSlotForList(const sf::String& string, List& list) :
SlotForList(string) {
    m_List = &list;
    sf::Vector2f size = GetSize();
    SetSize(sf::Vector2f(size.x + 16, size.y));
    m_Shape.AddPoint(0, 0, sf::Color::Black);
    m_Shape.AddPoint(5, 5, sf::Color::Black);
    m_Shape.AddPoint(0, 11, sf::Color::Black);
    m_Shape.SetPosition(m_Pos.x + m_Size.x - 10, m_Pos.y + 5);
    m_List->Move(m_Size.x - 17, 0);
}

ListSlotForList::~ListSlotForList() {
}

void ListSlotForList::Move(const sf::Vector2f& shift) {
    Slot::Move(shift);
    m_List->Move(shift);
    m_Shape.Move(shift);
}

void ListSlotForList::SetSize(const sf::Vector2f& size) {
    sf::Vector2f shift = size-m_Size;
    Slot::SetSize(size);
    m_List->Move(shift);
    m_Shape.Move(shift);
}

void ListSlotForList::Draw(sf::RenderTarget& target) const {
    Slot::Draw(target);
    target.Draw(m_Shape);
    if( m_IsSelected && m_IsActivated )
        (*m_List).Draw(target);
}

ActionQueue ListSlotForList::Update(Events events) {
    ActionQueue actions;
    if( m_IsSelected && m_IsActivated) {
        ActionQueue listActions = m_List->Update(events);
        if( listActions.Contains(Widget::ActionMouseOver) ) {
            m_List->SetLock(true);
			SetLock(true);
			BuildDrawing();
		}
        if( listActions.Contains(Widget::ActionMouseOut) ) {
            m_List->SetLock(false);
			SetLock(false);
			m_IsSelected = false;
			BuildDrawing();
        }
		if( listActions.Contains(Widget::ActionMouseLeftClicked)) {
			m_List->SetLock(false);
			SetLock(false);
			m_IsSelected = false;
			BuildDrawing();
		}
 	       actions+=listActions;
    }
    Slot::Update(events);
    return actions;
}

////////////////////////////////////////////////////////////
/// class ActionSlotForList definition
////////////////////////////////////////////////////////////
ActionSlotForList::ActionSlotForList(const sf::String& string, ActionQueue actions) :
SlotForList(string),
m_Actions(actions),
m_Bascule(0) {
    m_Trigger.SetMouseClickTest(true);
}

ActionSlotForList::ActionSlotForList(const sf::String& string, Action action) :
SlotForList(string),
m_Actions(ActionQueue(action)),
m_Bascule(0) {
    m_Trigger.SetMouseClickTest(true);
}

ActionQueue ActionSlotForList::Update(Events events) {
    ActionQueue actions;
    ActionQueue internalActions = Slot::Update(events);
    if( m_IsSelected && m_IsActivated ) {
        if( internalActions.Contains(Widget::ActionMouseLeftClicked) ) {
            SetLock(false);
            ForceMouseOut();
            actions = m_Actions;
        }
    }
    return actions;
}

////////////////////////////////////////////////////////////
/// class CheckSlotForList definition
////////////////////////////////////////////////////////////
CheckSlotForList::CheckSlotForList(const sf::String& string, ActionQueue firstActions, ActionQueue lastActions, bool checked) :
m_FirstActions(firstActions),
m_LastActions(lastActions),
SlotForList(string),
m_IsChecked(checked),
m_Bascule(0) {
    SetSize(sf::Vector2f(m_Size.x + 16, m_Size.y));
    m_Sprite.SetImage(m_Theme.Images.Get("Check"));
    m_Sprite.SetPosition(m_Pos.x + m_Size.x - 10, m_Pos.y + 6);
    m_Trigger.SetMouseClickTest(true);
}

void CheckSlotForList::Move(const sf::Vector2f& shift) {
    Slot::Move(shift);
    m_Sprite.Move(shift);
}

void CheckSlotForList::SetSize(const sf::Vector2f& size) {
    Slot::SetSize(size);
    m_Sprite.SetPosition(m_Pos.x + m_Size.x - 10, m_Pos.y + 6);
}

void CheckSlotForList::Draw(sf::RenderTarget& target) const {
    Slot::Draw(target);
    if( m_IsChecked )
        target.Draw(m_Sprite);
}

ActionQueue CheckSlotForList::Update(Events events) {
    ActionQueue actions;
    ActionQueue internalActions = Slot::Update(events);
    if( m_IsSelected && m_IsActivated ) {
        /// Enfoncement 1 :
        if( internalActions.Contains(Widget::ActionMouseLeftPressed) ) {
            m_IsChecked = !m_IsChecked;
            actions+=m_IsChecked ? m_LastActions : m_FirstActions;
        }
    /// Relachement 1 :
        if( internalActions.Contains(Widget::ActionMouseLeftClicked) ) {
            SetLock(false);
            ForceMouseOut();
        }
    }
    return actions;
}

////////////////////////////////////////////////////////////
/// class SwitchSlotForList definition
////////////////////////////////////////////////////////////
SwitchSlotForList::SwitchSlotForList(const sf::String& firstString, const ActionQueue& firstActions, const sf::String& lastString, const ActionQueue& lastActions) :
m_FirstString(firstString),
m_LastString(lastString),
m_FirstActions(firstActions),
m_LastActions(lastActions),
m_IsSwitched(false),
SlotForList(firstString),
m_Bascule(0) {
    m_Trigger.SetMouseClickTest(true);
}

ActionQueue SwitchSlotForList::Update(Events events) {
    ActionQueue actions;
    ActionQueue internalActions = Slot::Update(events);
    if( m_IsSelected && m_IsActivated ) {
        /// Enfoncement 1 :
        if( internalActions.Contains(Widget::ActionMouseLeftPressed) ) {
            m_IsSwitched = !m_IsSwitched;
            actions+=m_IsSwitched ? m_FirstActions : m_LastActions;
            m_Label.SetString(m_IsSwitched ? m_LastString : m_FirstString);
        }
    /// Relachement 1 :
        if( internalActions.Contains(Widget::ActionMouseLeftClicked) ) {
            SetLock(false);
            ForceMouseOut();
        }
    }
    return actions;
}

}
