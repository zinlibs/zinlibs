/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/ScrollBox.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class ScrollBox definition
////////////////////////////////////////////////////////////
ScrollBox::ScrollBox(float width, float height) {
    SetSize(width, height);
    Add(*(new Scroller(m_Size.x, zin::Scroller::Horizontal)), "ScrollerH");
    Add(*(new Scroller(m_Size.y, zin::Scroller::Vertical  )), "ScrollerV");
    m_View.Reset(sf::FloatRect(m_Pos.x, m_Pos.y, m_Size.x, m_Size.y));
    m_View.SetViewport(sf::FloatRect(m_Pos.x/800, m_Pos.x/600, m_Size.x/800, m_Size.y/600));
    std::cout << "[Zindows] ScrollBox created (" << this << ")" << std::endl;
}

////////////////////////////////////////////////////////////
ScrollBox::~ScrollBox() {
    delete &Get("ScrollerH");
    delete &Get("ScrollerV");
    std::cout << "[Zindows] ScrollBox deleted (" << this << ")" << std::endl;
}

////////////////////////////////////////////////////////////
sf::Vector2f ScrollBox::AutoSet(const sf::Vector2f& objectSize) {

    sf::Vector2f hScrollerSize = Get("ScrollerH").GetSize();
    sf::Vector2f vScrollerSize = Get("ScrollerV").GetSize();

/// Tests de dépassement de l'objet :

    m_IsHScroller = objectSize.x > m_Size.x;
    m_IsVScroller = objectSize.y > m_Size.y;

/// Calcul de la nouvelle taille de l'objet :

    sf::Vector2f newObjectSize = objectSize;
    if( m_IsVScroller )
        newObjectSize.x-=vScrollerSize.x;
    if( m_IsHScroller )
        newObjectSize.y-=hScrollerSize.y;

/// Redimensionnement des scrollers :

    if( m_IsVScroller && m_IsHScroller ) {
        if( m_IsVScroller ) {
            Add(*(new Scroller(m_Size.y - hScrollerSize.y, zin::Scroller::Vertical  )), "ScrollerV");
            Get("ScrollerV").SetVisibility(true);
            Get("ScrollerV").SetLock(false);
            Add(*(new Scroller(m_Size.x - vScrollerSize.x, zin::Scroller::Horizontal)), "ScrollerH");
            Get("ScrollerH").SetVisibility(true);
            Get("ScrollerH").SetLock(false);
        }
        else {
            Get("ScrollerV").SetVisibility(false);
            Get("ScrollerV").SetLock(true);
            Get("ScrollerH").SetVisibility(false);
            Get("ScrollerH").SetLock(true);
        }
    }
    else {
        if( m_IsVScroller ) {
            Add(*(new Scroller(m_Size.y, zin::Scroller::Vertical  )), "ScrollerV");
            Get("ScrollerH").SetVisibility(false);
            Get("ScrollerH").SetLock(true);
        }
        else {
            Add(*(new Scroller(m_Size.x, zin::Scroller::Horizontal)), "ScrollerH");
            Get("ScrollerV").SetVisibility(false);
            Get("ScrollerV").SetLock(true);
        }
    }

    return newObjectSize;
}

////////////////////////////////////////////////////////////
void ScrollBox::Set(const sf::String& childName) {
    if( childName == "ScrollerH" ) {
        float height = Get("ScrollerH").GetSize().y;
        Get("ScrollerH").Move(m_Pos.x, m_Pos.y + m_Size.y - height);
    }
    if( childName == "ScrollerV" ) {
        float width = Get("ScrollerV").GetSize().x;
        Get("ScrollerV").Move(m_Pos.x + m_Size.x - width, m_Pos.y);
    }
}

////////////////////////////////////////////////////////////
void ScrollBox::Move(const sf::Vector2f& shift) {
    Widget::Move(shift);
    m_View.Move(shift);
    m_View.SetViewport(sf::FloatRect(m_Pos.x/800, m_Pos.y/600, m_Size.x/800, m_Size.y/600));
}

////////////////////////////////////////////////////////////
void ScrollBox::BuildDrawing() {
    m_Drawing.Clear();
    m_Drawing+=sf::Shape::Rectangle(m_Pos.x, m_Pos.y, m_Size.x, m_Size.y, m_Theme.Colors.Get("TextBoxBody"));
}

////////////////////////////////////////////////////////////
void ScrollBox::Draw(sf::RenderTarget& target) const {
    target.SetView(m_View);
    Widget::Draw(target);
    target.SetView(target.GetDefaultView());
}

}

