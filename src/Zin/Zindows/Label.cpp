/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zindows/Label.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class TextArea definition
////////////////////////////////////////////////////////////
Label::Label() :
sf::Text() {
}

Label::Label(const sf::String& string, const sf::Font& font, unsigned int characterSize) :
sf::Text(string, font, characterSize),
m_UnformatedString(string) {
    AutoResize();
}

void Label::SetWidth(float x) {
    if( x > 0 &&
        x != m_Size.x ) {
        m_Size.x = x;
        Format();
    }
}

const sf::Vector2f &Label::GetSize() {
    return TextTools::GetSize(*this);
}

float Label::GetWidth() {
    return m_Size.x;
}

float Label::GetHeight() {
    return m_Size.y;
}

void Label::SetString(const sf::String& string) {
    sf::Text::SetString(string);
    m_UnformatedString = string;
    AutoResize();
}

void Label::SetCharacterSize(unsigned int size) {
    sf::Text::SetCharacterSize(size);
    AutoResize();
}

void Label::AutoResize() {
    sf::Vector2f newSize = TextTools::GetSize(*this);
    if( newSize.x > m_Size.x || newSize.y > m_Size.y ) {
        m_Size = newSize;
    }
}

void Label::Format() {
/// Si la largeur du label est inf�rieure � la largeur du texte :
    if( m_Size.x < TextTools::GetWidth(*this) ) {
    /// R�cup�ration des caract�ristiques du texte :
        sf::Font font = GetFont();
        float fontSize = GetCharacterSize();
        std::string str = m_UnformatedString;
        unsigned int nbChar = str.size();
        float shortenerWidth = TextTools::GetWidth(sf::Text("...", GetFont(), GetCharacterSize()));
    /// Si la largeur du label est inf�rieure � la largeur du shortener :
        if( m_Size.x < shortenerWidth )
            SetString("..."); // On remplace le texte par le shortener, le label est automatiquement redimensionn�
        else {
            float currentLength = 0;
            float maxLength = m_Size.x - shortenerWidth;
        /// Parcours de la cha�ne :
            for( size_t k(0); k < nbChar; ++k ) {
            /// Ajout de la largeur du caract�re courant � la largeur totale du texte parcourue :
                currentLength+=TextTools::GetCharWidth(str[k], font, fontSize);
            /// Si la largeur totale parcourue est sup�rieure � la largeur maximale autoris�e :
                if( currentLength > maxLength ) {
                /// On supprime la suite de la cha�ne et on la remplace par le shortener :
                    str.erase(str.begin()+k, str.end());
                    str+="...";
                    SetString(str);
                    break;
                }
            }
        }
    }
}

}
