/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zintools/Math/Intersection.hpp>

namespace zin {

namespace Math {

bool OnIntersectCircles(const sf::Vector2f& center1, float radius1, const sf::Vector2f& center2, float radius2) {
	// If distance between centers is lower or equal than
	// the sum of the radiuses, circles are colliding
	return GetDistance(center1, center2) <= radius1 + radius2;
}

bool OnIntersectSegments(const sf::Vector2f& s1p1, const sf::Vector2f& s1p2, const sf::Vector2f& s2p1, const sf::Vector2f& s2p2) {
	// One special step : if one of the segments, or both, is/are vertical
	if(s1p2.x - s1p1.x == 0 || s2p2.x - s2p1.x == 0) {
		// If both segments are vertical, they are parallel :
		if(s1p2.x - s1p1.x == 0 && s2p2.x - s2p1.x == 0) {
			// If segments are not on the same line, then they are not
			// colliding
			if(s1p1.x != s2p2.x)
				return false;
			// Else segments are on the same line, so we check
			// if the segments are touching
			if( IsBelongingTo(s1p1.y, s1p2.y, s2p1.y) || IsBelongingTo(s1p1.y, s1p2.y, s2p2.y) )
				return true;
		}
		// Else, the next step depends on what segment is vertical
		// Segment 1 is vertical
		else if(s1p2.x - s1p1.x == 0) {
			// Find the equation of the "not vertical" segment
			float slope = (s2p2.y - s2p1.y) / (s2p2.x - s2p1.x);
			float y_intercept = s2p1.y - slope * s2p1.x;
			// Next we find the ordinate of the point who have the vertical
			// segment's abscissa. If this ordinate belongs to the vertical
			// segment, then segments are colliding
			if( IsBelongingTo(s1p1.y, s1p2.y, s1p1.x * slope + y_intercept) )
				return true;
		}
		// Segment 2 is vertical (all steps are the same)
		else if( s2p2.x - s2p1.x == 0 ) {
			float slope = (s1p2.y - s1p1.y) / (s1p2.x - s1p1.x);
			float y_intercept = s1p1.y - slope * s1p1.x;
			if( IsBelongingTo(s2p1.y, s2p2.y, s2p1.x * slope + y_intercept) )
				return true;
		}
		// If we are here, segments are not colliding
		return false;
	}
	// If there isn't any vertical segment
	else {
		// Calculate equation of the lines formed by the segments
		// y = slope * x + y-intercept
		float slope1 = (s1p2.y - s1p1.y) / (s1p2.x - s1p1.x);
		float y_intercept1 = s1p1.y - (slope1 * s1p1.x);
		float slope2 = (s2p2.y - s2p1.y) / (s2p2.x - s2p1.x);
		float y_intercept2 = s2p1.y - (slope1 * s2p1.x);
		// If the lines are parallel :
		if( slope1 == slope2 ) {
			// If y-intercepts are not equal, the segments are not
			// on the same line, so they are not colliding
			if( y_intercept1 != y_intercept2 )
				return false;
			// Else, check if one segment is touching the other one
			if( IsBelongingTo(s1p1.x, s1p2.x, s2p1.x) || IsBelongingTo(s1p1.x, s1p2.x, s2p2.x) )
				return true;
			// If it is not touching, segments are not colliding
				return false;
		}
		// If lines are not parallel
		else {
			// Find the intersection point of the axis
			// (only one value, x or y, is needed)
			float x = (y_intercept2 - y_intercept1) / (slope1 - slope2);
			// If the intersection point belongs to bosh segments, then
			// the segments are colliding
			if( IsBelongingTo(s1p1.x, s1p2.x, x) || IsBelongingTo(s2p1.x, s2p2.x, x) )
				return true;
		}
		// If we are here, then segments are not colliding
		return false;
	}
}

}

}

