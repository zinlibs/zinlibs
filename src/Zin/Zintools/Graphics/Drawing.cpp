/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zintools/Graphics/Drawing.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Drawing definition
////////////////////////////////////////////////////////////
Drawing::~Drawing() {
    Clear();
}

void Drawing::Add(const sf::Shape& shape) {
    m_Drawables.push_back(new sf::Shape(shape));
}

void Drawing::operator+=(const sf::Shape& shape) {
    Add(shape);
}

void Drawing::Add(const sf::Text& text) {
    m_Drawables.push_back(new sf::Text(text));
}

void Drawing::operator+=(const sf::Text& text) {
    Add(text);
}

void Drawing::Add(const sf::Sprite& sprite) {
    m_Drawables.push_back(new sf::Sprite(sprite));
}

void Drawing::operator+=(const sf::Sprite& sprite) {
    Add(sprite);
}

void Drawing::Move(const sf::Vector2f& shift) {
    m_Pos+=shift;
    for( std::list<sf::Drawable*>::iterator it = m_Drawables.begin(); it != m_Drawables.end(); ++it )
        (*(*it)).Move(shift);
}

void Drawing::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

void Drawing::SetPosition(const sf::Vector2f& pos) {
    Move(pos-m_Pos);
}

void Drawing::SetPosition(float x, float y) {
    SetPosition(sf::Vector2f(x, y));
}

void Drawing::Render(sf::RenderTarget& target, sf::Renderer& renderer) const {
    for( std::list<sf::Drawable*>::const_iterator it = m_Drawables.begin(); it != m_Drawables.end(); ++it )
        target.Draw(*(*it));
}

void Drawing::Clear() {
    for( std::list<sf::Drawable*>::iterator it = m_Drawables.begin(); it != m_Drawables.end(); ++it )
        delete *it;
    m_Drawables.clear();
}

}
