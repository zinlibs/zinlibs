/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zintools/System/Timer.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Timer definition
////////////////////////////////////////////////////////////
Timer::Timer() {
    Reset();
}

void Timer::Reset() {
    m_Time = 0;
    m_T = 0;
    m_StartTime = 0;
    m_TimePause = 0;
    m_IsRunning = false;
    m_IsFirst = true;
}

void Timer::Start() {
    if( !m_IsRunning ) {
        m_IsRunning = true;
        if ( m_IsFirst )
            m_StartTime = m_Clock.GetElapsedTime();
        else {
            m_StartTime+=m_Clock.GetElapsedTime();
            m_StartTime-=m_TimePause;
        }
        m_IsFirst = false;
    }
}

void Timer::Pause() {
    if( m_IsRunning ) {
        m_IsRunning = false;
        m_TimePause = m_Clock.GetElapsedTime();
    }
}

void Timer::Update() {
    if( m_IsRunning )
        m_Time = m_Clock.GetElapsedTime() - m_StartTime;
}

float Timer::GetTime() {
    return m_Time;
}

void Timer::Print() {
    std::cout << "[Zintools] Temps actuel : " << m_Time << std::endl;
}

void Timer::SaveCurrentTime() {
    m_T = m_Time;
}

float Timer::GiveTimeElapsed() {
    return m_Time - m_T;
}

bool Timer::IsRunning() {
    return m_IsRunning;
}

}
