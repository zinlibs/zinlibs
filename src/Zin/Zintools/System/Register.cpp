/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zintools/System/Register.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Register definition
////////////////////////////////////////////////////////////
Register::Register() {
    std::cout << "[zintools] Register created (" << this << ")" << std::endl;
}

void Register::Enable(const sf::String& string) {
    Add(string, true);
}

void Register::Switch(const sf::String& string) {
    std::map<sf::String, Value>::iterator it = m_Map.find(string);
    if( it != m_Map.end() ) {
        it->second.Status = !it->second.Status;
        it->second.Changed = true;
    }
}

void Register::Disable(const sf::String& string) {
    Add(string, false);
}

void Register::Add(const sf::String& string, bool enabled) {
    std::map<sf::String, Value>::iterator it = m_Map.find(string);
    if( it == m_Map.end() )
        m_Map.insert(std::pair<sf::String, Value>(string, Value(enabled)));
    else Change(string, enabled);
}

void Register::Change(const sf::String& string, bool enabled) {
    std::map<sf::String, Value>::iterator it = m_Map.find(string);
    if( it != m_Map.end() ) {
        if( it->second.Status != enabled ) {
            it->second.Status = enabled;
            it->second.Changed = true;
        }
    }
}

bool Register::IsTrue(const sf::String& string) {
    std::map<sf::String, Value>::iterator it = m_Map.find(string);
    return it != m_Map.end() ? it->second.Status : 0;
}

bool Register::IsChanged(const sf::String& string) {
    std::map<sf::String, Value>::iterator it = m_Map.find(string);
    if( it != m_Map.end() ) {
        it->second.Changed = false;
        return it->second.Changed;
    }
    return true;
}

bool Register::Contains(const sf::String& string) {
    std::map<sf::String, Value>::iterator it = m_Map.find(string);
    return it != m_Map.end();
}

void Register::Print() {
    if( m_Map.size() ) {
        /*std::cout << "[Zintools] Register content :\n";
        for( std::map<sf::String, Value>::iterator it = m_Map.begin(); it != m_Map.end(); ++it ) {
            std::cout << " - " << it->first; // acc�de � la cl�
            it->second.Status ? std::cout << " enabled\n" : std::cout << " disabled\n";// accede � la valeur
        }*/
    }
}

}
