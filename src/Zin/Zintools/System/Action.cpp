/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zintools/System/Action.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Action definition
////////////////////////////////////////////////////////////
Action::Action(const unsigned int id) : Id(id) {
}

Action& Action::operator=(const Action& action) {
    Id = action.Id;
    return *this;
}

bool Action::operator==(const Action& action) {
  return Id == action.Id;
}

////////////////////////////////////////////////////////////
/// class ActionIndex definition
////////////////////////////////////////////////////////////
ActionIndex::ActionIndex() :
m_ActionCount(0),
m_UnknowActionNotification("Unknow in action index") {
    std::cout << "[zintools] Action index created (" << this << ")" << std::endl;
}

ActionIndex::~ActionIndex() {
    std::cout << "[zintools] Action index deleted (" << this << ")" << std::endl;
    m_Map.clear();
}

const Action& ActionIndex::Get(const std::string& string) {
    std::map<std::string, Action>::iterator it = m_Map.find(string);
    if( it != m_Map.end() )
        return (*it).second;
    m_Map.insert(std::pair<std::string, Action>(string, Action(m_ActionCount)));
    ++m_ActionCount;
    return Get(string);
}

const Action& ActionIndex::operator[](const std::string& string) {
    return Get(string);
}

const Action& ActionIndex::operator()(const std::string& string) {
    return Get(string);
}

void ActionIndex::Add(const std::string& string) {
    Get(string);
}

void ActionIndex::operator+=(const std::string& string) {
    Add(string);
}

const std::string& ActionIndex::Get(Action action) {
    std::map<std::string, Action>::iterator it = m_Map.begin();
    while( it != m_Map.end() ) {
        if( (*it).second.Id == action.Id )
            return (*it).first;
        ++it;
    }
    return m_UnknowActionNotification;
}

////////////////////////////////////////////////////////////
/// class ActionQueue definition
////////////////////////////////////////////////////////////
ActionQueue::ActionQueue() {
}

ActionQueue::ActionQueue(Action action) {
    m_Actions.push_back(action);
}

void ActionQueue::operator+=(const ActionQueue& queue) {
    for( size_t k(0); k < queue.m_Actions.size(); ++k )
        m_Actions.push_back(queue.m_Actions[k]);
}

void ActionQueue::operator+=(Action action) {
    m_Actions.push_back(action);
}

bool ActionQueue::Empty() {
    return m_Actions.empty();
}

void ActionQueue::Print() {
    if( !Empty() ) {
        std::cout << "[zintools] There is " << m_Actions.size() << " action(s) into the action queue" << std::endl;
        ActionIndex* index = ActionIndex::GetSingleton();
        for( size_t k(0); k <  m_Actions.size(); ++k ) {
            std::cout << "[zintools] - " << index->Get(m_Actions[k]) << std::endl;
        }
    }
}

ActionQueue ActionQueue::GetEmpty() {
    return ActionQueue();
}

bool ActionQueue::Get(Action& action) {
    if( !Empty() ) {
        action = m_Actions.back();
        m_Actions.pop_back();
        return true;
    }
    return false;
}

bool ActionQueue::Contains(const Action& action) {
    for( size_t k(0); k < m_Actions.size(); ++k )
        if( m_Actions[k] == action )
            return true;
    return false;
}

}
