/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zinetic/Particle.hpp>

namespace zin {

Particle::Particle(const sf::Vector2f& pos, float mass) :
m_Pos(pos),
m_Mass(mass) {
    m_Mark = sf::Shape::Circle(pos.x, pos.y, 1, sf::Color::White);
}

void Particle::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

void Particle::Move(const sf::Vector2f& shift) {
    m_Pos+=shift;
    m_Mark.Move(shift);
}

void Particle::SetPosition(float x, float y) {
    SetPosition(sf::Vector2f(x, y));
}

void Particle::SetPosition(const sf::Vector2f& pos) {
    Move(pos-m_Pos);
}

void Particle::SetSpeed(const sf::Vector2f& speed) {
    m_Speed = speed;
}

void Particle::SetMass(float mass) {
    m_Mass = mass;
}

void Particle::SetForce(const sf::Vector2f& force) {
    m_Force = force;
}

const sf::Vector2f& Particle::GetPosition() const {
    return m_Pos;
}

const sf::Vector2f& Particle::GetForce() const {
    return m_Force;
}

const sf::Vector2f& Particle::GetSpeed() const {
    return m_Speed;
}

float Particle::GetMass() const {
    return m_Mass;
}

const sf::Vector2f& Particle::GetGravitationForce(const Particle& p, const float G) {
/// On calcule la distance entre les deux particules
    const sf::Vector2f Vector = p.GetPosition()-m_Pos;
    float distance = Math::GetLength(Vector);
/// On devrait utiliser un vecteur unitaire, mais ça fait tout boguer
    const sf::Vector2f unitVector = Vector/distance;
/// On applique la loi de gravitation universelle
/// Puis on retourne le vecteur force résultant
    return G*m_Mass*p.GetMass()*Vector/(distance*distance);
}

void Particle::Update(float timeLapse) {
/// Nous désignons la somme des force appliquées à la particule par m_Force
/// L'intervalle de temps dans lequel nous appliquons cette force est nommé timeLapse
/// D'après Newton, la somme des forces est égale à la masse que multiplie l'accélération (vectoriellement)
/// L'accélération est la dérivée de la vitesse par rapport au temps
/// Soit l'expression de (m_Speed'- m_Speed)/timeLapse
/// D'où m_Force = m_Mass * (m_Speed' - m_Speed)/timeLapse
/// Exprimons m_Speed' en fonction du reste, nous obtenons :
/// m_Speed' = m_Speed + m_Force * timeLapse/m_Mass
/// En langage de programmation nous traduisons par :
    m_Speed+=(m_Force*timeLapse/m_Mass);
/// Nous calculons maintenant la nouvelle position de la particule
/// Grace à l'expression v = d/t, donc d = v * t
    Move(m_Speed*timeLapse);
/// La force a été appliqué, elle n'a donc plus de raison d'être
    m_Force = sf::Vector2f(0.f, 0.f);
}

void Particle::Draw(sf::RenderTarget& target) const {
    target.Draw(m_Mark);
}

}
