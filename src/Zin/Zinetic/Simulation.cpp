/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zinetic/Simulation.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// Simulation
////////////////////////////////////////////////////////////
Simulation::Simulation(float width, float height) :
m_Size(sf::Vector2f(width, height)),
m_View(sf::FloatRect(0, 0, width, height)),
m_GravityPoint(NULL),
m_Sandbox(NULL),
m_IsCollisions(false),
m_IsGravitation(true),
m_Seed(sf::Randomizer::GetSeed()),
m_ZoomLevel(5),
m_IsCollisionSuggested(true) {
}

Simulation::~Simulation() {
    Clear();
    if( m_Sandbox )
        delete m_Sandbox;
}

void Simulation::Randomize(size_t objectNb, float maxMass) {
    Clear();
    sf::Vector2f randomCenter;
    sf::Vector2f maxRange;
    if( m_Sandbox ) {
        randomCenter = sf::Vector2f((*m_Sandbox).Rect.Width/2+(*m_Sandbox).Rect.Left, (*m_Sandbox).Rect.Height/2+(*m_Sandbox).Rect.Top);
        maxRange = sf::Vector2f((*m_Sandbox).Rect.Width/2, (*m_Sandbox).Rect.Height/2);
    }
    else {
        randomCenter = sf::Vector2f(m_Size.x/2, m_Size.y/2);
        maxRange = sf::Vector2f(m_Size.x/2, m_Size.y/2);
    }
    randomCenter+=m_Pos;
    size_t n(0);
    while( n < objectNb ) {
        bool isColliding = false;
    /// Obtention des données aléatoires
        float x = sf::Randomizer::Random(-maxRange.x, maxRange.x);
        float y = sf::Randomizer::Random(-maxRange.y, maxRange.y);
        sf::Vector2f pos(x, y);
        float mass = sf::Randomizer::Random(1.f, maxMass);
    /// Création de l'objet
        CircleObject* object = new CircleObject(pos+randomCenter, mass, sqrt(mass/3.14));
    /// Test de collision
        for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it ) {
            if( (*(*it)).OnCollide(*object) ) {
                isColliding = true;
                break;
            }
        }
    /// Si l'objet ne se superpose pas avec un objet déjà créé et qu'il ne dépasse pas du cercle, alors on l'ajoute à la liste
        if( zin::Math::GetLength(pos) < 150 && !isColliding ) {
            n++;
            m_Objects.push_back(object);
			object->SetForce(sf::Vector2f(0.f,0.f));
        }
    /// Sinon il est détruit et on recommence
        else delete object;
    }
    ComputeCollisions();
    Update();
}

void Simulation::SetGravitation(bool gravitation) {
    m_IsGravitation = gravitation;
}

const sf::View& Simulation::GetView() {
    return m_View;
}

void Simulation::Clear() {
/// On détruit la liste d'objets
    for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it )
		if(*it) {
    		delete *it;
			*it = NULL;
		}
    m_Objects.clear();
/// On détruit le potentiel point de gravité
    if( m_GravityPoint ) {
        delete m_GravityPoint;
        m_GravityPoint = NULL;
    }
}

void Simulation::Restart() {
    size_t nb = m_Objects.size();
    Clear();
    sf::Randomizer::SetSeed(m_Seed);
    Randomize(nb);
}

void Simulation::Move(float x, float y) {
    Move(sf::Vector2f(x, y));
}

void Simulation::Move(const sf::Vector2f& shift) {
    m_Pos+=shift;
    for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it )
        (*(*it)).Move(shift);
    if( m_Sandbox ) {
        (*m_Sandbox).Rect.Left+=shift.x;
        (*m_Sandbox).Rect.Top+=shift.y;
        (*m_Sandbox).Shape.Move(shift);
    }
}

void Simulation::SetPosition(float x, float y) {
    SetPosition(sf::Vector2f(x, y));
}

void Simulation::SetRunning(bool running) {
    running ? m_Clock.Start() : m_Clock.Pause();
}

void Simulation::SetPosition(const sf::Vector2f& pos) {
    Move(pos-m_Pos);
}

void Simulation::SetCollisions(bool enable) {
    m_IsCollisions = enable;
}

void Simulation::SetGravityPoint(bool enable, const sf::Vector2f& mousePos, float mass) {
    if( enable ) {
        if( !m_GravityPoint )
            m_GravityPoint = new Particle(mousePos, mass);
        (*m_GravityPoint).SetPosition(mousePos);
    }
    else if( m_GravityPoint ) {
        delete m_GravityPoint;
        m_GravityPoint = NULL;
    }
}

void Simulation::SetSandbox(bool enable, sf::FloatRect sandboxRect) {
    if( enable ) {
        if( !m_Sandbox ) {
            m_Sandbox = new Sandbox;
            (*m_Sandbox).AttenuationCoeff = .5;
        }
        (*m_Sandbox).Rect = sandboxRect;
        (*m_Sandbox).Shape = sf::Shape::Rectangle(sandboxRect, sf::Color(0, 0, 0, 0), 4, sf::Color::Yellow);
    }
    else {
        delete m_Sandbox;
        m_Sandbox = NULL;
    }
}

void Simulation::SetObjectsImage(const sf::Image& image) {
    for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it )
        (*(*it)).SetImage(image);
}

void Simulation::Resize(float x, float y) {
    Resize(sf::Vector2f(x, y));
}

void Simulation::Resize(const sf::Vector2f& size) {
    m_Size = size;
}

void Simulation::ZoomIn() {
    m_ZoomLevel++;
    m_View.Zoom(.5);
}

void Simulation::ZoomOut() {
    if( m_ZoomLevel > 3 ) {
        m_ZoomLevel--;
        m_View.Zoom(2);
    }
}

void Simulation::ResetZoom() {
    while( m_ZoomLevel != 5 ) {
        if( m_ZoomLevel > 5 )
            ZoomOut();
        else ZoomIn();
    }
}

void Simulation::Draw(sf::RenderTarget& target) {
    target.SetView(m_View);
    if( m_Sandbox )
        target.Draw((*m_Sandbox).Shape);
    for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it )
        (*(*it)).Draw(target);
    target.SetView(target.GetDefaultView());
}

void Simulation::ComputeGravity() {
/// Tableau qui contiendra la somme des forces de chaque objet
    size_t size = m_Objects.size();
	std::vector<sf::Vector2f> sumForces;
		sumForces.reserve(size);
/// Parcours croisé des objets (la gravitation est une force exercée par un objet sur un autre)
    size_t a = 0;
    for( std::list<CircleObject*>::iterator ita = m_Objects.begin(); ita != m_Objects.end(); ++ita ) {
        size_t b = 0;
    /// Application de la gravité du point de gravité aux objets
        if( m_GravityPoint )
            sumForces[a]+=(*(*ita)).GetGravitationForce(*m_GravityPoint);
    /// Application de la gravité entre les objets
        if( m_IsGravitation ) {
            for( std::list<CircleObject*>::iterator itb = m_Objects.begin(); itb != m_Objects.end(); ++itb ) {
            /// Nous évitons les calculs inutiles ( vecteur_force( A -> B ) == - vecteur_force( B -> A ) )
            /// Il y a donc seulement (n²-n)/2 calculs à faire, c'est à dire pour 100 objets : calculs = (100²-100)/2 = 4950
            /// Au lieu de 10000, soit un peu plus de 2 fois moins
                if( a < b ) {
                    sf::Vector2f force = (*(*ita)).GetGravitationForce(*(*itb)); // Obtention de la force de gravitation
                    sumForces[a]+=force;
                    sumForces[b]-=force; // Le vecteur déjà calculé est stoqué
                }
                b++;
            }
        }
        a++;
    }
/// Parcours normal des objets afin de leur transmettre la force résultante
    size_t k = 0;
    for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it ) {
        (*(*it)).SetForce(sumForces[k]);
        k++;
    }
}

void Simulation::ComputeCollisions() {
    size_t a = 0;
    for( std::list<CircleObject*>::iterator ita = m_Objects.begin(); ita != m_Objects.end(); ++ita ) {
        size_t b = 0;
    /// Application des collisions à la sandbox aux objets
        if( m_Sandbox )
                (*(*ita)).OnSandboxCollide(*m_Sandbox);
    /// Application des collisions entre les objets
        if( m_IsCollisions || m_IsCollisionSuggested ) {
            for( std::list<CircleObject*>::iterator itb = m_Objects.begin(); itb != m_Objects.end(); ++itb ) {
            /// Nous évitons les calculs inutiles ( vecteur_force( A -> B ) == - vecteur_force( B -> A ) )
            /// Il y a donc seulement (n²-n)/2 calculs à faire, c'est à dire pour 100 objets : calculs = (100²-100)/2 = 4950
            /// Au lieu de 10000, soit un peu plus de 2 fois moins
                if( a < b ) {
                    bool collision = (*(*ita)).OnCollide((*(*itb)), m_IsCollisions);
                    if( m_IsCollisionSuggested ) {
                        if( collision ) {
                            //(*(*ita)).SetColor(sf::Color::Yellow);
                            //(*(*itb)).SetColor(sf::Color::Yellow);
                        }
                        else {
                            //(*(*ita)).SetColor(sf::Color::Red);
                            //(*(*itb)).SetColor(sf::Color::Red);
                        }
                    }
                }
                b++;
            }
        }
        a++;
    }
}

void Simulation::Update() {
/// On vérifie que l'horloge tourne encore, on la met à jour et on récupère l'intervalle de temps stocké
    if( m_Clock.IsRunning() ) {
        m_Clock.Update();
        float step = m_Clock.GiveTimeElapsed();
        m_Clock.SaveCurrentTime();
/// Si cet intervalle est non nul, on procède aux calculs
        if( step > 0 ) {
            if( m_IsGravitation || m_GravityPoint )
                ComputeGravity();
            if( m_IsCollisions || m_Sandbox )
                ComputeCollisions();
        /// Enfin, les objets convertissent leur force en énergie cinétique
            for( std::list<CircleObject*>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it )
                (*(*it)).Update(step);
        }
    }
}

}
