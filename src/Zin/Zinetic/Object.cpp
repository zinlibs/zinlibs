/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *
 *    Zinlibs - Copyright (c) 2010-2011 The Zin Team :
 *    - Pierre-Emmanuel Brian
 *    - Alexandre Janniaux
 *    - Paul Helly
 *    - Nicolas Treiber
 *    Please help to support the project at 'http://gitorious.org/zinlibs'
 *
 *    This software is provided 'as-is', without any express or implied
 *    warranty. In no event will the authors be held liable for any damages
 *    arising from the use of this software.
 *
 *    Permission is granted to anyone to use this software for any purpose,
 *    including commercial applications, and to alter it and redistribute it
 *    freely, subject to the following restrictions :
 *
 *    1. The origin of this software must not be misrepresented; you must not
 *       claim that you wrote the original software. If you use this software
 *       in a product, an acknowledgment in the product documentation would be
 *       appreciated but is not reqInterfacered.
 *
 *    2. Altered source versions must be plainly marked as such, and must not be
 *       misrepresented as being the original software.
 *
 *    3. This notice may not be removed or altered from any source
 *       distribution.
 *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Zin/Zinetic/Object.hpp>

namespace zin {

////////////////////////////////////////////////////////////
/// class Object definition
////////////////////////////////////////////////////////////
Object::Object(const sf::Vector2f& pos, float mass) :
Particle(pos, mass),
m_Sprite(NULL) {
}

void Object::Move(const sf::Vector2f& shift) {
    Particle::Move(shift);
    m_Shell.Move(shift);
    if( m_Sprite )
        (*m_Sprite).Move(shift);
}

void Object::SetColor(const sf::Color& color) {
    for( size_t k(0); k < m_Shell.GetPointsCount(); ++k )
        m_Shell.SetPointOutlineColor(k, color);
    if( m_Sprite )
        (*m_Sprite).SetColor(color);
}

void Object::SetImage(const sf::Image& image) {
    if( !m_Sprite )
        m_Sprite = new sf::Sprite;
    (*m_Sprite).SetImage(image);
    sf::Vector2f spriteSize = (*m_Sprite).GetSize();
    sf::Vector2f origin = sf::Vector2f(spriteSize.x/2, spriteSize.y/2);
    (*m_Sprite).SetOrigin(origin);
    (*m_Sprite).SetPosition(m_Pos);
    (*m_Sprite).SetColor(sf::Color(sf::Randomizer::Random(0, 255), sf::Randomizer::Random(0, 255), sf::Randomizer::Random(0, 255)));
}

void Object::Draw(sf::RenderTarget& target) const {
    Particle::Draw(target);
    if( m_Sprite )
        target.Draw(*m_Sprite);
    else target.Draw(m_Shell);

}

////////////////////////////////////////////////////////////
/// class CircleObject definition
////////////////////////////////////////////////////////////
CircleObject::CircleObject(const sf::Vector2f& pos, float mass, float radius) :
Object(pos, mass),
m_Radius(radius) {
    m_Shell = sf::Shape::Circle(pos.x, pos.y, radius, sf::Color(0, 0, 0, 0), 2, sf::Color::Red);
}

float CircleObject::GetRadius() const {
    return m_Radius;
}

void CircleObject::SetImage(const sf::Image& image) {
    Object::SetImage(image);
    sf::Vector2f spriteSize = (*m_Sprite).GetSize();
    (*m_Sprite).SetScale(2.8*m_Radius/spriteSize.x, 2.8*m_Radius/spriteSize.y);
}

void CircleObject::OnSandboxCollide(const Sandbox& sandbox) {
    if( m_Pos.x < sandbox.Rect.Left + m_Radius ) {
        Particle::SetPosition(sandbox.Rect.Left + m_Radius, m_Pos.y);
        m_Speed.x = -sandbox.AttenuationCoeff * m_Speed.x;
    }
    if( m_Pos.x > sandbox.Rect.Left + sandbox.Rect.Width - m_Radius ) {
        Particle::SetPosition(sandbox.Rect.Left + sandbox.Rect.Width - m_Radius, m_Pos.y);
        m_Speed.x = -sandbox.AttenuationCoeff * m_Speed.x;
    }
    if( m_Pos.y < sandbox.Rect.Top + m_Radius ) {
        Particle::SetPosition(m_Pos.x, sandbox.Rect.Top + m_Radius);
        m_Speed.y = -sandbox.AttenuationCoeff * m_Speed.y;
    }
    if( m_Pos.y > sandbox.Rect.Top + sandbox.Rect.Height - m_Radius) {
        Particle::SetPosition(m_Pos.x, sandbox.Rect.Top + sandbox.Rect.Height - m_Radius);
        m_Speed.y = -sandbox.AttenuationCoeff * m_Speed.y;
    }
}

bool CircleObject::OnCollide(CircleObject& incoming, bool applyCollision) {
    bool collision = zin::Math::GetDistance(m_Pos, incoming.GetPosition()) <= incoming.GetRadius()+m_Radius;
    if( applyCollision ) {
        if( collision ) {
            ComputeCollision(incoming);
            return true;
        }
    }
    else return collision;
}

void CircleObject::ComputeCollision(CircleObject& incoming) {
/// On calcule la distance entre les deux particules
    const sf::Vector2f diff = incoming.GetPosition()-m_Pos;
    float distance = Math::GetLength(diff);
/// Calcul du vecteur unitaire
    const sf::Vector2f unitVector = diff/distance;
/// Calcul de la distance excèdentaire
    float extraDistance = incoming.GetRadius() + m_Radius - distance;
/// Calcul du vecteur de correction
    sf::Vector2f translation = extraDistance * unitVector;
    translation.x/=2;
    translation.y/=2;
/// Application de la correction
    Move(-translation);
    incoming.Move(translation);
/// Calcul de la vitesse du centre de gravité des deux masses en collision
    float mass = incoming.GetMass();
    sf::Vector2f speed = incoming.GetSpeed();
    sf::Vector2f centerSpeed = (m_Mass*m_Speed+mass*speed)/(m_Mass+mass);
/// Calcul du vecteur vitesse de chaque masse après collision
    SetSpeed(-m_Speed+centerSpeed+centerSpeed);
    incoming.SetSpeed(-speed+centerSpeed+centerSpeed);
}

}
